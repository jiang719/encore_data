#!/bin/bash


lang=$1
data_repo=$2
context=$3
fairseq_repo=$4
save_tokenized=$5
start=$6


# Tokenize
if [ "${start}" -lt 1 ]; then
    echo "Start tokenization"
    python -m source.tokenization.generate_data "${lang}" "${data_repo}" "${context}"
    python -m source.training.prepare_valid_data "${save_tokenized}" "${context}"
    echo "End tokenization"
fi

cp /home/tlutelli/issta_data/data/all2010/final/nocontext/valid/_test_meta.txt /home/tlutelli/issta_data/data/all2010/final/nocontext/train/_valid_meta.txt
cp /home/tlutelli/issta_data/data/all2010/final/nocontext/valid/_test_remadd.txt /home/tlutelli/issta_data/data/all2010/final/nocontext/train/_valid_remadd.txt

export FAIRSEQPY=/home/tlutelli/fairseq-context/

# Prepare data
if [ "${start}" -lt 2 ]; then


    echo "${context}"
    source activate pytorch
    python -m source.training.prepare_data "${lang}" "${data_repo}" "${fairseq_repo}" "${context}"
    source deactivate

fi



