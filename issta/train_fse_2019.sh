#!/bin/bash     
lang=$1
data_repo=$2
context=$3
fairseq_repo=$4
gpu_id=$5
tuning=$6
train_script=$7
start=$8


# Tokenize
if [ "${start}" -lt 1 ]; then
    echo "Start tokenization"
    python -m source.tokenization.generate_data "${lang}" "${data_repo}" "${context}"
    echo "End tokenization"
fi

if [ "${context}" == "nocontext" ]; then
    export FAIRSEQPY=/home/tlutelli/fairseq/
else
    export FAIRSEQPY=/home/tlutelli/fairseq-context/
fi

source activate pytorch
# Prepare data
if [ "${start}" -lt 2 ]; then
    

    echo "${context}"
    python -m source.training.prepare_data "${lang}" "${data_repo}" "${fairseq_repo}" "${context}"
fi
   

# Tune if tune on
if [ "${start}" -lt 3 ]; then
    if [ "${tuning}" -eq 1 ]; then
        echo "Tuning"

    fi
fi

# Tune if tune on
if [ "${start}" -lt 4 ]; then
    if [ "${tuning}" -eq 1 ]; then
        echo "Extract best models"
        python -m source.training.get_best_models

    fi
fi
        

if [ "${start}" -lt 5 ]; then
    export CUDA_VISIBLE_DEVICES="${gpu_id}"
    echo "Start training"
    model_dir="${fairseq_repo}"/"${train_script}"
    train_dir="${fairseq_repo}"/"${context}"/train/bin
    python -m source.training.autoscript."${train_script}" "${model_dir}" "${train_dir}"
    echo "End training"
fi


