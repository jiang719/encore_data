#!/bin/bash
lang=$1
date=$2
context=$3
git_repos=$4
start=$5
additional_name=$6

# start= 0 commit extraction
# start= 1

data_repo="/local1/tlutelli/issta_data/data/${additional_name}/"
fairseq_data="/local1/tlutelli/issta_data/fairseq-data/${additional_name}/"

intermediate="/local1/tlutelli/issta_data/intermediate/${additional_name}/"
temp_dir="/local1/tlutelli/issta_data/temp/${additional_name}/"

### Generate repositories
    [ ! -d "${intermediate}" ] && mkdir -p "${intermediate}"
    ### Generate repositories for the language
    [ ! -d "${data_repo}${lang}" ] && mkdir -p "${data_repo}${lang}"
    [ ! -d "${fairseq_data}${lang}" ] && mkdir -p "${fairseq_data}${lang}"
    [ ! -d "${temp_dir}${lang}" ] && mkdir -p "${temp_dir}${lang}"

    ### Generate data for date
    [ ! -d "${data_repo}${lang}/${date}" ] && mkdir -p "${data_repo}${lang}/${date}"
    [ ! -d "${data_repo}${lang}/${date}/meta" ] && mkdir -p "${data_repo}${lang}/${date}/meta"
    [ ! -d "${fairseq_data}${lang}/${date}" ] && mkdir -p "${fairseq_data}${lang}/${date}"
    [ ! -d "${temp_dir}${lang}/${date}_temp" ] && mkdir -p "${temp_dir}${lang}/${date}_temp"

    ### Generate data for context
    [ ! -d "${data_repo}${lang}/${date}/${context}" ] && mkdir -p "${data_repo}${lang}/${date}/${context}"
    [ ! -d "${fairseq_data}${lang}/${date}/${context}" ] && mkdir -p "${fairseq_data}${lang}/${date}/${context}"

### Commit extraction
if [ "${start}" -lt 1 ]; then
    echo "Extract commits"
    python -m source.data_extraction.buggy_commit_extractor1 "${lang}" "${git_repos}" "${data_repo}"
    echo "Commit extraction Done"
fi
### File extraction
if [ "${start}" -lt 2 ]; then
    echo "Start extracting files"
    if [ "${date}" == "all" ]; then
        for i in {2006..2019}
            do
                python -m source.data_extraction.training_extractor "${lang}" "${i}" "${git_repos}" "${data_repo}" "${temp_dir}"
            done
    else
        for (( i=2006; i<="${date}"; i++ ))
            do
                echo "${i}"
                python -m source.data_extraction.training_extractor "${lang}" "${i}" "${git_repos}" "${data_repo}" "${temp_dir}"
            done
    fi
    echo "File extraction done"
fi
exit 1
### Understand analysis
if [ "${start}" -lt 3 ]; then
    echo "Start working with Understand"
    export PYTHONPATH=$PYTHONPATH:/local/tlutelli/scitools/bin/linux64/Python
    if [ "${date}" == "all" ]; then
        for i in {2006..2019}
            do
                echo "${i}"
                python -m source.data_extraction.understand_work "${lang}" "${i}" "${temp_dir}" "${intermediate}"
                pids="$pids $!"
            done
        wait $pids
    else
        for (( i=2006; i<="${date}"; i++ ))
            do
                echo "${i}"
                python -m source.data_extraction.understand_work "${lang}" "${i}" "${temp_dir}" "${intermediate}"
                pids="$pids $!"
            done
        wait $pids
    fi
    echo "Working with Understand done"
fi

### Function extraction
if [ "${start}" -lt 4 ]; then
    echo "Start extracting Functions"
    export PYTHONPATH=$PYTHONPATH:/local/tlutelli/scitools/bin/linux64/Python
    if [ "${date}" == "all" ]; then
        for i in {2006..2019}
            do
                echo "${i}"
                python -m source.data_extraction.pickle_function "${lang}" "${i}" "${intermediate}" &
                pids="$pids $!"
            done
        wait $pids
    else
        for (( i=2006; i<="${date}"; i++ ))
            do
                echo "${i}"
                python -m source.data_extraction.pickle_function "${lang}" "${i}" "${intermediate}" &
                pids="$pids $!"
            done
        wait $pids

    fi
    echo "Extracting Function done"
fi

### Data extraction
if [ "${start}" -lt 5 ]; then
    echo "Start extracting instances"
    if [ "${date}" == "all" ]; then
        for i in {2006..2019}
            do
                echo "${i}"
                python -m source.data_extraction.extract_context "${lang}" "${i}" "${data_repo}" "${intermediate}" &
                pids="$pids $!"
            done
        wait $pids
        ### Combine all files together:
        [ ! -d "${data_repo}${lang}/all" ] && mkdir "${data_repo}${lang}/all"
        for i in {2006..2019}
            do
                cat "${data_repo}${lang}/${i}/add.txt" >>   "${data_repo}${lang}/all/add.txt"
                cat "${data_repo}${lang}/${i}/rem.txt" >>   "${data_repo}${lang}/all/rem.txt"
                cat "${data_repo}${lang}/${i}/meta.txt" >>   "${data_repo}${lang}/all/meta.txt"
                cat "${data_repo}${lang}/${i}/context.txt" >>   "${data_repo}${lang}/all/context.txt"
            done
    else
        for (( i=2006; i<="${date}"; i++ ))
            do
                echo "${i}"
                python -m source.data_extraction.extract_context  "${lang}" "${i}" "${data_repo}" "${intermediate}" # &
                pids="$pids $!"
            done
        wait $pids
        #exit 1
        #if [ ! -d "${data_repo}${lang}/all_${date}" ] && mkdir "${data_repo}${lang}/all_${date}"
        for (( i=2006; i<="${date}"; i++ ))
            do
                cat "${data_repo}${lang}/${i}/add.txt" >>   "${data_repo}${lang}/all_${date}/add.txt"
                cat "${data_repo}${lang}/${i}/rem.txt" >>   "${data_repo}${lang}/all_${date}/rem.txt"
                cat "${data_repo}${lang}/${i}/meta.txt" >>   "${data_repo}${lang}/all_${date}/meta.txt"
                cat "${data_repo}${lang}/${i}/context.txt" >>   "${data_repo}${lang}/all_${date}/context.txt"
            done

    fi
    echo "Extracting Instances done"
fi
