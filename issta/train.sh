#!/bin/bash
lang=$1
date=$2
gpu_id=$3
train_script=$4



export FAIRSEQPY=/home/tlutelli/fairseq-context/

source activate pytorch
export CUDA_VISIBLE_DEVICES="${gpu_id}"
echo "Start training"
#model_dir="${fairseq_repo}"/"${train_script}"
#train_dir="${fairseq_repo}"/"${context}"/train/bin
python -m source.training.autoscript."${lang}""${date}"."${train_script}"
echo "End training"
