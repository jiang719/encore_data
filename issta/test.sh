# bash test.sh
# nocontext
# quixbugs (or defects4j)
# /local/tlutelli/issta_data/fairseq-data/ghtorrent_2007/java/2006/nocontext/test/
# /local/tlutelli/issta_data/fairseq-data/ghtorrent_2007/java/2006/nocontext/train/bin/dict.src.txt
# /local/tlutelli/issta_data/fairseq-data/ghtorrent_2007/java/2006/nocontext/train/bin/dict.trg.txt
# /local/tlutelli/issta_data/fairseq-data/ghtorrent_2007/java/2006/fconv_tuned_2/
# checkpoint1.pt
# /local/tlutelli/issta_data/fairseq-data/ghtorrent_2007/java/2006/nocontext/d4j_results/fconv_tuned_2

context=$1
testdata=$2
save_tokenized=$3
src_dict=$4
trg_dict=$5
model_dir=$6
model=$7
result_dir=$8
gpu_id=$9
identifier=${10}

export CUDA_VISIBLE_DEVICES="${gpu_id}"
FAIRSEQPY=/home/n44jiang/python-workspace/DeepRepair/fairseq-context

python -m source.testing.prepare_test_data "${save_tokenized}" "${testdata}" "${context}"
echo "***************Prepare Test Data Finished******************"

bash source/testing/get_testing_data.sh process "${src_dict}" "${trg_dict}" "${save_tokenized}" "${save_tokenized}"
echo "***************Get Testing Data Finished*******************"

#exit 1
output_dir="${result_dir}"/"${model}"
model_path="${model_dir}""${model}"
target="${save_tokenized}"/bin
input_file="${save_tokenized}"/test.src
beam=1000

mkdir -p $output_dir

#:<<!
if [ "${identifier}" == "identifier" ]; then
	if [ "${context}" == "context" ]; then
		echo "identifier context"
		python "${FAIRSEQPY}"/generate.py -s src -t trg --use-context --use-identifier --path "${model_path}" --beam $beam --nbest "${beam}" "${target}" --max-tokens 50 < "${input_file}" > "${output_dir}"/output.tok.nbest.txt
	else
		echo "identifier nocontext"
		python "${FAIRSEQPY}"/generate.py -s src -t trg --use-identifier --path "${model_path}" --beam $beam --nbest "${beam}" "${target}" --max-tokens 50 < "${input_file}" > "${output_dir}"/output.tok.nbest.txt
	fi

elif [ "${context}" == "context" ]; then
	echo "context"
	python "${FAIRSEQPY}"/generate.py -s src -t trg --use-context --path "${model_path}" --beam $beam --nbest "${beam}" "${target}" --max-tokens 50 < "${input_file}" > "${output_dir}"/output.tok.nbest.txt

else
	echo "nocontext"
	python "${FAIRSEQPY}"/generate.py -s src -t trg --path "${model_path}" --beam $beam  --nbest "${beam}" "${target}" --max-tokens 50  < "${input_file}" > "${output_dir}"/output.tok.nbest.txt

# getting best hypotheses
#cat $output_dir/output.tok.nbest.txt | grep "^H"  | python -c "import sys; x = sys.stdin.readlines(); x = ' '.join([ x[i] for i in range(len(x)) if(i%$beam == 0) ]); print(x)" | cut -f3 > $output_dir/output.tok.txt

fi
#!