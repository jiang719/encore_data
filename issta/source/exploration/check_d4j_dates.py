from source.utils.defects4j_wrapper import get_info

projects = {'Chart': 26, 'Closure': 176, 'Lang': 65, 'Math': 106, 'Mockito': 38, 'Time': 27}

def get_all_dates():
    for project in projects:
        for id in range(1, projects[project] + 1):

            out, err = get_info(project, id)
            print(out.decode())
            exit()
            lines = out.decode().split('\n')
            for idx, line in enumerate(lines):
                if line.startswith("Revision date"):
                    print(project + "_" + str(id) + "\t" + lines[idx+1])

get_all_dates()
