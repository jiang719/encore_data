#!/bin/bash
loop=1


while [ "${loop}" -lt 1000000 ]
do
    echo $loop
    curl "https://gitlab.com/api/v4/projects/?private_token=zEy9BcS7CsRAiXPrRLFK&with_programming_language=java&order_by=created_at&visibility=public&per_page=100&sort=asc&page=$loop" > temp"${loop}".json
    arr=$(jq -r '.[].http_url_to_repo' temp"${loop}".json)
    echo $arr | tr " " "\n" >> all_gitlab.sh
    loop=$(( $loop + 1 ))
    if ! (( "${loop}" % 300 )); then
        echo "Waiting for one minute"
        sleep 1m
    fi
done

