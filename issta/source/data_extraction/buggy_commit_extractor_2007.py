import sys
import os
import source.utils.git_wrapper as git_wrapper
import source.data_extraction.commit_checker as commit_checker
import datetime

'''
languages supported = ['c', 'java', 'javascript', 'python']
'''


def get_commits(lang):
    """
    This functions finds all commits we consider buggy for a language and write the output in a file
    :return: write output in /data/XXX/commits_meta_lines.txt
    """
    init_dir = os.getcwd()
    project_repos = '/local/tlutelli/issta_data/repos/' + lang + '2007/'
    projects = os.listdir(project_repos)
    progress_count = 0.
    for project in projects:
        project_path = project_repos + project
        if os.path.isdir(project_path):  # check if the repo exist
            os.chdir(project_path)
            git_wrapper.pull_project(project_path)
            commits = git_wrapper.get_list_commit(project_path)
            os.chdir(init_dir)
            progress_count += 1.
            print("Project: " + project)
            print(progress_count / len(projects))
            for commit in commits:
                os.chdir(project_path)
                if commit_checker.is_fix(commit, project, project_repos):
                    # Get commit time:
                    date = git_wrapper.get_commit_date(commit, project, project_path)
                    d = date.decode().split(" ")[0].split("-")[0:3]
                    #select correct meta_file:
                    #print(date.decode())
                    if int(d[0]) < 2006:
                        meta_file = '/local/tlutelli/issta_data/data/' + lang + '/2007/all_commits_meta_lines.txt'
                        #print(meta_file)
                        output = git_wrapper.get_files_changed(commit)
                        os.chdir(init_dir)
                        filenames = str(output, 'utf-8').split('\n')
                        for filename in filenames:
                            if (lang == 'java' and commit_checker.is_java(filename)) or \
                                    (lang == 'c' and commit_checker.is_cpp(filename)) or \
                                    (lang == 'javascript' and commit_checker.is_javascript(filename)) or \
                                    (lang == 'python' and commit_checker.is_python(filename)):
                                with open(meta_file, 'a') as fin:
                                    fin.write(commit + ',' + filename + ',' + project + ',' + date.decode())



get_commits(sys.argv[1])
