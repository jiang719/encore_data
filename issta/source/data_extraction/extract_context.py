from source.data_extraction.function import Function
import source.tokenization.tokenization as tokenization
import pickle
import sys
import os
import multiprocessing
import difflib


class Defect(object):
    commit_id = ""
    file_name = ""
    buggy = b""
    clean = b""
    context = b""

    def __init__(self, commit_id, file_name, buggy, clean, context):
        self.commit_id = commit_id
        self.file_name = file_name
        self.buggy = buggy
        self.clean = clean
        self.context = context

def load_pickled_function(pickle_path):
    with open(pickle_path, 'rb') as handle:
        function_dict = pickle.load(handle)
    return function_dict


def analyze_function(func, key):
    defect_list = []
    bug = func.buggy_function.strip().splitlines()
    clean = func.clean_function.strip().splitlines()

    start_change = False
    buggy = ""
    for line in difflib.unified_diff(bug, clean, fromfile='file1', tofile='file2', lineterm='', n=0):
        #print(line)
        if line.startswith("@@"):
            if buggy:
                #print("RESULT: " + buggy)
                defect_list.append(Defect(func.commit_id, func.file_name, buggy, clean, func.buggy_function))
            buggy = ""
            clean = ""
            start_change = True
        if line.startswith('-') and not line.startswith('---'):
            if '//' in line:
                line = line.split('//')[0]
            buggy += line[1:].strip() + ' '
        if line.startswith('+') and not line.startswith('+++'):
            if '//' in line:
                line = line.split('//')[0]
            clean += line[1:].strip() + ' '
    #print("RESULT: " + buggy)
    defect_list.append(Defect(func.commit_id, func.file_name, buggy, clean, func.buggy_function))
    return defect_list


def multi_analyze(lang, date, data_repo, intermediate_repo, i):
    fout = open(data_repo + '/' + lang + '/' + date +'/' + str(i) + 'rem.txt', 'w')
    fout0 = open(data_repo + '/' + lang + '/' + date + '/' + str(i) + 'meta.txt', 'w')
    fout1 = open(data_repo + '/' + lang + '/' + date + '/' + str(i) + 'context.txt', 'w')
    fout2 = open(data_repo + '/' + lang + '/' + date + '/' + str(i) + 'add.txt', 'w')
    print("Start_loading: ", i)
    count = 0
    dict_function = load_pickled_function(intermediate_repo + '/' + lang + '_' + date + '_pickle_func/functions' + str(i) + '.pkl')
    print("# of functions" + str(len(dict_function)))
    for key, function in dict_function.items():
        if function.buggy_function and function.clean_function and function.buggy_function != function.clean_function:
            defect_list = analyze_function(function, key)
            for defect in defect_list:
                if isinstance(defect.buggy, str) and isinstance(defect.context, str) and isinstance(defect.clean, str):
                    fout.write(defect.buggy.replace('\n','') + '\n')
                    fout0.write(defect.commit_id + '\t' + defect.file_name + '\n')
                    fout1.write(defect.context.replace('\n','') + '\n')
                    fout2.write(defect.clean.replace('\n','') + '\n')


def main(lang, date, data_repo, intermediate_repo):
    fout = data_repo + '/' + lang + '/' + date +'/' + 'rem.txt'
    fout0 = data_repo + '/' + lang + '/' + date + '/' + 'meta.txt'
    fout1 = data_repo + '/' + lang + '/' + date + '/' + 'context.txt'
    fout2 = data_repo + '/' + lang + '/' + date + '/' + 'add.txt'
    num_files = len([name for name in os.listdir(intermediate_repo + '/' + lang + '_' + date + '_pickle_func/') if os.path.isfile(os.path.join(intermediate_repo + '/' + lang + '_' + date + '_pickle_func/',name))])
    jobs = []
    for i in range(0, num_files):
        p = multiprocessing.Process(target=multi_analyze, args=(lang, date, data_repo, intermediate_repo, i))
        jobs.append(p)
    for idx, proc in enumerate(jobs):
        proc.start()
        if idx > 0 and idx % 4 == 0:
            #jobs[idx-5].join()
            jobs[idx-4].join()
            jobs[idx-3].join()
            jobs[idx-2].join()
            jobs[idx-1].join()
            jobs[idx].join()
    for job in jobs:
        job.join()
    with open(fout, 'w') as outfile:
        for i in range(0, num_files):
            with open(data_repo + lang + '/' + date +'/' + str(i) + 'rem.txt','r') as infile:
                for line in infile:
                    outfile.write(line)
            os.remove(data_repo  + lang + '/' + date +'/' + str(i) + 'rem.txt')
    with open(fout0, 'w') as outfile0:
        for i in range(0, num_files):
            with open(data_repo  + lang + '/' + date +'/' + str(i) + 'meta.txt','r') as infile:
                for line in infile:
                    outfile0.write(line)
            os.remove(data_repo  + lang + '/' + date +'/' + str(i) + 'meta.txt')
    with open(fout1, 'w') as outfile1:
        for i in range(0, num_files):
            with open(data_repo  + lang + '/' + date +'/' + str(i) + 'context.txt','r') as infile:
                for line in infile:
                    outfile1.write(line)
            os.remove(data_repo + lang + '/' + date +'/' + str(i) + 'context.txt')
    with open(fout2, 'w') as outfile2:
        for i in range(0, num_files):
            with open(data_repo  + lang + '/' + date +'/' + str(i) + 'add.txt','r') as infile:
                for line in infile:
                    outfile2.write(line)
            os.remove(data_repo  + lang + '/' + date +'/' + str(i) + 'add.txt')

main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
