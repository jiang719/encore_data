import os
import source.utils.git_wrapper as git_wrapper
import re


def is_java(filename):
    """
    Check if a file is a java file
    :param filename:
    :return:
    """
    if filename.lower().endswith(".java"):
        return True
    return False


def is_cpp(filename):
    """
    Check if a file is a c or cpp file
    :param filename:
    :return:
    """
    cpp_extensions = ['.c', '.cpp', '.cc', '.cp', '.cxx', '.c++', '.h', '.hpp', '.hh', '.hxx', '.h++']
    if any(filename.lower().endswith(end) for end in cpp_extensions):
        return True
    return False


def is_javascript(filename):
    """
    Check if a file is a javascript file
    :param filename:
    :return:
    """
    if filename.lower().endswith(".js"):
        return True
    return False


def is_python(filename):
    """
    Check if a file is a python file
    :param filename:
    :return:
    """
    if filename.lower().endswith(".py"):
        return True
    return False


def is_fix(commit, project, repos):
    """
    This function take a commit, a project and a path as input and check whether the commit is a bug fixing commit
    according to patterns and anti-patterns (is_bug and is_not_bug) in the commit message.
    :param commit:
    :param project:
    :param repos:
    :return: True if the commit is a bug fixing commits, False otherwise.
    """
    is_bug = ['fix', 'bug', 'patch']
    is_not_bug = ['rename', 'rewrite', 'clean up', 'refactor',
                  'merge', 'misspelling', 'compiler warning', 'javadoc', 'comment']
    os.chdir(repos+project)
    message = git_wrapper.get_commit_message(commit, project, repos)
    if any(substring in message for substring in is_bug) and not any(substring in message for substring in is_not_bug):
        return True
    return False


# We don't care about import statements.
def is_import_package(minus_line, plus_line, lang):
    if lang == 'java' or 'python' or 'javascript':
        if minus_line.startswith('import ') or plus_line.startswith('import '):
            return True
        if minus_line.startswith('package ') or plus_line.startswith('package '):
            return True
    if lang == 'python':
        if minus_line.startswith('from ') or plus_line.startswith('from '):
            return True
    if lang == 'c':
        if minus_line.startswith('#include') or plus_line.startswith('#include'):
            return True
    return False


def is_cosmetic(minus_line, plus_line):
    if minus_line.strip() == plus_line.strip():
        return True
    return False


def is_assert(minus_line, plus_line):
    if minus_line.startswith('assert') or plus_line.startswith('assert'):
        return True
    return False


# Check if the line is Deprecated.
def is_deprecated(minus_line, plus_line):
    if minus_line.startswith('@Deprecated') or plus_line.startswith('@Deprecated'):
        return True
    return False


def safe_unicode(obj, *args):
    try:
        return str(obj, *args)
    except UnicodeDecodeError:
        ascii_text = bytes(obj).encode('string_escape')
        return str(ascii_text)


# Remove Comment
# https://stackoverflow.com/questions/2319019/using-regex-to-remove-comments-from-source-files
def remove_comments(string, lang):
    if lang == 'java' or lang == 'c' or lang == 'javascript':
        string_no_comment = string.split('//')[0]
        if string.startswith('*'):
            string_no_comment = ""
        if string.startswith('/*') and '*/' not in string:
            string_no_comment = ""
        pattern = r"(\".*?\"|\'.*?\')|(/\*.*?\*/|//[^\r\n]*$)"
        # first group captures quoted strings (double or single)
        # second group captures comments (//single-line or /* multi-line */)
        regex = re.compile(pattern, re.MULTILINE|re.DOTALL)

        def _replacer(match):
            # if the 2nd group (capturing comments) is not None,
            # it means we have captured a non-quoted (real) comment string.
            if match.group(2) is not None:
                return ""  # so we will return empty to remove the comment
            else:  # otherwise, we will return the 1st group
                return match.group(1)  # captured quoted-string
        return regex.sub(_replacer, string_no_comment)

    if lang == 'python':
        if string.startswith('"""') and '"""' not in string:
            string_no_comment = ""
        elif string.startswith("'''") and "'''" not in string:
            string_no_comment = ""
        else:
            string_no_comment = string.split('#')[0]
        if '"""' not in string_no_comment and "'''" not in string_no_comment and "#" not in string_no_comment:
            return string_no_comment
        return ""
