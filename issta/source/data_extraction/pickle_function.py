from source.data_extraction.pickle_function_utils import get_functions
import pickle
import os
import multiprocessing
import sys


def multi_pickle(lang, date, path,i):
    func_list = {}
    udb_path = path + "/udb" + str(i) + ".udb"
    print("Get functions for: ", lang, date, i)

    func_list = get_functions(udb_path, func_list, lang)



    print("# of functions:" + str(len(func_list)))
    if not os.path.exists(path.rsplit('/',1)[0] + lang + '_' + date + "_pickle_func"):
        os.makedirs(path.rsplit('/',1)[0]  + lang + '_' + date + "_pickle_func")
    with open(path.rsplit('/',1)[0]  + lang + '_' + date + '_pickle_func/functions' + str(i) +'.pkl','wb') as handle:
        pickle.dump(func_list, handle, protocol=pickle.HIGHEST_PROTOCOL)
    print("\nDone for: ", lang)
    #return

def pickle_function(lang, date, intermediate_repo):
    path = intermediate_repo + '/' + lang + '_' + date + '_udb_projects'
    num_files = len([name for name in os.listdir(path) if os.path.isfile(os.path.join(path,name))])
    jobs = []
    for i in range(0, num_files):
        p = multiprocessing.Process(target=multi_pickle, args=(lang, date, path,i))
        jobs.append(p)
    for idx, proc in enumerate(jobs):
        proc.start()
        if idx > 0 and idx % 3 == 0:
            #jobs[idx-5].join()
            #jobs[idx-4].join()
            jobs[idx-3].join()
            jobs[idx-2].join()
            jobs[idx-1].join()
            jobs[idx].join()



pickle_function(sys.argv[1], sys.argv[2], sys.argv[3])
