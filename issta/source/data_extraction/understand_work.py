import os
import understand
import pickle
import shutil
from source.data_extraction.function import Function
import subprocess
import sys
import logging
import multiprocessing

UNDERSTAND_PATH = '/local/tlutelli/scitools/bin/linux64/'


def chunks(l, n):
    """Yield succesive n-sized chunks from 1."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def list_immediate_subdir(root):
    """"
    List immediate subdir. The goal is to get the list of bugs
    :param root:
    :return:
    """
    return [name for name in os.listdir(root)
            if os.path.isdir(os.path.join(root, name))]


def list_all_files(repo):
    list_files = []
    for path, subdirs, files in os.walk(repo):
        for name in files:
            list_files.append(os.path.join(path, name))
    return list_files


def generate_file(elem):
    try:
        typ = elem.kindname()
        if typ == 'File':
            return elem
        else:
            return generate_file(elem.parent())
    except:
        return None

def generate_key(file_ent,func):
    key = file_ent.longname().split('_temp/', 1)[-1].replace('/clean/','/').replace('/buggy/','/') + '/' + func.name()
    return key


def generate_commit_id(file_ent):
    commit_id = file_ent.longname().split('_temp/',1)[-1].split('/')[1]
    return commit_id


def create_udb(udb_path, language, file_list):
    if language == 'javascript':
        language = 'web'
    if language == 'c':
        language = 'c++'
    try:
        output = subprocess.check_output(UNDERSTAND_PATH + "und create -db {udb_path} -languages {lang}".format(udb_path=udb_path, lang=language), shell=True)
        logging.info(output)
        output = subprocess.check_output(UNDERSTAND_PATH + "und add -db {udb_path} @{file_list}".format(udb_path=udb_path, file_list=file_list), shell=True)
        logging.info(output)

    except subprocess.CalledProcessError as e:
        logging.exception(e.output)
        logging.fatal("udb creation failed")
        #raise Exception


def analyze_udb(udb_path):
    try:
        output = subprocess.check_output(UNDERSTAND_PATH + "und analyze -all {udb_path}".format(udb_path=udb_path), shell=True)
        logging.info(output)
    except subprocess.CalledProcessError as e:
        logging.exception(e.output)
        logging.fatal("udb analyzis failed")
        #raise Exception

def create_analyze(params):
    lang = params[0]
    output_dir = params[1]
    in_dir = params[2] 
    i = params[3]
    date = params[4]
    print(date + " Start project creation of Chunk " + str(i) + " Done.")
    create_udb(output_dir + '_udb_projects/udb' + str(i) + '.udb', lang, in_dir + '_understand_chunks/chunk' + str(i) + '.txt')
    print(date + " Project creation of Chunk " + str(i) + " Done.")
    analyze_udb(output_dir + '_udb_projects/udb' + str(i) + '.udb')
    print(date + " Analyzis of Chunk " + str(i) + " Done.")

def create_and_analyze(lang, output_dir, in_dir, num_chunk, date):
   
    if not os.path.exists(output_dir + "_udb_projects"):
        os.makedirs(output_dir  + "_udb_projects")
    all_commands = []
    
    for i in range(0, num_chunk + 1):
        all_commands.append([lang, output_dir, in_dir,i, date])

    pool = multiprocessing.Pool(10)
    result = pool.map(create_analyze, all_commands)
    

def get_functions(db_path, dict_function, lang):
    db = understand.open(db_path)
    # Get all file names:
    file_names = db.ents("File")
    all_file_names = {}
    for f in file_names:
        if "/" + lang + "_temp" in f.longname():
            all_file_names[f.longname()] = f
    ### Take care of bugs inside of functions:
    ents = db.ents("function,method,procedure")
    for func in ents:
        file_ent = generate_file(func)
        if file_ent is None:
            continue
        if "/" + lang + "_temp" not in file_ent.longname():
            continue
        dict_key = generate_key(file_ent, func)
        #print(file_ent)
        #print(dict_key)
        file_name = file_ent.longname()
        if file_name in all_file_names:
                del all_file_names[file_name]
        if dict_key not in dict_function:
            commit_id = generate_commit_id(file_ent)
            #print(commit_id)
            #print(file_name)
            #return

            if "/clean/" in file_name:
                clean_function = func.contents()
                new_function = Function(commit_id, file_name, clean=clean_function)
            elif "/buggy/" in file_name:
                buggy_function = func.contents()
                new_function = Function(commit_id, file_name, buggy=buggy_function)
            else:
                print("ERROR", file_ent.longname() + '\n')
            dict_function[dict_key] = new_function
        else:
            if "/clean/" in file_name:
                if dict_function[dict_key].clean_function is None:
                    dict_function[dict_key].clean_function = func.contents()
            elif "/buggy/" in file_name:
                if dict_function[dict_key].buggy_function is None:
                    dict_function[dict_key].buggy_function = func.contents()
    ### Take care of bugs outside of functions:
    print("Working on bugs outside of functions: ", len(all_file_names))
    print(len(dict_function))
    for f in all_file_names:
        func = all_file_names[f]
        file_ent = func
        if file_ent is None:
            print("Error")
            continue
        if "/" + lang + "_temp" not in file_ent.longname():
            continue
        dict_key = generate_key(file_ent, func)
        #print(file_ent)
        #print(dict_key)
        file_name = file_ent.longname()
        if dict_key not in dict_function:
            commit_id = generate_commit_id(file_ent)

            if "/clean/" in file_name:
                clean_function = func.contents()
                new_function = Function(commit_id, file_name, clean=clean_function)
            elif "/buggy/" in file_name:
                buggy_function = func.contents()
                new_function = Function(commit_id, file_name, buggy=buggy_function)
            else:
                print("ERROR", file_ent.longname() + '\n')
            dict_function[dict_key] = new_function
        else:
            if "/clean/" in file_name:
                if dict_function[dict_key].clean_function is None:
                    dict_function[dict_key].clean_function = func.contents()
            elif "/buggy/" in file_name:
                if dict_function[dict_key].buggy_function is None:
                    dict_function[dict_key].buggy_function = func.contents()
    print(len(dict_function))
    return dict_function


def get_understand_function(lang):
    for i in range(0, 500):
        func_list = {}
        udb_path = lang + "_udb_projects/udb" + str(i) + ".udb"
        if not os.path.exists(lang + "_udb_projects"):
            os.makedirs(lang + "_udb_projects")
        func_list = get_functions(udb_path, func_list, lang)
        print(len(func_list))
        with open(lang + '_pickle_func/functions' + str(i) + '.pkl', 'wb') as handle:
            pickle.dump(func_list, handle, protocol=pickle.HIGHEST_PROTOCOL)


def pickle_function(lang):
    for i in range(0, 500):
        func_list = {}
        udb_path = lang + "_udb_projects/udb" + str(i) + ".udb"
        func_list = get_functions(udb_path, func_list, lang)
        print(len(func_list))
        if not os.path.exists(lang + "_pickle_func"):
            os.makedirs(lang + "_pickle_func")
        with open(lang + '_pickle_func/functions' + str(i) +'.pkl','wb') as handle:
            pickle.dump(func_list, handle, protocol=pickle.HIGHEST_PROTOCOL)
        #print("\nDone for: ", lang)
        #return

def main(lang, date, temp_repo, intermediate_repo):
    init_dir = os.getcwd()
    data_dir = temp_repo
    result_dir = intermediate_repo
    list_dir = list_immediate_subdir(data_dir + '/' + lang + '/' + date + '_temp/')
    print(date + "Loading Done")
    # split in 10 chunks
    #print(list_dir)
    all_inst = []
    for inst in list_dir:
        all_inst += list_all_files(os.path.join(data_dir  + lang + '/' + date + '_temp/', inst))
    partial_lists = chunks(all_inst, 10000)
    if not os.path.exists(result_dir + lang + '_' + date  + "_understand_chunks"):
        os.makedirs(result_dir + lang + '_' + date  + "_understand_chunks")
    num_chunk = 0
    for i, partial_list_files in enumerate(partial_lists):
        fout= open(result_dir + lang + '_' + date  + "_understand_chunks/chunk" + str(i) + ".txt", 'w')
        for f in partial_list_files:
                fout.write(f + '\n')
        print(date + " Chunk " + str(i) + " Done.")
        num_chunk = i
    create_and_analyze(lang, result_dir + lang + '_' + date, result_dir + lang + '_' + date, num_chunk, date)


main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])

#create_and_analyze('python')


#pickle_function('java')
#DONE #pickle_function('python')
#pickle_function('javascript')
