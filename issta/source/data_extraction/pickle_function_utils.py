from source.data_extraction.function import Function
import understand


def generate_commit_id(file_ent):
    commit_id = file_ent.longname().split('_temp/',1)[-1].split('/')[1]
    return commit_id


def generate_file(elem):
    try:
        typ = elem.kindname()
        if typ == 'File':
            return elem
        else:
            return generate_file(elem.parent())
    except:
        return None


def generate_key(file_ent,func):
    key = file_ent.longname().split('_temp/', 1)[-1].replace('/clean/','/').replace('/buggy/','/') + '/' + func.name()
    return key


def get_functions(db_path, dict_function, lang):
    db = understand.open(db_path)
    # Get all file names:
    file_names = db.ents("File")
    all_file_names = {}
    for f in file_names:
        if "_temp/" in f.longname():
            all_file_names[f.longname()] = f
    ### Take care of bugs inside of functions:
    ents = db.ents("function,method,procedure")
    for func in ents:
        file_ent = generate_file(func)
        if file_ent is None:
            continue
        if "_temp/" not in file_ent.longname():
            continue
        dict_key = generate_key(file_ent, func)
        #print(file_ent)
        #print(dict_key)
        file_name = file_ent.longname()
        if file_name in all_file_names:
                del all_file_names[file_name]
        if dict_key not in dict_function:
            commit_id = generate_commit_id(file_ent)
            #print(commit_id)
            #print(file_name)
            #return

            if "/clean/" in file_name:
                clean_function = func.contents()
                new_function = Function(commit_id, file_name, clean=clean_function)
            elif "/buggy/" in file_name:
                buggy_function = func.contents()
                new_function = Function(commit_id, file_name, buggy=buggy_function)
            else:
                print("ERROR", file_ent.longname() + '\n')
            dict_function[dict_key] = new_function
        else:
            if "/clean/" in file_name:
                if dict_function[dict_key].clean_function is None:
                    dict_function[dict_key].clean_function = func.contents()
            elif "/buggy/" in file_name:
                if dict_function[dict_key].buggy_function is None:
                    dict_function[dict_key].buggy_function = func.contents()
    ### Take care of bugs outside of functions:
    '''
    print("Working on bugs outside of functions: ", len(all_file_names))
    print(len(dict_function))
    for f in all_file_names:
        func = all_file_names[f]
        file_ent = func
        if file_ent is None:
            print("Error")
            continue
        if "/" + lang + "_temp" not in file_ent.longname():
            continue
        dict_key = generate_key(file_ent, func)
        #print(file_ent)
        #print(dict_key)
        file_name = file_ent.longname()
        if dict_key not in dict_function:
            commit_id = generate_commit_id(file_ent)

            if "/clean/" in file_name:
                clean_function = func.contents()
                new_function = Function(commit_id, file_name, clean=clean_function)
            elif "/buggy/" in file_name:
                buggy_function = func.contents()
                new_function = Function(commit_id, file_name, buggy=buggy_function)
            else:
                print("ERROR", file_ent.longname() + '\n')
            dict_function[dict_key] = new_function
        else:
            if "/clean/" in file_name:
                if dict_function[dict_key].clean_function is None:
                    dict_function[dict_key].clean_function = func.contents()
            elif "/buggy/" in file_name:
                if dict_function[dict_key].buggy_function is None:
                    dict_function[dict_key].buggy_function = func.contents()
    '''
    return dict_function



