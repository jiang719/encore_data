import sys
import os
import time
import source.utils.git_wrapper as git_wrapper
import source.data_extraction.commit_checker as commit_checker
import datetime
import shutil
import multiprocessing

'''
languages supported = ['c', 'java', 'javascript', 'python']
'''

def multi_commits(lang, project_repos, data_repo, project, init_dir):
    useful = 0
    project_path = project_repos + project
    if os.path.isdir(project_path):  # check if the repo exist
        os.chdir(project_path)
        git_wrapper.pull_project(project_path)
        commits = git_wrapper.get_list_commit(project_path)
        os.chdir(init_dir)
        for commit in commits:
            os.chdir(project_path)
            if commit_checker.is_fix(commit, project, project_repos):
                # Get commit time:
                date = git_wrapper.get_commit_date(commit, project, project_path)
                d = date.decode().split(" ")[0].split("-")[0:3]
                if int(d[0]) < 2006:
                    if int(d[0]) == 2005 and int(d[1]) == 12:
                        continue
                    else:
                        if not os.path.exists(data_repo + '/' + lang + '/2005/meta/'):
                            os.makedirs(data_repo + '/' + lang + '/2005/meta/')
                        meta_file = data_repo + '/' + lang + '/2005/meta/' + project + '_commits_meta_lines.txt'
                        output = git_wrapper.get_files_changed(commit)
                        os.chdir(init_dir)
                        filenames = str(output, 'utf-8').split('\n')
                        for filename in filenames:
                            if (lang == 'java' and commit_checker.is_java(filename)) or \
                                    (lang == 'c' and commit_checker.is_cpp(filename)) or \
                                    (lang == 'javascript' and commit_checker.is_javascript(filename)) or \
                                    (lang == 'python' and commit_checker.is_python(filename)):
                                useful = 1
                                with open(meta_file, 'a') as fin:
                                    fin.write(commit + ',' + filename + ',' + project + ',' + date.decode())
            #if useful == 0:
            #    shutil.rmtree(project_path)

    print(project, ": Done")

def get_commits(lang, project_repo, data_repo):
    """
    This functions finds all commits we consider buggy for a language and write the output in a file
    :return: write output in /data/XXX/commits_meta_lines.txt
    """
    init_dir = os.getcwd()
    project_repos = project_repo
    projects = os.listdir(project_repos)
    progress_count = 0.
    jobs = []
    for project in projects:
        p = multiprocessing.Process(target=multi_commits, args=(lang, project_repos, data_repo, project, init_dir))
        jobs.append(p)
    for idx, proc in enumerate(jobs):
        proc.start()
        if idx > 0 and idx % 30 == 0:
            jobs[idx-30].join()
            time.sleep(1)
            #for j in jobs[idx-100:idx]:
            #    jobs[j].join()
            progress_count += 30.
            print(progress_count / len(projects))





get_commits(sys.argv[1], sys.argv[2], sys.argv[3])
