#!/bin/bash
input="/local/tlutelli/issta_data/ghtorrent_2007/results-20191209-151331.csv"
repos_counter=1
while IFS= read -r line
do
  cd /local/tlutelli/issta_data/repos/java2007
  git clone $line $repos_counter ;
  echo $line $repos_counter >> /home/tlutelli/ENCORE_data/issta/results/mapping_repos_idx.txt
  ((repos_counter++))
done < "$input"

