loop=1
next="https://api.bitbucket.org/2.0/repositories?pagelen=100"

while [ "${loop}" -lt 1000000 ]
    do
        echo $next
        curl "${next}" > /home/tlutelli/ENCORE_data/issta/bitbucket/temp"${loop}".json
        next=$( cat /home/tlutelli/ENCORE_data/issta/bitbucket/temp"${loop}".json | rev | cut -f 1  -d ':' | rev )
        next=https:"${next::-2}"

        loop=$(( $loop + 1 ))
    done

