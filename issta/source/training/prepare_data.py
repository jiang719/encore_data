import os
import subprocess
from source.training.split_remadd import split_exclude
import sys


def main(language, train_dir, fairseq_dir):
    fairseq = os.environ['FAIRSEQPY']
    print("Pre-processing data for :", language)
    if not os.path.exists(fairseq_dir):
        os.makedirs(fairseq_dir)
    split_exclude(train_dir + 'exclude.txt', train_dir + '_train_remadd.txt',  train_dir + '_train_meta.txt',
                    fairseq_dir + 'train.src', fairseq_dir + 'train.trg', fairseq_dir + 'train.meta')
    split_exclude(train_dir + 'exclude.txt', train_dir + '_valid_remadd.txt',  train_dir + '_valid_meta.txt',
                    fairseq_dir + 'valid.src', fairseq_dir + 'valid.trg', fairseq_dir + 'valid.meta')
    cmd = 'python ' + fairseq + 'preprocess.py ' + '--source-lang src --target-lang trg --workers 40 ' + \
        '--trainpref ' + fairseq_dir + 'train ' + \
        '--validpref ' + fairseq_dir + 'valid ' + \
        '--testpref ' + fairseq_dir + 'valid ' + \
        '--destdir ' + fairseq_dir + 'bin'
    subprocess.call(cmd, shell=True)


def main_context(language, train_dir, fairseq_dir):

    fairseq = os.environ['FAIRSEQPY']
    print("Pre-processing data for :", language)
    if not os.path.exists(fairseq_dir):
        os.makedirs(fairseq_dir)
    split_exclude(train_dir + 'exclude.txt', train_dir + '_train_remadd.txt',  train_dir + '_train_meta.txt',
                    fairseq_dir + 'train.src', fairseq_dir + 'train.trg', fairseq_dir + 'train.meta')
    split_exclude(train_dir + 'exclude.txt', train_dir + '_valid_remadd.txt',  train_dir + '_valid_meta.txt',
                    fairseq_dir + 'valid.src', fairseq_dir + 'valid.trg', fairseq_dir + 'valid.meta')
    cmd = 'python ' + fairseq + 'preprocess.py ' + '--source-lang src --target-lang trg --workers 40 ' + \
        '--trainpref ' + fairseq_dir + 'train ' + \
        '--validpref ' + fairseq_dir + 'valid ' + \
        '--testpref ' + fairseq_dir + 'valid ' + \
        '--destdir ' + fairseq_dir + 'bin'
    subprocess.call(cmd, shell=True)


def main_context_one_input():
    fairseq = os.environ['FAIRSEQPY']
    languages = ['java']
    init_dir = os.getcwd()
    for language in languages:
        print("Pre-processing data for :", language)
        data_dir = init_dir + '/data/' + language + '/'
        train_dir = data_dir + 'context/train/'
        fairseq_dir = init_dir + '/fairseq-data/' + language + '/contextoneinput/train/'
        if not os.path.exists(fairseq_dir):
            os.makedirs(fairseq_dir)
        split_exclude(data_dir + 'exclude.txt', train_dir + '_train_remadd.txt',  train_dir + '_train_meta.txt',
                      fairseq_dir + 'train.src', fairseq_dir + 'train.trg', fairseq_dir + 'train.meta')
        split_exclude(data_dir + 'exclude.txt', train_dir + '_valid_remadd.txt',  train_dir + '_valid_meta.txt',
                      fairseq_dir + 'valid.src', fairseq_dir + 'valid.trg', fairseq_dir + 'valid.meta')
        split_exclude(data_dir + 'exclude.txt', train_dir + '_test_remadd.txt',  train_dir + '_test_meta.txt',
                      fairseq_dir + 'test.src', fairseq_dir + 'test.trg', fairseq_dir + 'test.meta')
        cmd = 'python ' + fairseq + 'preprocess.py ' + '--source-lang src --target-lang trg --workers 40 ' + \
            '--trainpref ' + fairseq_dir + 'train ' + \
            '--validpref ' + fairseq_dir + 'valid ' + \
            '--testpref ' + fairseq_dir + 'test ' + \
            '--destdir ' + fairseq_dir + 'bin'
        subprocess.call(cmd, shell=True)


if sys.argv[4] == 'nocontext':
    print("True")
    train_dir = sys.argv[2] + '/nocontext/train/'
    fairseq_dir =  sys.argv[3] + '/nocontext/train/'
    main(sys.argv[1], train_dir, fairseq_dir)
if sys.argv[4] == 'context':
    train_dir = sys.argv[2] + '/context/train/'
    fairseq_dir =  sys.argv[3] + '/context/train/'
    main_context(sys.argv[1], train_dir, fairseq_dir)


#main()
#main_context_one_input()

