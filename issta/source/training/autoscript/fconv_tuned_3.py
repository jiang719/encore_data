from source.training.train import train_fconv
import sys

model_dir = sys.argv[1]
train_dir = sys.argv[2]


train_fconv(0.21442252259711814,True,201,201,201,'[(128,7)] * 5','[(512,8)] * 1',0.24652713900278644,0.9589175875539746,0.16150931866182217,'adagrad','label_smoothed_cross_entropy',model_dir,train_dir)
