import source.testing.generate_split as generate_split
import sys


def perfect_data_all(saved_data_path):
    both_path = ''
    rem_path = '/home/tlutelli/issta_data/data/java/defects4j/valid/rem.txt'
    add_path  = '/home/tlutelli/issta_data/data/java/defects4j/valid/add.txt'
    meta_path = '/home/tlutelli/issta_data/data/java/defects4j/valid/meta.txt'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split(both_path, rem_path, add_path, meta_path, saved_data_path, valid_size, perfect_loc, only_test)
    #generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)


def perfect_data_all_context(saved_data_path):
    both_path = ''
    rem_path = '/home/tlutelli/issta_data/data/java/defects4j/valid/rem.txt'
    add_path  = '/home/tlutelli/issta_data/data/java/defects4j/valid/add.txt'
    meta_path = '/home/tlutelli/issta_data/data/java/defects4j/valid/meta.txt'
    context_path = '/home/tlutelli/issta_data/data/java/defects4j/valid/context.txt'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split_context(both_path, rem_path, add_path, meta_path, context_path, saved_data_path, valid_size, perfect_loc, only_test)
    #generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)


#perfect_data_javascript()
#perfect_data_genprog()
#perfect_data_codeflaw_651()
#perfect_data_all_context()
#perfect_data_python_quixbug()
if sys.argv[2] == "nocontext":
    perfect_data_all(sys.argv[1])
if sys.argv[2] == "context":
    perfect_data_all_context(sys.argv[1])
