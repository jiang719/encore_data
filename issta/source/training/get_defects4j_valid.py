from source.utils.defects4j_wrapper import get_info
import datetime

projects = {'Chart': 26, 'Closure': 176, 'Lang': 65, 'Math': 106, 'Mockito': 38, 'Time': 27}

old_projects = []

def get_all_dates():
    for project in projects:
        for id in range(1, projects[project] + 1):

            out, err = get_info(project, id)
            lines = out.decode().split('\n')
            for idx, line in enumerate(lines):
                if line.startswith("Revision date"):
                    if int(lines[idx+1].split('-')[0]) < 2011:
                        old_projects.append(project + '_' + str(id))
                        
                        
get_all_dates()


print(old_projects)

D4J_DIR="/home/tlutelli/issta_data/data/java/defects4j/nocontext/"
VALID_D4J_DIR="/home/tlutelli/issta_data/data/java/defects4j/valid/"

old_line_idx = []

meta = open(D4J_DIR+'meta.txt', 'r').readlines()
add = open(D4J_DIR+'add.txt', 'r').readlines()
rem = open(D4J_DIR+'rem.txt', 'r').readlines()
context = open(D4J_DIR+'context.txt', 'r').readlines()



new_meta = open(VALID_D4J_DIR+'meta.txt', 'w')
new_add = open(VALID_D4J_DIR+'add.txt', 'w')
new_rem = open(VALID_D4J_DIR+'rem.txt', 'w')
new_context = open(VALID_D4J_DIR+'context.txt', 'w')

for idx, line in enumerate(meta):
    li = line.split('\t')
    print(li)
    if li[0] + '_' + li[1] in old_projects:
        old_line_idx.append(idx)
        new_meta.write(line)
        new_add.write(add[idx])
        new_rem.write(rem[idx])
        new_context.write(context[idx])





add        
    

# read meta:
# if date  before 2011-01-01
#      put it in a separation
