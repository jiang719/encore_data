import os
import subprocess
import shutil
from source.utils.runcmd import RunCmd
DEFECTS4J_PATH = '/local/tlutelli/defects4j/framework/bin/'


def checkout_project(project, bug_id, temp_dir):
    """
    Checkout a defect4j bug
    :param project: project name
    :param bug_id:  bug id (number) + b for buggy or f for fix (e.g. 4b or 4f)
    :param temp_dir: temporary directory where to checkout the project.
    :return:
    """
    FNULL = open(os.devnull, 'w')
    command = DEFECTS4J_PATH + "defects4j " + " checkout " + " -p " + project + " -v " + bug_id + " -w " + temp_dir
    p = subprocess.Popen([command], shell=True, stdout=FNULL, stderr=FNULL)
    p.wait()
    return True


def clean_temp_folder(temp_dir):
    """
    :param temp_dir: temporary directory where the patch will be compiled
    :return: nothing
    """
    if os.path.isdir(temp_dir):
        for files in os.listdir(temp_dir):
            file_p = os.path.join(temp_dir, files)
            try:
                if os.path.isfile(file_p):
                    os.unlink(file_p)
                elif os.path.isdir(file_p):
                    shutil.rmtree(file_p)
            except Exception as e:
                print(e)
    else:
        os.makedirs(temp_dir)


def compile_fix(temp_folder):
    """
    Compile an already checkout project.
    :param temp_folder:
    :return:
    """
    init = os.getcwd()
    os.chdir(temp_folder)
    p = subprocess.Popen([DEFECTS4J_PATH + "defects4j", "compile"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    if "FAIL" in str(err) or "FAIL" in str(out):
        res = False
    else:
        res = True
    os.chdir(init)
    return res


def defects4j_test_suite(temp_folder):
    """
    Run full test suite
    :param temp_folder:
    :return:
    """
    init = os.getcwd()
    os.chdir(temp_folder)
    out, err = RunCmd([DEFECTS4J_PATH + "defects4j", "test"], 300).Run()
    os.chdir(init)
    return out, err


def run_individual_test_case(temp_folder, test_case):
    """
    run individual test suite
    :param temp_folder:
    :param test_case:
    :return:
    """
    out, err = RunCmd([DEFECTS4J_PATH + "defects4j", "test", '-w', temp_folder, '-t', test_case], 300).Run()
    return out, err


def extract_failed_test_cases(string_test_cases):
    """
    Extract fail test cases from output of defects4j_test_suite
    :param string_test_cases:
    :return:
    """
    test_cases = string_test_cases.replace("''", '').replace('\\n', '').split(" - ")[1:]
    return test_cases


def get_info(project, bug_id):
    """
    Return info about a specific project
    """
    p = subprocess.Popen([DEFECTS4J_PATH + "defects4j", "info", "-p", project, "-b", str(bug_id)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    return out, err
    out, err = RunCmd([DEFECTS4J_PATH + "defects4j", "info", "-p", project, "-b", str(bug_id)], 1000).Run()
    return out, err

