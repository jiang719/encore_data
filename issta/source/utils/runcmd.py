import threading
import subprocess
import psutil
import os
import signal


class RunCmd(threading.Thread):
    """
    Run subprocess with time out and correct kill
    Usage: RunCmd(["./someProg", "arg1"], 60).Run()
    """
    # https://stackoverflow.com/questions/4158502/kill-or-terminate-subprocess-when-timeout?noredirect=1
    def __init__(self, cmd, timeout):
        threading.Thread.__init__(self)
        self.cmd = cmd
        self.timeout = timeout
        self.out = "TIMEOUT"
        self.err = "TIMEOUT"

    def run(self):
        self.p = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        self.out, self.err = self.p.communicate()

    def Run(self):
        self.start()
        self.join(self.timeout)

        if self.is_alive():
            current_process = psutil.Process()
            children = current_process.children(recursive=True)
            for child in children:
                print('Killing Child pid {}'.format(child.pid))
                os.kill(child.pid, signal.SIGTERM)
                os.kill(child.pid, signal.SIGKILL)
            os.kill(self.p.pid, signal.SIGTERM)
            os.kill(self.p.pid, signal.SIGKILL)


