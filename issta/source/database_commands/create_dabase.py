import pandas as pd
import subprocess
import sys
import psycopg2
import signal
import csv
import threading
import psutil


class RunCmd(threading.Thread):
    # https://stackoverflow.com/questions/4158502/kill-or-terminate-subprocess-when-timeout?noredirect=1
    def __init__(self, cmd, timeout):
        threading.Thread.__init__(self)
        self.cmd = cmd
        self.timeout = timeout
        self.out = b"TIMEOUT"
        self.err = b"TIMEOUT"

    def run(self):
        self.p = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        self.out, self.err = self.p.communicate()

    def Run(self):
        self.start()
        self.join(self.timeout)
        return self.out, self.err

        if self.is_alive():

            current_process = psutil.Process()
            children = current_process.children(recursive=True)
            for child in children:
                print('Killing Child pid {}'.format(child.pid))
                os.kill(child.pid, signal.SIGTERM)
                os.kill(child.pid, signal.SIGKILL)
            os.kill(self.p.pid, signal.SIGTERM)
            os.kill(self.p.pid, signal.SIGKILL)


            self.p.kill()      #use self.p.kill() if process needs a kill -9
            self.join()


def create_connection():
        conn = psycopg2.connect(host="deepgpu1.eng.uwaterloo.ca",database="deeprepair", user="tlutelli", password="123")
        return conn
        #print(sqlite3.version)
        #c =  conn.cursor()
        #c.execute('''CREATE TABLE PERFECT_D4J_NO_CONTEXT ([command] text, [status] text)''')
        #conn.commit()
        #read_clients = pd.read_csv(r'database_command_dumb.csv')
        #read_clients.to_sql('PERFECT_D4J_NO_CONTEXT', conn, if_exists='append', index = False)

def select_task(conn):
    cur = conn.cursor()
    cur.execute("SELECT command FROM ISSTA_COMMANDS WHERE status='' ORDER BY RANDOM() LIMIT 1")
    rows = cur.fetchall()
    return rows


def setup(db_file):
        conn = psycopg2.connect(host="deepgpu1.eng.uwaterloo.ca",database="deeprepair", user="tlutelli", password="123")

        c = conn.cursor()
        #c.execute('''PRAGMA journal_mode = OFF''')
        c.execute('''CREATE TABLE ISSTA_COMMANDS (command text, status text)''')
        #conn.commit()
        with open('database_dumps/fconv_1_database_command_dumb.csv') as f:
            reader = csv.reader(f)
            next(reader)
            for row in reader:
                c.execute("INSERT INTO ISSTA_COMMANDS VALUES (%s, %s)", row)
        conn.commit()
        sys.exit()
        
        
if __name__ == '__main__':
    #setup(r'/home/tlutelli/ENCORE_data/issta/validation_commands.db')



    init_command = "time"
    # SELECT ONE COMMAND TO RUN
    conn = create_connection()

    while 'time' in init_command:

        with conn:
            res = select_task(conn)
        #i +=1

        for r in res:
            print(r[0])
            init_command = r[0]
        if not res:
            init_command = ""
        

        # UPDATE COMMAND TO DOING
        conn = create_connection()
        """
        with conn:
            cur = conn.cursor()
            cur.execute('''
                UPDATE ISSTA_COMMANDS
                SET status = 'DOING'
                WHERE
                command = (%s)
                ''', (init_command,))
            conn.commit()
        """
        # MODIFY COMMAND
        print(init_command)
        locs = init_command.rsplit('.java',1)[1].split('_')[0]
        #init_command = "time python -m source.compilator.d4j_compilation fairseq-data/java/fconv/perfectloc_d4j_all/Chart1source.org.jfree.chart.renderer.category.AbstractCategoryItemRenderer.java17971797_ENCORE_PERFECT_LOC.txt /local/tlutelli/_Chart_1/ 0"
        commands = init_command.split('_')
        commands[-3] = commands[-3] + locs
        command = '_'.join(commands)
        print(command)
        exit()
        # RUN COMMAND
        out, err = RunCmd(command.split(' '), 10800).Run()
        #p = subprocess.Popen(command.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        #out, err = p.communicate()
        print(out.decode())
        print(err.decode())
        # SAVE OUTPUT
        log_file = 'log/d4j_perfect/' +  command.split('/')[-2] + '.log'
        fout = open(log_file,'w')
        fout.write(out.decode())
        fout.write(err.decode())
        conn = create_connection()
        with conn:
            # UPDATE COMMAND TO DONE
            cur = conn.cursor()
            cur.execute('''
                UPDATE ISSTA_COMMANDS
                SET status = 'DONE'
                WHERE
                command = (%s)
                ''', (init_command,))
            conn.commit()


