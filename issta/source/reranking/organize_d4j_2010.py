from source.utils.defects4j_wrapper import get_info
import datetime

projects = {'Chart': 26, 'Closure': 176, 'Lang': 65, 'Math': 106, 'Mockito': 38, 'Time': 27}



def get_all_dates():
    new_projects = []
    for project in projects:
        for id in range(1, projects[project] + 1):

            out, err = get_info(project, id)
            lines = out.decode().split('\n')
            for idx, line in enumerate(lines):
                if line.startswith("Revision date"):
                    if int(lines[idx+1].split('-')[0]) >= 2011:
                        new_projects.append(project + ' ' +  str(id) + ' ')
    return new_projects
                        
                        



from source.reranking.patch import Patch
import os
import sys
from collections import defaultdict


# Goal: Organize Defects4J into 3 parties:

# get name of the ones that are 1 line hunks, etc.


# 1) 1-line hunks we can fix

# 2) other 1-linie hunks

# 3) bugs we can fix 

# 4) others

# put in output dir


INTERSTING_BUGS=  ['Chart11 ',
    'Chart1 ',
    'Chart12 ',
    'Chart13 ',
    'Chart14 ',
    'Chart19 ',
    'Chart20 ',
    'Chart24  ',
    'Chart26 ',
    'Chart4 ',
    'Chart7  ',
    'Chart8 ',
    'Chart9 ',
    'Closure10 ',
    'Closure102 ',
    'Closure109  ',
    'Closure104 ',
    'Closure11 ',
    'Closure115  ',
    'Closure119 ',
    'Closure125 ',
    'Closure126 ',
    'Closure13 ',
    'Closure18 ',
    'Closure2 ',
    'Closure21 ',
    'Closure22 ',
    'Closure25  ',
    'Closure31 ',
    'Closure38  ',
    'Closure40 ',
    'Closure46 ',
    'Closure4 ',
    'Closure59  ',
    'Closure62 ',
    'Closure63 ',
    'Closure70 ',
    'Closure73 ',
    'Closure86 ',
    'Closure92 ',
    'Closure93 ',
    'Lang10 ',
    'Lang13 ',
    'Lang18 ',
    'Lang21 ',
    'Lang22 ',
    'Lang24  ',
    'Lang26 ',
    'Lang27  ',
    'Lang33 ',
    'Lang39 ',
    'Lang47  ',
    'Lang51 ',
    'Lang57 ',
    'Lang58 ',
    'Lang59 ',
    'Lang6 ',
    'Lang63 ',
    'Lang7 ',
    'Math11 ',
    'Math15  ',
    'Math2 ',
    'Math22 ',
    'Math28  ',
    'Math30 ',
    'Math32 ',
    'Math33 ',
    'Math34 ',
    'Math35 ',
    'Math3 ',
    'Math4 ',
    'Math5 ',
    'Math50 ',
    'Math52 ',
    'Math56 ',
    'Math57 ',
    'Math58 ',
    'Math59 ',
    'Math62 ',
    'Math63 ',
    'Math65 ',
    'Math70 ',
    'Math73 ',
    'Math75 ',
    'Math77 ',
    'Math79 ',
    'Math80 ',
    'Math82 ',
    'Math85 ',
    'Math88 ',
    'Math89 ',
    'Math8 ',
    'Math94 ',
    'Math96 ',
    'Math98 ',
    'Mockito26 ',
    'Mockito29 ',
    'Mockito38 ',
    'Mockito5 ',
    'Time17 ',
    'Time18 ',
    'Time19 ',
    'Time26 ',
    'Time4 ',
    'Time7']


def read_beam(beam_file, meta_data):
    lines = open(beam_file).read().split('\n')
    dataset = []

    for line in lines:
        if line.startswith('S'): # start of new bug
            source  = line.split('\t')[1]
            row_num = int(line.split('\t')[0].split('-')[1])
            defects4j_id = " ".join(meta_data[row_num])
            meta = "\t".join(meta_data[row_num])
        elif line.startswith('T'):
            target = line.split('\t')[1]
        elif line.startswith('H'):
            hypothesis = line.split('\t')[2]
            score = float(line.split('\t')[1])
            dataset.append(Patch(source, target, hypothesis, defects4j_id, row_num, score, meta))

    return dataset


def get_defects4j_meta(meta):
    fin = open(meta, 'r')
    data = []
    for l in fin.readlines():
        data.append(l.split('\t'))
    return data
    
    
def main():
    meta_file = sys.argv[1]
    beam_file = sys.argv[2]
    output_dir = sys.argv[3]
    fixed = []
    unique = {}
    only_unique = []
    one_line = []
    intbugs = []
    count = 0
    interesting_bugs = []
    new = []


    new_projects = get_all_dates()
    print(len(new_projects))
    # open meta file:
    meta_data = get_defects4j_meta(meta_file)
    for i in meta_data:
        if len(i) > 1:
        
            if i[0] + ' ' + i[1] + ' ' not in unique:
                unique[i[0] + ' ' + i[1] + ' '] = 0
            else:
                unique[i[0] + ' ' + i[1] + ' '] += 1
            if i[0]+ i[1] + ' '  in INTERSTING_BUGS:
                interesting_bugs.append(i[0] + ' ' + i[1] + ' ')
                

    for inst in unique:
        if unique[inst] == 0:
            only_unique.append(inst)


    dataset = read_beam(beam_file, meta_data)
    
    current_patch = ""
    
    all_patches = defaultdict(list)
    for result in dataset:
        if current_patch != result.defects4j_id:

            current_patch = result.defects4j_id
            for substring in only_unique:
                if substring in current_patch:
                    one_line.append(current_patch)
            for substring in interesting_bugs:
                if substring in current_patch:
                    fixed.append(current_patch)
            for substring in new_projects:
                if substring in current_patch:
                    new.append(current_patch)
            rank = 1

        if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
            all_patches[current_patch].append([result.patch, result.target, result.score, result.meta, rank, "nocontext"])
            rank += 1
            if result.patch == result.target:
                fixed.append(current_patch)
    fixed = list(set(fixed))
    print(len(fixed))
    print(len(one_line))
    
    
    for key in all_patches:
        if key in new: 
            if not os.path.exists(output_dir + '/fixed'):
                os.mkdir(output_dir + '/fixed')
            if not os.path.exists(output_dir + '/one_line'):
                os.mkdir(output_dir + '/one_line')   
            if not os.path.exists(output_dir + '/others'):
                os.mkdir(output_dir + '/others')               
                         
            sorted_list =  sorted(all_patches[key], key=lambda x: x[2], reverse=True)
            for elem in sorted_list:
                if key in fixed:
                    outdir = output_dir + '/fixed/'
                elif key in one_line:
                    outdir = output_dir + '/one_line/'
                else:
                    outdir = output_dir + '/others/'
                fout = open(outdir + key.replace(" ","").replace('\n','').replace('/','.') + '_ENCORE_PERFECT_LOC.txt', 'w')
                
                for elem in sorted_list:
                    fout.write("START PATCH" + '\n')
                    fout.write("score: " + str(elem[2]) + "\n")
                    fout.write("rank: " + str(elem[4]) + "\n")
                    fout.write(elem[0] + "\n")
                    fout.write(key + "\n")





            

main()
