from source.reranking.patch import Patch
import os
from collections import defaultdict


def read_beam(beam_file, meta_data):
    lines = open(beam_file).read().split('\n')
    dataset = []

    for line in lines:
        if line.startswith('S'): # start of new bug
            source  = line.split('\t')[1]
            row_num = int(line.split('\t')[0].split('-')[1])
            defects4j_id = " ".join(meta_data[row_num])
            meta = "\t".join(meta_data[row_num])
        elif line.startswith('T'):
            target = line.split('\t')[1]
        elif line.startswith('H'):
            hypothesis = line.split('\t')[2]
            score = float(line.split('\t')[1])
            dataset.append(Patch(source, target, hypothesis, defects4j_id, row_num, score, meta))

    return dataset


def get_defects4j_meta(meta):
    fin = open(meta, 'r')
    data = []
    for l in fin.readlines():
        data.append(l.split('\t'))
    return data


def indiv_results():
    # open the 10 output.
    MAIN_DIR = "/home/tlutelli/issta_data/final/fairseq-data/2006/nocontext/results/"

    meta_data = get_defects4j_meta('/home/tlutelli/issta_data/final/data/2006/defects4j/_test_meta.txt')
    coconut1 = MAIN_DIR + 'fconv_tuned_1/checkpoint_best.pt/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'fconv_tuned_2/checkpoint_best.pt/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'fconv_tuned_3/checkpoint_best.pt/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'fconv_tuned_4/checkpoint_best.pt/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'fconv_tuned_5/checkpoint_best.pt/output.tok.nbest.txt'
    coconut6 = MAIN_DIR + 'fconv_tuned_6/checkpoint_best.pt/output.tok.nbest.txt'
    coconut7 = MAIN_DIR + 'fconv_tuned_7/checkpoint_best.pt/output.tok.nbest.txt'
    coconut8 = MAIN_DIR + 'fconv_tuned_8/checkpoint_best.pt/output.tok.nbest.txt'
    coconut9 = MAIN_DIR + 'fconv_tuned_9/checkpoint_best.pt/output.tok.nbest.txt'
    coconut10 = MAIN_DIR + 'fconv_tuned_10/checkpoint_best.pt/output.tok.nbest.txt'
    encore = [coconut1, coconut1, coconut3, coconut4, coconut5, coconut6, coconut7, coconut8, coconut9, coconut10]
    '''
    MAIN_DIR = "/home/tlutelli/issta_data/fairseq-data/java/context/results/"
    coconut1 = MAIN_DIR + 'context_tuned_1/checkpoint_best.pt/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'context_tuned_2/checkpoint_best.pt/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'context_tuned_3/checkpoint_best.pt/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'context_tuned_4/checkpoint_best.pt/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'context_tuned_5/checkpoint_best.pt/output.tok.nbest.txt'
    coconut6 = MAIN_DIR + 'context_tuned_6/checkpoint_best.pt/output.tok.nbest.txt'
    coconut7 = MAIN_DIR + 'context_tuned_7/checkpoint_best.pt/output.tok.nbest.txt'
    coconut8 = MAIN_DIR + 'context_tuned_8/checkpoint_best.pt/output.tok.nbest.txt'
    coconut9 = MAIN_DIR + 'context_tuned_9/checkpoint_best.pt/output.tok.nbest.txt'
    coconut10 = MAIN_DIR + 'context_tuned_10/checkpoint_best.pt/output.tok.nbest.txt'
    coconuts = [coconut1, coconut1, coconut3, coconut4, coconut5, coconut6, coconut7, coconut8, coconut9, coconut10]
    '''
    #meta_coco = get_defects4j_meta('/home/tlutelli/issta_data/fairseq-data/context/test/_test_meta.txt')
    for i in range(0,10):
        print("Working on coconut:", str(i))
        all_coconut = []
        current_patch = ""
        all_patches = defaultdict(list)
        for j in range(0,i+1):
            print(j)
            all_coconut +=read_beam(encore[j], meta_data)

        for result in all_coconut:
            if current_patch != result.defects4j_id:
                current_patch = result.defects4j_id
                count = 0
            if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
                # temp_patch[current_patch].append(result.patch)
                all_patches[current_patch].append([result.patch, result.score, result.meta, count, "no_context"])
        all_coconut = []
        for j in range(0, i+1):
            all_coconut +=read_beam(coconuts[j], meta_coco)
        for result in all_coconut:
            if current_patch != result.defects4j_id:
                current_patch = result.defects4j_id
                count = 0
            if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
                # temp_patch[current_patch].append(result.patch)
                all_patches[current_patch].append([result.patch, result.score, result.meta, count, "context"])
        if not os.path.exists(MAIN_DIR + '../coconut_' + str(i)):
            os.mkdir('../coconut_' + str(i))
        for key in all_patches:
            temp_patch = []
            fout = open(MAIN_DIR + '../coconut_' + str(i) + '/' + key.replace(" ","").replace('\n','').replace('/','.') + '_ENCORE_PERFECT_LOC.txt', 'w')
            sorted_list =  sorted(all_patches[key], key=lambda x: x[1], reverse=True)
            #print(key +" " +  str(len(sorted_list)))
            for elem in sorted_list:
                if elem[0] not in temp_patch:
                    fout.write("START PATCH" + '\n')
                    fout.write("\n")
                    fout.write("\n")
                    fout.write(elem[0] + "\n")
                    fout.write(key + "\n")
                    fout.write(str(elem[1]) + " " + str(elem[3]) + " " + elem[4] + '\n')
                    temp_patch.append(elem[0])

def perfect_loc_merged():
    # open the 20 output.
    MAIN_DIR = "/home/tlutelli/issta_data/final/fairseq-data/2006/nocontext/results/"

    meta_data = get_defects4j_meta('/home/tlutelli/issta_data/final/data/2006/defects4j/_test_meta.txt')
    coconut1 = MAIN_DIR + 'fconv_tuned_1/checkpoint_best.pt/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'fconv_tuned_2/checkpoint_best.pt/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'fconv_tuned_3/checkpoint_best.pt/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'fconv_tuned_4/checkpoint_best.pt/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'fconv_tuned_5/checkpoint_best.pt/output.tok.nbest.txt'
    coconut6 = MAIN_DIR + 'fconv_tuned_6/checkpoint_best.pt/output.tok.nbest.txt'
    coconut7 = MAIN_DIR + 'fconv_tuned_7/checkpoint_best.pt/output.tok.nbest.txt'
    coconut8 = MAIN_DIR + 'fconv_tuned_8/checkpoint_best.pt/output.tok.nbest.txt'
    coconut9 = MAIN_DIR + 'fconv_tuned_9/checkpoint_best.pt/output.tok.nbest.txt'
    coconut10 = MAIN_DIR + 'fconv_tuned_10/checkpoint_best.pt/output.tok.nbest.txt'
    '''
    encore = [coconut1, coconut1, coconut3, coconut4, coconut5, coconut6, coconut7, coconut8, coconut9, coconut10]
    MAIN_DIR = "/home/tlutelli/issta_data/fairseq-data/java/context/results/"
    coconut1 = MAIN_DIR + 'context_tuned_1/checkpoint_best.pt/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'context_tuned_2/checkpoint_best.pt/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'context_tuned_3/checkpoint_best.pt/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'context_tuned_4/checkpoint_best.pt/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'context_tuned_5/checkpoint_best.pt/output.tok.nbest.txt'
    coconut6 = MAIN_DIR + 'context_tuned_6/checkpoint_best.pt/output.tok.nbest.txt'
    coconut7 = MAIN_DIR + 'context_tuned_7/checkpoint_best.pt/output.tok.nbest.txt'
    coconut8 = MAIN_DIR + 'context_tuned_8/checkpoint_best.pt/output.tok.nbest.txt'
    coconut9 = MAIN_DIR + 'context_tuned_9/checkpoint_best.pt/output.tok.nbest.txt'
    coconut10 = MAIN_DIR + 'context_tuned_10/checkpoint_best.pt/output.tok.nbest.txt'
    coconuts = [coconut1, coconut1, coconut3, coconut4, coconut5, coconut6, coconut7, coconut8, coconut9, coconut10]
    meta_coco = get_defects4j_meta('/home/tlutelli/issta_data/fairseq-data/context/test/_test_meta.txt')
    '''
    
    
    for idx, enc_res in enumerate(encore):
        print(idx)
        all_patches = defaultdict(list)
        all_coconut = read_beam(enc_res, meta_data)
        current_patch = ""
        temp_patch = defaultdict(list)
        for result in all_coconut:
            if current_patch != result.defects4j_id:
                current_patch = result.defects4j_id
                fout = open('/local/tlutelli/fconv/' + current_patch.replace(" ","").replace('\n','').replace('/','.') + '_ENCORE_PERFECT_LOC.txt', 'w')
                count = 0
            if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
                count += 1
                fout.write("START PATCH" + '\n')
                fout.write(current_patch)
                fout.write(str(result.patch) + '\n')
                fout.write(str(result.score) + '\n')
                fout.write(result.meta)
                fout.write(str(count) + '\n')
    '''
    for idx, enc_res in enumerate(coconuts):
        print(idx)
        all_patches = defaultdict(list)
        all_coconut = read_beam(enc_res, meta_data)
        current_patch = ""
        temp_patch = defaultdict(list)
        for result in all_coconut:
            if current_patch != result.defects4j_id:
                current_patch = result.defects4j_id
                count = 0
            if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
                count += 1
                fout.write("START PATCH" + '\n')
                fout.write(current_patch)
                fout.write(str(result.patch) + '\n')
                fout.write(str(result.score) + '\n')
                fout.write(result.meta)
                fout.write(str(count) + '\n')
    '''
perfect_loc_merged()
#quixjava_merged()
#perfect_loc_merged()
#codeflaws()
#ochoai()
