import subprocess
import os


model_dir = '/local/tlutelli/issta_data/fairseq-data/java/fconv_tuned_2_icse_data_test/checkpoint'
models = ['fconv_tuned_2_icse_data_test']

for model in range(18,20):
    command = "bash source/testing/run_trained_model.sh '/local/tlutelli/issta_data/fairseq-data/java/d4j_icse/test.src' " \
              + '/local/tlutelli/issta_data/fairseq-data/java/icse_perfectloc_d4j_all/checkpoint' + str(model)  + ' ' \
              + model_dir  + str(model) +'.pt' \
              + " /local/tlutelli/issta_data/fairseq-data/java/d4j_icse/bin " + str(1000)

    print(command)
    p = subprocess.Popen([command], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate()

