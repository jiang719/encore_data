import sys

src_f_path,rem_f_path,add_f_path = sys.argv[1:]
rem = []
add = []
with open(src_f_path) as src_f:
  for i,line in enumerate(src_f.read().split('\n')):
    if len(line)>0:
      rem_line,add_line = line.split('\t')
      rem.append(rem_line)
      add.append(add_line)
with open(rem_f_path,'w') as rem_f:
  rem_f.write('\n'.join(rem))
with open(add_f_path,'w') as add_f:
  add_f.write('\n'.join(add))