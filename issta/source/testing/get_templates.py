


def read_beam(beam_file):
    lines = open(beam_file).read().split('\n')
    dataset = []
    res = 0
    target = "all"
    for line in lines:
        if line.startswith('S'): # start of new bug
            source  = line.split('\t')[1]
            row_num = int(line.split('\t')[0].split('-')[1])
        elif line.startswith('T'):
            target = line.split('\t')[1]
        elif line.startswith('H'):
            hypothesis = line.split('\t')[2]
            score = float(line.split('\t')[1])
            if target == hypothesis:
                res +=1
    return res


res = 0
#dataset = read_beam('/home/tlutelli/issta_data/final/fairseq-data/2006/nocontext/results/fconv_tuned_6/checkpoint_best.pt/output.tok.nbest.txt')
#print(dataset)
dataset = read_beam('/home/tlutelli/issta_data/fairseq-data/all2010/nocontext/results/fconv_tuned_2/checkpoint_best.pt/output.tok.nbest.txt')
print(dataset)


