import source.testing.generate_split as generate_split
import sys

def perfect_data_1line():
    """
    This is the preprocessing for single line with only context (i.e., no separation between buggy and clean parts of the function)
    """
    both_path = 'data/java/test/perfect/remadd_1line.txt'
    meta_path = 'data/java/test/perfect/meta_1line.txt'
    saved_data_path = 'data/java/test/perfect/'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)


# perfect data
def perfect_data_1line_sep():
    """
    This is the preprocessing for single line with input and context separated by tokens (i.e., no separation between buggy and clean parts of the function).
    """
    both_path = 'data/java/test/perfect/remadd_1line.txt'
    meta_path = 'data/java/test/perfect/meta_1line.txt'
    saved_data_path = 'data/java/test/perfect/'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)


# ochoai localization
def ochoai_1line():
    both_path = 'data/java/test/ochoai_100loc'
    meta_path = ''
    saved_data_path = 'data/java/test/ochoai_100loc/'
    valid_size = 2000
    perfect_loc = False
    only_test = True
    generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)

def perfect_data_all(saved_data_path):
    both_path = ''
    rem_path = '/home/tlutelli/issta_data/data/java/defects4j/nocontext/rem.txt'
    add_path  = '/home/tlutelli/issta_data/data/java/defects4j/nocontext/add.txt'
    meta_path = '/home/tlutelli/issta_data/data/java/defects4j/nocontext/meta.txt'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split(both_path, rem_path, add_path, meta_path, saved_data_path, valid_size, perfect_loc, only_test)
    #generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)


def perfect_data_python_quixbug():
    both_path = 'data/python/test/quixbug_remadd_py.txt'
    meta_path = 'data/python/test/meta.txt'
    saved_data_path = 'data/python/test/'
    valid_size = 2000

    print("hj")
    perfect_loc = True
    only_test = True
    generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)


def perfect_data_all_context(saved_data_path):
    both_path = ''
    rem_path = '/home/tlutelli/issta_data/data/java/defects4j/nocontext/rem.txt'
    add_path  = '/home/tlutelli/issta_data/data/java/defects4j/nocontext/add.txt'
    meta_path = '/home/tlutelli/issta_data/data/java/defects4j/nocontext/meta.txt'
    context_path = '/home/tlutelli/issta_data/data/java/defects4j/nocontext/context.txt'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split_context(both_path, rem_path, add_path, meta_path, context_path, saved_data_path, valid_size, perfect_loc, only_test)
    #generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)


def perfect_data_genprog():
    both_path = 'data/cpp/test/genprog/remadd_lines.txt'
    rem_path = ''
    add_path = ''
    contex_path = ''
    meta_path = 'data/cpp/test/genprog/meta_lines.txt'
    saved_data_path = 'data/cpp/test/genprog/'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)
    #generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)


def perfect_data_javascript():
    print('tir')
    both_path = 'data/javascript/test/js_remadd.txt'
    rem_path = ''
    add_path = ''
    contex_path = ''
    meta_path = 'data/javascript/test/_test_meta.txt'
    saved_data_path = 'data/javascript/test/'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split(both_path, '', '', both_path, saved_data_path, valid_size, perfect_loc, only_test)
    #generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)




def perfect_data_codeflaw_all():
    both_path = ''
    init_path = 'data/cpp/test/codeflaws/all_codeflaws_onelines.tsv'
    fin = open(init_path, 'r')
    lines = fin.read().split('\n')
    rem_path = 'data/cpp/test/codeflaws/rem.txt'
    add_path  = 'data/cpp/test/codeflaws/add.txt'
    meta_path = 'data/cpp/test/codeflaws/meta.txt'
    '''
    fout1 = open(rem_path, 'w')
    fout2 = open(add_path, 'w')
    fout3 = open(meta_path, 'w')
    for line in lines:
        rem, add, meta, loc = line.split('\t')
        fout1.write(rem.strip() + '\n')
        fout2.write(add.strip() + '\n')
        fout3.write('\t'.join([meta, loc]) + '\n')

    '''
    saved_data_path = 'data/cpp/test/codeflaws/all/'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split(both_path, rem_path, add_path, meta_path, saved_data_path, valid_size, perfect_loc, only_test)


def perfect_data_codeflaw_651():
    both_path = ''
    init_path = 'data/cpp/test/codeflaws/3cat_onelines.tsv'
    fin = open(init_path, 'r')
    lines = fin.read().split('\n')
    rem_path = 'data/cpp/test/codeflaws/codeflaw_651/rem.txt'
    add_path  = 'data/cpp/test/codeflaws/codeflaw_651/add.txt'
    meta_path = 'data/cpp/test/codeflaws/codeflaw_651/meta.txt'
    '''
    fout1 = open(rem_path, 'w')
    fout2 = open(add_path, 'w')
    fout3 = open(meta_path, 'w')
    for line in lines:
        rem, add, meta, loc = line.split('\t')
        fout1.write(rem.strip() + '\n')
        fout2.write(add.strip() + '\n')
        fout3.write('\t'.join([meta, loc]) + '\n')
    '''

    saved_data_path = 'data/cpp/test/codeflaws/codeflaw_651/'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split(both_path, rem_path, add_path, meta_path, saved_data_path, valid_size, perfect_loc, only_test)

def quix_java_perfect_data_all():
    both_path = ''
    rem_path = 'data/java/context/test/quixjava/rem.txt'
    add_path  = 'data/java/context/test/quixjava/add.txt'
    contex_path = 'data/java/context/test/quixjava/context.txt'

    meta_path = 'data/java/context/test/quixjava/meta.txt'
    saved_data_path = 'data/java/context/test/quixjava/context/'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split_context(both_path, rem_path, add_path, meta_path, contex_path, saved_data_path, valid_size, perfect_loc, only_test)
    #generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)

    both_path = ''
    rem_path = 'data/java/context/test/quixjava/rem.txt'
    add_path  = 'data/java/context/test/quixjava/add.txt'
    meta_path = 'data/java/context/test/quixjava/meta.txt'
    saved_data_path = 'data/java/context/test/quixjava/no_context/'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split(both_path, rem_path, add_path, meta_path, saved_data_path, valid_size, perfect_loc, only_test)
    #generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)

def quixbug_data_all(saved_data_path):
    both_path = ''
    rem_path = '/home/tlutelli/issta_data/final/data/quixjava/rem.txt'
    add_path  = '/home/tlutelli/issta_data/final/data/quixjava/add.txt'
    meta_path = '/home/tlutelli/issta_data/final/data/quixjava/meta.txt'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split(both_path, rem_path, add_path, meta_path, saved_data_path, valid_size, perfect_loc, only_test)
    #generate_split.generate_split(both_path, '', '', meta_path, saved_data_path, valid_size, perfect_loc, only_test)

def quixbug_data_all_context(saved_data_path):
    both_path = ''
    rem_path = '/home/tlutelli/issta_data/final/data/quixjava/rem.txt'
    add_path  = '/home/tlutelli/issta_data/final/data/quixjava/add.txt'
    meta_path = '/home/tlutelli/issta_data/final/data/quixjava/meta.txt'
    context_path = '/home/tlutelli/issta_data/final/data/quixjava/context.txt'
    valid_size = 2000
    perfect_loc = True
    only_test = True
    generate_split.generate_split_context(both_path, rem_path, add_path, meta_path, context_path, saved_data_path, valid_size, perfect_loc, only_test)

#perfect_data_javascript()
#perfect_data_genprog()
#perfect_data_codeflaw_651()
#perfect_data_all_context()
#perfect_data_python_quixbug()
if sys.argv[2] == 'defects4j':
    if sys.argv[3] == "nocontext":
        perfect_data_all(sys.argv[1])
    if sys.argv[3] == "context":
        perfect_data_all_context(sys.argv[1])
if sys.argv[2] == 'quixbugs':
    if sys.argv[3] == "nocontext":
        quixbug_data_all(sys.argv[1])
    if sys.argv[3] == "context":
        quixbug_data_all_context(sys.argv[1])