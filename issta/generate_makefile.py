#This generate a make file for compilation
import os
input_dir = '/home/tlutelli/issta_data/final/fairseq-data/2006/nocontext/validation/results/fconv_7'
model = "fconv_tuned_7"


fout= open("Makefile_" + model, 'w')


# loop dir fixed
list_of_files = []
for (dirpath, dirnames, filenames) in os.walk(input_dir + '/fixed'):
    for filename in filenames:
        if filename.endswith('.txt'):
            list_of_files.append(os.sep.join([dirpath, filename]))



# loop oneline
for (dirpath, dirnames, filenames) in os.walk(input_dir + '/one_line'):
    for filename in filenames:
        if filename.endswith('.txt'):
            list_of_files.append(os.sep.join([dirpath, filename]))

# loop other.
for (dirpath, dirnames, filenames) in os.walk(input_dir + '/others'):
    for filename in filenames:
        if filename.endswith('.txt'):
            list_of_files.append(os.sep.join([dirpath, filename]))



fout.write("all: ")
for i in range(0, len(list_of_files)):
    fout.write(str(i) + " ")
fout.write('\n')

for idx, f in enumerate(list_of_files):
    fout.write(str(idx) + ":\n")
    filename = f.split('/')[-1]
    fout.write("\tpython -m source.compilator.d4j_compilation " + f + " /local/tlutelli/" + model + "/" + filename + '/ ' +  " /local/tlutelli/" + model + "/" + filename.replace('.txt', '.log') + " 0\n")
    #fout.write("\trm -rf /local/tlutelli/" + model + "/" + filename + '/ \n')

#init_command = "python -m source.compilator.d4j_compilation
#/home/tlutelli/issta_data/final/fairseq-data/2006/nocontext/results/fconv_tuned_2/checkpoint_best.pt/fixed/Chart1source.org.jfree.chart.renderer.category.AbstractCategoryItemRenderer.java17971797_
#ENCORE_PERFECT_LOC.txt
#/local/tlutelli/fconv_tuned_2/_Chart_1/
#/local/tlutelli/fconv_tuned_2/_Chart_1.log  0"
#

#rm /local/tlutelli/fconv_tuned_2_Chart_1/
