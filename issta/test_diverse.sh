# bash test.sh
# /local/tlutelli/issta_data/fairseq-data/ghtorrent_2007/java/2006/nocontext/test/
# /local/tlutelli/issta_data/fairseq-data/ghtorrent_2007/java/2006/nocontext/train/bin/dict.src.txt
# /local/tlutelli/issta_data/fairseq-data/ghtorrent_2007/java/2006/nocontext/train/bin/dict.trg.txt
# /local/tlutelli/issta_data/fairseq-data/ghtorrent_2007/java/2006/fconv_tuned_2/
# checkpoint1.pt
# /local/tlutelli/issta_data/fairseq-data/ghtorrent_2007/java/2006/nocontext/d4j_results/fconv_tuned_2

context=$1
save_tokenized=$2
src_dict=$3
trg_dict=$4
model_dir=$5
model=$6
result_dir=$7
gpu_id=$8


export CUDA_VISIBLE_DEVICES="${gpu_id}"
FAIRSEQPY=/home/tlutelli/fairseq-context

#python -m source.testing.prepare_test_data "${save_tokenized}" "${context}"

#bash source/testing/get_testing_data.sh process "${src_dict}" "${trg_dict}" "${save_tokenized}" "${save_tokenized}"


#exit 1
output_dir="${result_dir}"/"${model}"/diverse
model_path="${model_dir}""${model}"
target="${save_tokenized}"/bin
input_file="${save_tokenized}"/test.src
beam=1000


mkdir -p $output_dir


if [ "${context}" == "nocontext" ]; then

python "${FAIRSEQPY}"/generate.py -s src -t trg --path "${model_path}" --beam $beam  --nbest "${beam}" "${target}" --max-tokens 50  < "${input_file}" > "${output_dir}"/output.tok.nbest.txt

else
    python "${FAIRSEQPY}"/generate.py -s src -t trg --use-context --path "${model_path}" --diverse-beam-groups 10 --beam $beam --nbest "${beam}" "${target}" --max-tokens 50 < "${input_file}" > "${output_dir}"/output.tok.nbest.txt
# getting best hypotheses
#cat $output_dir/output.tok.nbest.txt | grep "^H"  | python -c "import sys; x = sys.stdin.readlines(); x = ' '.join([ x[i] for i in range(len(x)) if(i%$beam == 0) ]); print(x)" | cut -f3 > $output_dir/output.tok.txt

fi
