import sys
import os
import subprocess
from multiprocessing import Queue
input_dir = '/home/tlutelli/issta_data/final/fairseq-data/2010/nocontext/validation/2010_fconv_8'
model = "2010_fconv_tuned_8"



# loop dir fixed
list_of_files = []
for (dirpath, dirnames, filenames) in os.walk(input_dir + '/fixed'):
    for filename in filenames:
        if filename.endswith('.txt'):
            list_of_files.append(os.sep.join([dirpath, filename]))



# loop oneline
for (dirpath, dirnames, filenames) in os.walk(input_dir + '/one_line'):
    for filename in filenames:
        if filename.endswith('.txt'):
            list_of_files.append(os.sep.join([dirpath, filename]))

# loop other.
for (dirpath, dirnames, filenames) in os.walk(input_dir + '/others'):
    for filename in filenames:
        if filename.endswith('.txt'):
            list_of_files.append(os.sep.join([dirpath, filename]))






all_commands = []

for idx, f in enumerate(list_of_files):
    filename = f.split('/')[-1]
    all_commands.append("python -m source.compilator.d4j_compilation " + f + " /local/tlutelli/" + model + "/" + filename + '/ ' +  " /local/tlutelli/" + model + "/" + filename.replace('.txt', '.log') +  " 0")
    #fout.write("\trm -rf /local/tlutelli/" + model + "/" + filename + '/ \n')




import multiprocessing as mp

def my_func(x):
    x_stuff = x.split(' ')
    process = subprocess.call(x, shell=True)
    
    process = subprocess.call("rm -rf " + x_stuff[4], shell=True)



pool = mp.Pool(5)
result = pool.map(my_func, all_commands)


