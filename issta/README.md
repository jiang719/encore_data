# Data Extraction
 
## Download 1,000 top github repos for each language:

```
cd COCONUT
mkdir data
mkdir data/repos
mkdir data/repos/c
mkdir data/repos/java
mkdir data/repos/javascript
mkdir data/repos/python

cd /source/data_extraction/download_data/

bash downloadJavaProjects.sh
bash downloadCProjects.sh
bash downloadJavaScriptProjects.sh
bash downloadPythonProjects.sh
```
Note that we save the mapping between projects and repos name in 
```
results/mapping_repos_idx_language.txt
```
We use number for repos names in case there are repos with similar names. Also, it's easier to measure progress.
The data should not be saved on the NAS
For reference, here is how long it took for me:

Java: 116min

C:

Python:

Javascript:

## Get the list of relevant commits for each language per year:


```
 python -m source.data_extraction.buggy_commit_extractor java
```
The data should not be saved on the NAS.