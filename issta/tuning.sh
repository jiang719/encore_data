#!/bin/bash
lang=$1
data_repo=$2
context=$3
fairseq_repo=$4
train_data=$5
gpu_id=$6

export FAIRSEQPY=/home/tlutelli/fairseq-context/

export CUDA_VISIBLE_DEVICES="${gpu_id}"
echo "Start tuning"
python -m source.training.random_search_tuning "${fairseq_repo}" "${lang}" "${train_data}" "${context}"

