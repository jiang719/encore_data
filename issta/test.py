import re

data = 'var v = "this is string constant + some numbers and \" is also included "\r\nvar v = "and another \"line\" "'
matches = re.findall( r'"([^"]*)"', data, re.I | re.M)
print(matches)

data = 'x = 10; take("string one", "string two")'
matches = re.findall( r'"([^"]*)"', data, re.I | re.M)
print(matches)

data = "var v = 'this is string constant + some numbers and \" is also included '\r\nvar v = 'and another \'line\' '"
matches = re.findall( r"'([^']*)'", data, re.I | re.M)
print(matches)

data2 = "x = 10; take('string one', 'string two')"
matches = re.findall( r"'([^']*)'", data2, re.I | re.M)
print(matches)
