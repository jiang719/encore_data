from operator import itemgetter
import shutil
import os
import subprocess

## Output from ochoia, goltzar 1.6: https://github.com/SerVal-DTF/FL-VS-APR


### test for Chart 1.

FAULT_LOCALIZATION = "/home/tlutelli/FL-VS-APR/FaultLocalization/GZoltar-1.6.0/crushMatrixOutput/ochiai/"
DEFECTS4J = "/home/tlutelli/defects4j/"
DEFECTS4J_BIN = DEFECTS4J + 'framework/bin/'

NO_LOC = ['Mockito 5', 'Mockito 8']

BUG_LIST = ['Closure 62',
'Closure 65',
'Math 80',
'Closure 67',
'Math 75',
'Lang 59',
'Math 41',
'Closure 51',
'Math 2',
'Closure 10',
'Chart 11',
'Time 16',
'Chart 1',
'Closure 73',
'Closure 38',
'Chart 10',
'Lang 57',
'Math 85',
'Math 58',
'Chart 24',
'Math 105',
'Mockito 26',
'Math 30',
'Math 59',
'Chart 12',
'Closure 71',
'Math 63',
'Closure 130',
'Math 32',
'Chart 13',
'Math 69',
'Closure 14',
'Mockito 34',
'Closure 63',
'Math 5',
'Closure 57',
'Math 33',
'Lang 16',
'Math 34',
'Closure 52',
'Math 82',
'Closure 92',
'Time 4',
'Mockito 29',
'Closure 18',
'Closure 123',
'Closure 93',
'Closure 86',
'Closure 104',
'Closure 114',
'Math 96',
'Math 11',
'Closure 125',
'Lang 61',
'Lang 6',
'Math 70',
'Math 57',
'Math 104',
'Chart 9',
'Time 19',
'Lang 33',
'Lang 24',
'Lang 26',
'Chart 8',
'Math 94',
'Math 27',
'Mockito 24',
'Closure 31',
'Chart 20',
'Closure 70',
'Lang 21',
'Lang 29',
'Mockito 38',
'Closure 113']

#BUG_LIST = ["Chart 1"]





def clean_temp_folder(temp_dir):
    """
    :param temp_dir: temporary directory where the patch will be compiled
    :return: nothing
    """
    if os.path.isdir(temp_dir):
        for files in os.listdir(temp_dir):
            file_p = os.path.join(temp_dir, files)
            try:
                if os.path.isfile(file_p):
                    os.unlink(file_p)
                elif os.path.isdir(file_p):
                    shutil.rmtree(file_p)
            except Exception as e:
                print(e)
    else:
        os.makedirs(temp_dir)


def load_defects4j_project(project, bug_id, temp_dir):
    FNULL = open(os.devnull, 'w')
    command = DEFECTS4J_BIN + "defects4j " + " checkout " + " -p " + project + " -v " + bug_id + " -w " + temp_dir
    p = subprocess.Popen([command], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.wait()
    return True


def sort_localization(project, bug_id):
    fin = open(FAULT_LOCALIZATION + project + "_" + bug_id)
    localization = []

    for line in fin.readlines()[1:]:
        fileline, suspicion = line.split(',')
        suspicion = float(suspicion.replace('\n', ''))
        file, buggyline = fileline.split('#')
        localization.append([suspicion, file, int(buggyline)])

    l_sort = sorted(localization, key=itemgetter(0), reverse=True)

    return l_sort


def extract_buggy_line(l_sort, temp_dir, project, bug_id):
    fout = open('data/java/test/ochoai_100loc/' + project + '_' + bug_id, 'w')

    dic = {'Closure': 'src/', 'Chart': 'source/', 'Math': 'src/main/java/', 'Lang': 'src/java/', 'Time': 'src/main/java/', 'Mockito': 'src/'}
    i = 0
    for loc in l_sort:
        full_file_path = temp_dir + dic[project] + loc[1].replace('.', '/').split('$', 1)[0] + '.java'
        if not os.path.isfile(full_file_path):
            full_file_path = temp_dir + dic['Lang'] + loc[1].replace('.', '/').split('$', 1)[0] + '.java'
        if not os.path.isfile(full_file_path):
            full_file_path = temp_dir + dic['Math'] + loc[1].replace('.', '/').split('$', 1)[0] + '.java'
        try:
            fin_lines = open(full_file_path, 'r').readlines()
        except Exception as e:
            print(project, str(bug_id))
            print(e)
        line = fin_lines[loc[2] - 1].strip()
        if loc[0] == 0.0:
            return
        if line:
            i += 1
            if line.startswith('//'):
                continue
            fout.write(line + " $SUSPICION$ " + str(loc[0]) + '$FILE$ ' + str(loc[1]) + ' $LINE$ ' + str(loc[2]) + '\n')
            if i == 100:
                break


def main():
    temp_dir = '/home/tlutelli/temp/'
    for bug in BUG_LIST:
        clean_temp_folder(temp_dir)
        project, bug_id = bug.split(' ')
        load_defects4j_project(project, str(bug_id) + 'b', temp_dir)
        l_sort = sort_localization(project, bug_id)
        extract_buggy_line(l_sort, temp_dir, project, bug_id)


main()