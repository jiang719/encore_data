import os
import statistics

BUG_LIST = ['Closure 62',
'Closure 65',
'Math 80',
'Closure 67',
'Math 75',
'Lang 59',
'Math 41',
'Closure 51',
'Math 2',
'Closure 10',
'Chart 11',
'Time 16',
'Chart 1',
'Closure 73',
'Closure 38',
'Chart 10',
'Lang 57',
'Math 85',
'Math 58',
'Chart 24',
'Math 105',
'Mockito 26',
'Math 30',
'Math 59',
'Chart 12',
'Closure 71',
'Math 63',
'Closure 130',
'Math 32',
'Chart 13',
'Math 69',
'Closure 14',
'Mockito 34',
'Closure 63',
'Math 5',
'Closure 57',
'Math 33',
'Lang 16',
'Math 34',
'Closure 52',
'Math 82',
'Closure 92',
'Time 4',
'Mockito 29',
'Closure 18',
'Closure 123',
'Closure 93',
'Closure 86',
'Closure 104',
'Closure 114',
'Math 96',
'Math 11',
'Closure 125',
'Lang 61',
'Lang 6',
'Math 70',
'Math 57',
'Math 104',
'Chart 9',
'Time 19',
'Lang 33',
'Lang 24',
'Lang 26',
'Chart 8',
'Math 94',
'Math 27',
'Mockito 24',
'Closure 31',
'Chart 20',
'Closure 70',
'Lang 21',
'Lang 29',
'Mockito 38',
'Closure 113']

## GET META PERFECT_LOC
perfect_meta = {}

perf_loc = open('/home/tlutelli/ENCORE_data/data/java/test/perfect/meta_1line.txt', 'r')
for line in perf_loc.read().split('\n'):
    project, id, filepath, line = line.split('\t')
    perfect_meta[project + ' ' + id] = [filepath, int(line)]

average_loc = []

for bug in BUG_LIST:
    count = 0
    f = bug.replace(' ', '_')
    if os.path.isfile('/home/tlutelli/ENCORE_data/data/java/test/ochoai_loc/' + f):
        fin = open('/home/tlutelli/ENCORE_data/data/java/test/ochoai_loc/' + f, 'r')
        for line in fin.read().split('\n'):
            if '$SUSPICION$' in line:
                rem_line = line.split('$SUSPICION$')[0]
                meta = line.split('$SUSPICION$')[1]
                fileline = meta.split('$FILE$')[1]
                file, line = fileline.split(' $LINE$ ')
                file = file.strip().split('$')[0].replace('.','/') + '.java'
                line = int(line.replace('\n',''))
                count += 1
                if file in perfect_meta[bug][0] and line == perfect_meta[bug][1]:
                    print("Perfect Localization for ", bug, ' ranked: ', str(count))
                    average_loc.append(count)
                    break

print("Find rank for:")
print(len(average_loc))
print("Average rank:")
print(statistics.mean(average_loc))
print("Median rank:")
print(statistics.median(average_loc))
print("Max rank:")
print(max(average_loc))
