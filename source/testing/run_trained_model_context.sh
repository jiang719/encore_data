#!/usr/bin/env bash
## This script is to run the complete GEC system on any given test set
ENCORE=/home/tlutelli/ENCORE_data


input_file=$1
output_dir=$2
model_path=$3
target=$4
beam=$5

if [[ -d "$model_path" ]]; then
    models=`ls $model_path/*pt | tr '\n' ' ' | sed "s| \([^$]\)| --path \1|g"`
    echo $models
elif [[ -f "$model_path" ]]; then
    models=$model_path
elif [[ ! -e "$model_path" ]]; then
    echo "Model path not found: $model_path"
fi

FAIRSEQPY=/home/tlutelli/fairseq-context


mkdir -p $output_dir

# running fairseq on the test data
python $FAIRSEQPY/generate.py -s src -t trg --use-context --path $models --print-alignment --beam $beam  --nbest $beam $target --max-tokens 50  < $input_file > $output_dir/output.tok.nbest.txt

# getting best hypotheses
cat $output_dir/output.tok.nbest.txt | grep "^H"  | python -c "import sys; x = sys.stdin.readlines(); x = ' '.join([ x[i] for i in range(len(x)) if(i%$beam == 0) ]); print(x)" | cut -f3 > $output_dir/output.tok.txt

