import subprocess
import os

BUG_LIST = ['Closure 62',
'Closure 65',
'Math 80',
'Closure 67',
'Math 75',
'Lang 59',
'Math 41',
'Closure 51',
'Math 2',
'Closure 10',
'Chart 11',
'Time 16',
'Chart 1',
'Closure 73',
'Closure 38',
'Chart 10',
'Lang 57',
'Math 85',
'Math 58',
'Chart 24',
'Math 105',
'Mockito 26',
'Math 30',
'Math 59',
'Chart 12',
'Closure 71',
'Math 63',
'Closure 130',
'Math 32',
'Chart 13',
'Math 69',
'Closure 14',
'Mockito 34',
'Closure 63',
'Math 5',
'Closure 57',
'Math 33',
'Lang 16',
'Math 34',
'Closure 52',
'Math 82',
'Closure 92',
'Time 4',
'Mockito 29',
'Closure 18',
'Closure 123',
'Closure 93',
'Closure 86',
'Closure 104',
'Closure 114',
'Math 96',
'Math 11',
'Closure 125',
'Lang 61',
'Lang 6',
'Math 70',
'Math 57',
'Math 104',
'Chart 9',
'Time 19',
'Lang 33',
'Lang 24',
'Lang 26',
'Chart 8',
'Math 94',
'Math 27',
'Mockito 24',
'Closure 31',
'Chart 20',
'Closure 70',
'Lang 21',
'Lang 29',
'Mockito 38',
'Closure 113']

models = ['fconv_tuned_10']
#BUG_LIST = [
#'Mockito 38',
#'Closure 113']



for bug in BUG_LIST:
    folder = bug.replace(' ', '_') + 'dir'
    if os.path.isfile('/home/tlutelli/ENCORE_data/fairseq-data/java/ochoai_100loc/' + folder + "/test.src"):
        for model in models:
            command = "bash source/testing/run_trained_model.sh '/home/tlutelli/ENCORE_data/fairseq-data/java/ochoai_100loc/" + folder + "/test.src' " \
                      + '/home/tlutelli/ENCORE_data/fairseq-data/java/fconv/ochoai_100loc/' + folder +'/' + model \
                      + " /home/tlutelli/ENCORE_data/fairseq-data/java/fconv/trained/" + model \
                      + " /home/tlutelli/ENCORE_data/fairseq-data/java/ochoai_100loc/" + folder + "/bin " \
                      + str(1000)

            print(command)
            p = subprocess.Popen([command], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            p.communicate()
    else:
        print("Can't find any location for " + folder)
