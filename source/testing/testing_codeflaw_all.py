import subprocess
import os


models = ['fconv_tuned_1', 'fconv_tuned_2', 'fconv_tuned_3', 'fconv_tuned_4', 'fconv_tuned_5']

for model in models:
    command = "bash source/testing/run_trained_model.sh '/home/tlutelli/ENCORE_data/fairseq-data/cpp/codeflaw_651/test.src' " \
              + '/home/tlutelli/ENCORE_data/fairseq-data/cpp/codeflaw_651/' + model \
              + " /home/tlutelli/ENCORE_data/fairseq-data/cpp/fconv/trained/" + model \
              + " /home/tlutelli/ENCORE_data/fairseq-data/cpp/codeflaw_651/bin " + str(1000)

    print(command)
    p = subprocess.Popen([command], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate()
