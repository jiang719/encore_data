#!/usr/bin/env bash
# no context
ENCORE=/home/tlutelli/ENCORE_data
opts=$1
src_dir=$2
trg_dir=$3


if [[ $opts == "process"  ]]; then
    fairseq_dir=/home/tlutelli/fairseq
    echo "preprocessing data"
    python source/testing/split_remadd.py $src_dir/_train_remadd.txt $trg_dir/train.src $trg_dir/train.trg
    python source/testing/split_remadd.py $src_dir/_valid_remadd.txt $trg_dir/valid.src $trg_dir/valid.trg
    python source/testing/split_remadd.py $src_dir/_test_remadd.txt $trg_dir/test.src $trg_dir/test.trg

    src_dict=$ENCORE/fairseq-data/java/train/bin/dict.src.txt
    trg_dict=$ENCORE/fairseq-data/java/train/bin/dict.trg.txt

    python $fairseq_dir/preprocess.py --source-lang src --target-lang trg --trainpref $trg_dir/train --validpref $trg_dir/valid --testpref $trg_dir/test --destdir $trg_dir/bin --tgtdict $trg_dict --srcdict $src_dict

fi


if [[ $opts == "oneinput"  ]]; then
    fairseq_dir=/home/tlutelli/fairseq
    echo "preprocessing data"
    python source/testing/split_remadd.py $src_dir/_test_remadd.txt $trg_dir/train.src $trg_dir/train.trg
    python source/testing/split_remadd.py $src_dir/_test_remadd.txt $trg_dir/valid.src $trg_dir/valid.trg
    python source/testing/split_remadd.py $src_dir/_test_remadd.txt $trg_dir/test.src $trg_dir/test.trg

    src_dict=$ENCORE/fairseq-data/java/contextoneinput/train/bin/dict.src.txt
    trg_dict=$ENCORE/fairseq-data/java/contextoneinput/train/bin/dict.trg.txt

    python $fairseq_dir/preprocess.py --source-lang src --target-lang trg --trainpref $trg_dir/train --validpref $trg_dir/valid --testpref $trg_dir/test --destdir $trg_dir/bin --tgtdict $trg_dict --srcdict $src_dict

fi


if [[ $opts == "context"  ]]; then
    fairseq_dir=/home/tlutelli/fairseq-context
    echo "preprocessing data"
    python source/testing/split_remadd.py $src_dir/_train_remadd.txt $trg_dir/train.src $trg_dir/train.trg
    python source/testing/split_remadd.py $src_dir/_valid_remadd.txt $trg_dir/valid.src $trg_dir/valid.trg
    python source/testing/split_remadd.py $src_dir/_test_remadd.txt $trg_dir/test.src $trg_dir/test.trg

    src_dict=$ENCORE/fairseq-data/java/context/train/bin/dict.src.txt
    trg_dict=$ENCORE/fairseq-data/java/context/train/bin/dict.trg.txt

    python $fairseq_dir/preprocess.py --source-lang src --target-lang trg --trainpref $trg_dir/train --validpref $trg_dir/valid --testpref $trg_dir/test --destdir $trg_dir/bin --tgtdict $trg_dict --srcdict $src_dict

fi

if [[ $opts == "cpp"  ]]; then
    fairseq_dir=/home/tlutelli/fairseq
    echo "preprocessing data"
    python source/testing/split_remadd.py $src_dir/_train_remadd.txt $trg_dir/train.src $trg_dir/train.trg
    python source/testing/split_remadd.py $src_dir/_valid_remadd.txt $trg_dir/valid.src $trg_dir/valid.trg
    python source/testing/split_remadd.py $src_dir/_test_remadd.txt $trg_dir/test.src $trg_dir/test.trg

    src_dict=$ENCORE/fairseq-data/cpp/train/bin/dict.src.txt
    trg_dict=$ENCORE/fairseq-data/cpp/train/bin/dict.trg.txt

    python $fairseq_dir/preprocess.py --source-lang src --target-lang trg --trainpref $trg_dir/train --validpref $trg_dir/valid --testpref $trg_dir/test --destdir $trg_dir/bin --tgtdict $trg_dict --srcdict $src_dict

fi

if [[ $opts == "js"  ]]; then
    fairseq_dir=/home/tlutelli/fairseq
    echo "preprocessing data"
    python source/testing/split_remadd.py $src_dir/_train_remadd.txt $trg_dir/train.src $trg_dir/train.trg
    python source/testing/split_remadd.py $src_dir/_valid_remadd.txt $trg_dir/valid.src $trg_dir/valid.trg
    python source/testing/split_remadd.py $src_dir/_test_remadd.txt $trg_dir/test.src $trg_dir/test.trg

    src_dict=$ENCORE/fairseq-data/javascript/train/bin/dict.src.txt
    trg_dict=$ENCORE/fairseq-data/javascript/train/bin/dict.trg.txt

    python $fairseq_dir/preprocess.py --source-lang src --target-lang trg --trainpref $trg_dir/train --validpref $trg_dir/valid --testpref $trg_dir/test --destdir $trg_dir/bin --tgtdict $trg_dict --srcdict $src_dict

fi

if [[ $opts == "python"  ]]; then
    fairseq_dir=/home/tlutelli/fairseq
    echo "preprocessing data"
    python source/testing/split_remadd.py $src_dir/_train_remadd.txt $trg_dir/train.src $trg_dir/train.trg
    python source/testing/split_remadd.py $src_dir/_valid_remadd.txt $trg_dir/valid.src $trg_dir/valid.trg
    python source/testing/split_remadd.py $src_dir/_test_remadd.txt $trg_dir/test.src $trg_dir/test.trg

    src_dict=$ENCORE/fairseq-data/python/train/bin/dict.src.txt
    trg_dict=$ENCORE/fairseq-data/python/train/bin/dict.trg.txt

    python $fairseq_dir/preprocess.py --source-lang src --target-lang trg --trainpref $trg_dir/train --validpref $trg_dir/valid --testpref $trg_dir/test --destdir $trg_dir/bin --tgtdict $trg_dict --srcdict $src_dict

fi
