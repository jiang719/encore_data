import source.compilator.d4j_setup as d4j_setup
from shutil import copyfile
import os
import subprocess
import understand
import sys
from collections import defaultdict


UNDERSTAND_PATH = '/local/tlutelli/scitools/bin/linux64/'


def load_bug_position(path):
    bug_dic  = defaultdict(list)
    fin = open(path, 'r')
    for line in fin.read().split('\n'):
        bug, file, line = line.split('@')
        bug_dic[bug].append([file, line])
    return bug_dic


def temp_get_all_projects(bug_list):
    for bug in bug_list:
        project, bug_id = bug[0].split('_')
        print("WORKING ON: ", bug[0])
        file = bug[1]
        lines = bug[2]
        understand_repo = "/local/tlutelli/d4j_context/"
        temp_repo = "/local/tlutelli/temp_context/"
        # Clean temp repo
        d4j_setup.clean_temp_folder(temp_repo)
        # Load project in temp repo
        d4j_setup.load_defects4j_project(project, bug_id + 'b', temp_repo)
        # create dir if not exist
        if not os.path.exists(understand_repo + bug[0] + '/' + file.rsplit('/', 1)[0]):
            os.makedirs(understand_repo + bug[0] + '/' + file.rsplit('/', 1)[0])
        # copy file_away
        copyfile(temp_repo + file, understand_repo + bug[0] + '/' +  file)


def create_udb(udb_path, language, project_root):
    try:
        output = subprocess.check_output(UNDERSTAND_PATH + "und create -db {udb_path} -languages {lang}".format(udb_path=udb_path, lang=language), shell=True)
        output = subprocess.check_output(UNDERSTAND_PATH + "und add -db {udb_path} {project}".format(udb_path=udb_path, project=project_root), shell=True)
    except subprocess.CalledProcessError as e:
        raise Exception


def analyze_udb(udb_path):
    try:
        output = subprocess.check_output(UNDERSTAND_PATH + "und analyze -all {udb_path}".format(udb_path=udb_path), shell=True)
    except subprocess.CalledProcessError as e:
        raise Exception


def generate_file(elem):
    try:
        typ = elem.kindname()
        if typ == 'File':
            return elem
        else:
            return generate_file(elem.parent())
    except:
        return None


def get_functions(db_path,bug_dic):
    db = understand.open(db_path)
    ents = db.ents("function,method,procedure")
    dict_functions = defaultdict(list)
    for func in ents:
        file_ent = generate_file(func)
        try:
            file_name = file_ent.longname()
            bug_checked = file_name.replace('/local/tlutelli/d4j_context/','').split('/', 1)[0]
            #print(bug_checked)
            function = func.contents()
            def_loc = '0'
            for line in func.ib():
                if "Define " in line:
                    try:
                        def_loc = line.split('(')[1].split(')')[0]

                    except:
                        print(line)
            len_func = len(function.split('\n'))
            end_loc = int(def_loc) + len_func - 1
            for b in bug_dic[bug_checked]:
                if ',' in b[1]:
                    buggy_locations = b[1].split(',')
                else:
                    buggy_locations = [b[1]]
                for buggy_location in buggy_locations:
                    if '-' in buggy_location:
                        start_buggy = int(buggy_location.split('-')[0])
                        end_buggy = int(buggy_location.split('-')[1])
                    else:
                        start_buggy = int(buggy_location)
                        end_buggy = int(buggy_location)


                    if file_name.rsplit('/',1)[-1] in b[0]:
                        if start_buggy >= int(def_loc) and start_buggy <= end_loc:
                            if start_buggy == end_buggy:
                                buggy_lines = [function.split('\n')[start_buggy - int(def_loc)]]
                            else:
                                buggy_lines = function.split('\n')[start_buggy - int(def_loc):end_buggy - int(def_loc) + 1]
                            dict_functions[bug_checked].append([file_name, start_buggy, end_buggy, buggy_lines, function, def_loc])
                            print(bug_checked)
                            print(file_name)
                            print(buggy_lines)

        except Exception as e:
            print(e)
            continue

    return dict_functions



def main():
    bug_dic = load_bug_position('source/testing/BugPositions.txt')
    #temp_get_all_projects(bug_list)
    #create_udb('data/java/context/test/understand_d4j.udb', 'java', "/local/tlutelli/d4j_context/")
    #analyze_udb('data/java/context/test/understand_d4j.udb')
    dict_functions = get_functions('data/java/context/test/understand_d4j.udb', bug_dic)
    foutrem = open('data/java/context/test/rem.txt','w')
    foutmeta = open('data/java/context/test/meta.txt','w')
    foutcontext = open('data/java/context/test/context.txt', 'w')
    for key in dict_functions:
        for v in dict_functions[key]:
            file_name, start_buggy, end_buggy, buggy_lines, function, def_loc = v
            foutmeta.write(key.replace('_', '\t') + '\t' + file_name.replace('/local/tlutelli/d4j_context/' + key + '/', '') + '\t' + str(start_buggy) + '\t' + str(end_buggy) + '\n')
            rem = ""
            context = ""
            for l in buggy_lines:
                rem += ' ' + l.strip()
            foutrem.write(rem + '\n')
            for l in function.split('\n'):
                context += ' ' + l.strip()
            foutcontext.write(context + '\n')








main()

