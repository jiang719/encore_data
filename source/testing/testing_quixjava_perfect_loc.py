import subprocess
import os


models = ['fconv_tuned_1', 'fconv_tuned_2', 'fconv_tuned_3', 'fconv_tuned_4', 'fconv_tuned_5', 'fconv_tuned_6', 'fconv_tuned_7', 'fconv_tuned_8', 'fconv_tuned_9', 'fconv_tuned_10']

for model in models:
    command = "bash source/testing/run_trained_model.sh '/home/tlutelli/ENCORE_data/fairseq-data/java/quixjava/test.src' " \
              + '/home/tlutelli/ENCORE_data/fairseq-data/java/fconv/quixjava/' + model \
              + " /home/tlutelli/ENCORE_data/fairseq-data/java/fconv/trained/" + model \
              + " /home/tlutelli/ENCORE_data/fairseq-data/java/quixjava/bin " + str(1000)

    print(command)
    p = subprocess.Popen([command], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate()
