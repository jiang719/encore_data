import subprocess
import os


models = ['context_tuned_1', 'context_tuned_2', 'context_tuned_3', 'context_tuned_4', 'context_tuned_5', 'context_tuned_6', 'context_tuned_7', 'context_tuned_8', 'context_tuned_9', 'context_tuned_10']

for model in models:
    command = "bash source/testing/run_trained_model_context.sh '/home/tlutelli/ENCORE_data/fairseq-data/java/context/quixjava/test.src' " \
              + '/home/tlutelli/ENCORE_data/fairseq-data/java/fconv/context_quixjava/' + model \
              + " /home/tlutelli/ENCORE_data/fairseq-data/java/fconv/context/" + model \
              + " /home/tlutelli/ENCORE_data/fairseq-data/java/context/quixjava/bin " + str(1000)

    print(command)
    p = subprocess.Popen([command], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate()
