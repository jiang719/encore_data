import subprocess
import os


models = ['lstm/best_modelNOD4J/best_model', 'transformer/best_modelNOD4J/best_model']

for model in models:
    command = "bash source/testing/run_trained_model.sh '/home/tlutelli/ENCORE_data/fairseq-data/java/perfectd4j_all/test.src' " \
              + '/home/tlutelli/ENCORE_data/fairseq-data/java/lstm/perfectloc_basic/' + model \
              + " /home/tlutelli/ENCORE_data/fairseq-data/java/" + model \
              + " /home/tlutelli/ENCORE_data/fairseq-data/java/perfectd4j_all/bin " + str(1000)

    print(command)
    p = subprocess.Popen([command], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate()

