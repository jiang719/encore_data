import os
import subprocess
import shutil
import json
import sys

def get_bug_branches(project_path):
    branches = []
    cmd = "cd %s; git branch -a" % project_path
    output = subprocess.check_output(cmd, shell=True)
    for line in output.split():
        if "bugs-dot-jar_" in line.decode():
            branches += [line.decode().replace(")", "")]
    return branches


def extract_bug_id(branch):
    tmp = branch[branch.index("_") + 1::]
    tmp = tmp[tmp.index("-") + 1::]
    return tmp.split("_")


def create_bug(project_path, branch_name, destination):
    FNULL = open(os.devnull, 'w')
    cmd = "cd %s; git checkout checkout -- .; git checkout %s;" % (project_path, branch_name)
    subprocess.call(cmd, shell=True, stdout=FNULL, stderr=FNULL)
    shutil.copytree(project_path, destination)


def get_human_patch(bug_path):
    diff_patch = os.path.join(bug_path, ".bugs-dot-jar", "developer-patch.diff")
    with open(diff_patch) as fd:
        return fd.read()


def getTbar_bugs():
    fout = open("/home.local/ENCORE_data/BugsdotjarBugPositions.txt", 'w')
    bugs = []
    with open("/home.local/ENCORE_data/final_bugsdotjar_data.json") as f:
        for line in f.readlines():
            b = json.loads(line)
            if 'line_removed' in b:
                bugs.append(b)

    for bug in bugs:
        bug_id = bug['project'] + '_' + bug['commit']
        filename = bug['filename']
        loc = bug['loc']
        fout.write(bug_id + '@' + filename + '@' + str(loc) + '\n')


def checkout_bugs():

    bugsdotjar_repo = "/home.local/bugs-dot-jar/"
    BUGS_PATH = "/home.local/bugs-dot-jar/bugs/"
    bug_data_path = "/home.local/ENCORE_data/bugsdotjar_data.json"

    for project_name in os.listdir(BUGS_PATH):
        project_path = os.path.join(bugsdotjar_repo, project_name)
        if os.path.isdir(project_path) and not project_name.startswith('.'):
            print(project_name)
            branches = get_bug_branches(project_path)
            #print(branches)

            for branch in branches:
                (jira_id, commit) = extract_bug_id(branch)
                bug = {
                    "project": project_name,
                    "jira_id": jira_id,
                    "commit": commit,
                }
                #print(jira_id)
                bug_project_path = os.path.join(BUGS_PATH, project_name)
                bug_path = os.path.join(bug_project_path, commit)
                #print(bug_path)
                if not os.path.exists(bug_path):
                    continue
                #    print("[Checkout] %s %s %s" % (project_name, jira_id, commit))
                #    create_bug(project_path, branch, bug_path)
                # parse human patch
                patch = get_human_patch(bug_path)
                num_file = patch.count("+++ b/")
                if num_file == 1:
                    count = 0
                    num_lines_add= patch.count("\n+") - num_file
                    num_lines_rem = patch.count("\n-") - num_file
                    if num_lines_add <= num_lines_rem and num_lines_rem == 1:
                        bug['one_line'] = True
                        patch_lines= patch.split('\n')
                        for idx, line in enumerate(patch_lines):
                            if line.startswith('---'):
                                filename = line.split('a/',1)[1]
                                bug['filename'] = filename
                            #print(line)
                            if line.startswith('@@'):
                                if ',' in line:
                                    init_loc = int(line.split(',')[0].replace('@@','').replace('-','').strip())
                                else:
                                    init_loc = int(line.split(' ')[1].replace('-',''))
                                count = 0
                            if not line.startswith('+++') and not line.startswith('---'):
                                if line.startswith('+'):
                                    if patch_lines[idx -1].startswith('-'):
                                        bug['line_added'] = line[1:].strip()
                                elif line.startswith('-'):
                                    count += 1
                                    if num_lines_add == 0 or patch_lines[idx +1].startswith('+'):
                                        #print(count)
                                        bug['line_removed'] = line[1:].strip()
                                        bug['loc'] = init_loc + 3
                                else:
                                    count += 1


                        with open(bug_data_path, 'a') as fd:
                            json.dump(bug, fd, indent=2)


def load_bugs():
    fout = open("/home.local/ENCORE_data/bugsdotjar.txt", 'w')
    bugs = []
    with open("/home.local/ENCORE_data/final_bugsdotjar_data.json") as f:
        for line in f.readlines():
            b = json.loads(line)
            if 'line_removed' in b:
                bugs.append(b)

    print(len(bugs))
    fout1 = open("/home.local/ENCORE_data/data/java/test/bugsdotjar/add.txt", 'w')
    fout2 = open("/home.local/ENCORE_data/data/java/test/bugsdotjar/rem.txt", 'w')
    fout3 = open("/home.local/ENCORE_data/data/java/test/bugsdotjar/meta.txt", 'w')
    for bug in bugs:
        if 'line_added' in bug:
            fout1.write(bug['line_added'] + '\n')
        else:
            fout1.write("\n")
        fout2.write(bug['line_removed'] + '\n')
        fout3.write(bug['project'] + '\t'
                    + str(bug['commit'])
                    + '\t' + bug['filename']
                    + '\t' + str(bug['loc']) + '\n')

    print(bugs[0])


def remove_not_1line_bug():
    bugs = []
    with open("/home.local/ENCORE_data/final_bugsdotjar_data.json") as f:
        for line in f.readlines():
            b = json.loads(line)
            bugs.append(b['commit'])
    PROJECTS = ['accumulo', 'camel','commons-math', 'flink', 'jackrabbit-oak', 'logging-log4j2', 'maven', 'wicket']
    for proj in PROJECTS:
        BUGS_PATH = "/home.local/bugs-dot-jar/bugs/" + proj
        for commit in os.listdir(BUGS_PATH):
            if commit not in bugs:
                shutil.rmtree(BUGS_PATH + '/' + commit)

#checkout_bugs()
#remove_not_1line_bug()
getTbar_bugs()

'''


accumulo
=> different test fail
mvn clean install -DskipTests -Drat.skip=true
mvn test

jackrabbit-oak:
mvn clean install
mvn test
=> different test fail

logging-log4j2
mvn clean install -Drat.skip=true -DskipTests 
mvn test
=> GOOD

flink
mvn package
=> ok


camel
=> ok but take forever

maven
mvn install -V -B -DskipTests=true -Denforcer.skip=true -Dcheckstyle.skip=true -Dcobertura.skip=true -DskipITs=true -Drat.skip=true -Dlicense.skip=true -Dfindbugs.skip=true -Dgpg.skip=true -Dskip.npm=true -Dskip.gulp=true -Dskip.bower=true
mvn test  -Drat.numUnapprovedLicenses=100


wicket
=> Cant build

commons-math




'''