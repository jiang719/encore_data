import os
import shutil
import subprocess
import threading
from source.tokenization.tokenization import token2statement, get_strings_numbers
import source.compilator.d4j_setup as d4j_setup
import sys
import timeit
import shutil


class RunCmd(threading.Thread):
    # https://stackoverflow.com/questions/4158502/kill-or-terminate-subprocess-when-timeout?noredirect=1
    def __init__(self, cmd, timeout):
        threading.Thread.__init__(self)
        self.cmd = cmd
        self.timeout = timeout
        self.out = b"TIMEOUT"
        self.err = b"TIMEOUT"

    def run(self):
        self.p = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        self.out, self.err = self.p.communicate()

    def Run(self):
        self.start()
        self.join(self.timeout)
        return self.out, self.err

        if self.is_alive():
            os.kill(self.p.pid, 0)
            self.p.kill()      #use self.p.kill() if process needs a kill -9
            self.join()


def extract_failed_test_cases(string_test_cases):
    test_cases = string_test_cases.replace("''",'').replace('\\n','').split(" - ")[1:]
    return test_cases



def get_meta_tokens(file, temp_dir):
    temp_file = temp_dir + file
    final_strings = []
    final_numbers = []
    with open(temp_file, 'r') as file:
        try:
            data = file.readlines()
        except:
            data = ""
            return False
        for idx, line in enumerate(data):
            strings, numbers = get_strings_numbers(line)
            for num in numbers:
                if num != '0' and num != '1':
                    final_numbers.append(num)
            final_strings += strings
    final_numbers = list(set(final_numbers))
    final_strings = list(set(final_strings))
    return final_strings, final_numbers


def insert_fix_codeflaws(file,  loc, patch, temp_dir):
    temp_file = temp_dir + file
    shutil.copyfile(temp_file, temp_file + '.bak')
    with open(temp_file, 'r') as file:
        try:
            data = file.readlines()
        except:
            data = ""
            return False
    with open(temp_file, 'w') as file:
        for idx, line in enumerate(data):
            if idx == loc -1:
                file.write(patch)
            else:
                file.write(line)
    return temp_file + '.bak'



def read_result_file(path,init_temp_dir):
    start_time = timeit.timeit()
    os.chdir('/home/tlutelli/ENCORE_data')
    codeflaws_dir = '/local/tlutelli/codeflaws/'
    current_meta = ""
    with open(path, 'r') as fin:
        meta = ""
        lines = fin.read().split('\n')
        #print(len(lines))
    bug = lines[7].split('\t')[0] + '/'
    #print(bug)
    #bug = path.split('/')[-1].split('_')[0]
    temp_dir = init_temp_dir + bug
    if os.path.exists(temp_dir):
        return 0
    shutil.copytree(codeflaws_dir + bug, temp_dir)
    for i, line in enumerate(lines):
        if 'START PATCH' in line:
            # Start new patch
            source = lines[i+1]
            target = lines[i+2]
            tokenized_patch = lines[i+3]
            meta = lines[i+4]
            if current_meta != meta:
                current_meta = meta
                #print("Start working on newi bug:")
                #print(meta)
            loc = int(meta.split(' ')[1].replace('\n', ''))
            file = bug.rsplit('-',1)[0].replace('-bug','') + '.c'
            #print(file)
            strings, numbers = get_meta_tokens(file, temp_dir)
            patches = token2statement(tokenized_patch.split(' '), numbers, strings)
            count = 0
            for patch in patches:
             if count <= 10:
                count +=1
                original_file = insert_fix_codeflaws(file, loc, patch + '\n', temp_dir)
                os.chdir(temp_dir)
                out, err = RunCmd(["make" ], 50).Run()
                # get number of test cases
                count_test_cases = 0
                for (dirpath, dirnames, filenames) in os.walk(temp_dir):
                    for filename in filenames:
                        if filename.startswith("heldout-input"):
                            count_test_cases += 1
                correct = 0
                if err.decode("utf-8", 'ignore') == "":
                    #print("Compilable patch " + patch.replace('\n','') )
                    #RUN TEST SUITE
                    for i in range(1, count_test_cases +1):
                        out, err = RunCmd(["./test-valid.sh", "p" + str(i)], 50).Run()
                        if "Accepted" not in out.decode("utf-8", "ignore" ):
                            #print(out.decode())
                            break
                        else:
                            #print(out.decode())
                            correct +=1
                    if correct == count_test_cases:
                        #print("Correct patch")
                        #print(patch)

                        #if os.path.isdir(temp_dir):
                        #    shutil.rmtree(temp_dir)
                        end_time = timeit.timeit()
                        print("FIX IN:")
                        print(end_time - start_time)
                        return 0
    print("No correct patch found")
    return 0


for (dirpath, dirnames, filenames) in os.walk('fairseq-data/cpp/codeflaw_all/results/'):
    for filename in filenames:
        if "ENCORE" in filename:
            read_result_file(dirpath + filename, '/local/tlutelli/temp/')
#read_result_file('fairseq-data/java/fconv/perfectloc/ENCORE_PERFECT_LOC.txt','/local/tlutelli/temp/', False)
#read_result_file('fairseq-data/java/fconv/ochoai_100loc/Chart_1dirENCORE_ochoai_5.txt',"/local/tlutelli/tempochoai_Chart1/", True)
