import sys
sys.path.append('../../source/tokenization')

import os
import shutil
import subprocess
import threading
from tokenization import token2statement, get_strings_numbers
import d4j_setup as d4j_setup
import sys
import shutil
import quixbug_test_suite as quixbug_test_suite

class RunCmd(threading.Thread):
    # https://stackoverflow.com/questions/4158502/kill-or-terminate-subprocess-when-timeout?noredirect=1
    def __init__(self, cmd, timeout):
        threading.Thread.__init__(self)
        self.cmd = cmd
        self.timeout = timeout
        self.out = b"TIMEOUT"
        self.err = b"TIMEOUT"

    def run(self):
        self.p = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        self.out, self.err = self.p.communicate()

    def Run(self):
        self.start()
        self.join(self.timeout)
        return self.out, self.err



def get_meta_tokens(file, temp_dir):
    temp_file = temp_dir + file
    final_strings = []
    final_numbers = []
    with open(temp_file, 'r') as file:
        try:
            data = file.readlines()
        except:
            data = ""
            return False
        for idx, line in enumerate(data):
            strings, numbers = get_strings_numbers(line)
            for num in numbers:
                if num != '0' and num != '1':
                    final_numbers.append(num)
            final_strings += strings
    final_numbers = list(set(final_numbers))
    final_strings = list(set(final_strings))
    return final_strings, final_numbers


def clean_temp_folder(temp_dir):
    """
    :param temp_dir: temporary directory where the patch will be compiled
    :return: nothing
    """
    if os.path.isdir(temp_dir):
        for files in os.listdir(temp_dir):
            file_p = os.path.join(temp_dir, files)
            try:
                if os.path.isfile(file_p):
                    os.unlink(file_p)
                elif os.path.isdir(file_p):
                    shutil.rmtree(file_p)
            except Exception as e:
                print(e)
    else:
        os.makedirs(temp_dir)


def create_copy_quixbug(file_name, temp_dir, quixbug_dir):
    """
    Setup the copy of Quixbug for a specific bug
    :param file_name:
    :param temp_dir:
    :param quixbug_dir:
    :return:
    """
    shutil.copyfile(quixbug_dir + "Node.java", temp_dir + "Node.java")
    shutil.copyfile(quixbug_dir + "WeightedEdge.java", temp_dir + "WeightedEdge.java")
    shutil.copyfile(quixbug_dir + file_name, temp_dir + file_name)


def compile_fix(filename, temp_dir):
    # print(filename)
    FNULL = open(os.devnull, 'w')

    p = subprocess.call(["javac",
                          temp_dir + "Node.java",
                          temp_dir + "WeightedEdge.java",
                          filename], stderr=FNULL)
    #p = subprocess.Popen(["javac",
    #                      temp_dir + "Node.java",
    #                      temp_dir + "WeightedEdge.java",
    #                      filename],
    #                     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    #p.wait()
    if p:
        return False
    else:
        return True


def insert_fix_quixbugs_several(file,  loc_start, loc_end, patch, temp_dir):
    temp_file = temp_dir + file
    patch_flag = False
    shutil.copyfile(temp_file, temp_file + '.bak')
    with open(temp_file, 'r') as file:
        try:
            data = file.readlines()
        except:
            data = ""
            return False
    count_tabs = 0
    final_patch = ""
    with open(temp_file, 'w') as file:
        for idx, line in enumerate(data):
            if idx >= loc_start -1 and idx <=  loc_end - 1 and patch_flag == False:
                file.write(patch)
                patch_flag = True
            elif idx >= loc_start -1 and idx <= loc_end -1 and patch_flag == True:
                continue
            else:
                file.write(line)
    return temp_file + '.bak'

def insert_fix_quixbugs(file,  loc, patch, temp_dir):
    temp_file = temp_dir + file
    shutil.copyfile(temp_file, temp_file + '.bak')
    with open(temp_file, 'r') as file:
        try:
            data = file.readlines()
        except:
            data = ""
            return False
    count_tabs = 0
    final_patch = ""
    with open(temp_file, 'w') as file:
        for idx, line in enumerate(data):
            if idx == loc -1:

                for i, char in enumerate(data[idx]):
                    if char != ' ':
                        break
                    else:
                        count_tabs +=1
                for i in range(0, count_tabs):
                    final_patch += " "
                file.write(final_patch + patch)
            else:
                file.write(line)
    return temp_file + '.bak'


def read_result_file(path, init_temp_dir):
    quixbug_dir = '/home/n44jiang/research-data/QuixBugs4/java_programs_bak/'
    temp_folder = '/home/n44jiang/research-data/QuixBugs4/java_programs/'
    current_meta = ""

    import codecs
    def print_write(line):
        wp = codecs.open('/home/n44jiang/python-workspace/DeepRepair/QuixBugs/validation/coconut/2010_context_nocontext_identifier.txt', 'a', 'utf-8')
        for l in line:
            wp.write(l + ' ')
        wp.write('\n')
        wp.close()

    with open(path, 'r') as fin:
        meta = ""
        lines = fin.read().split('\n')
        print(len(lines))
    bug = path.split('/')[-1].rsplit('_',1)[0]

    global cnt, right_cnt
    print_write([bug])
    print(cnt, right_cnt, bug)

    for i, line in enumerate(lines):
        if 'START PATCH' in line:
            # Start new patch
            source = lines[i+1]
            target = lines[i+2]
            tokenized_patch = lines[i+3]
            meta = lines[i+4]
            lprob = lines[i+6]
            if current_meta != meta:
                current_meta = meta

                print_write(['Start new bug'])
                print_write([meta])

            if '-' not in meta.split(' ')[1].replace('\n',''):
                loc = int(meta.split(' ')[1].replace('\n', ''))
            else:
                loc = 0
                start_loc, end_loc = meta.split(' ')[1].replace('\n','').split('-')
            file = meta.split(' ')[0] + '.java'
            strings, numbers = get_meta_tokens(file, quixbug_dir)
            patches = token2statement(tokenized_patch.split(' '), numbers, strings)

            def test_patch(patches):
                for patch in patches:

                    #print(patch)
                    clean_temp_folder(temp_folder)
                    create_copy_quixbug(file, temp_folder, quixbug_dir)
                    #exit()
                    if loc > 0:
                        original_file = insert_fix_quixbugs(file, loc, patch + '\n', temp_folder)
                    else:
                        original_file = insert_fix_quixbugs_several(file, int(start_loc), int(end_loc), patch + '\n', temp_folder)
                    res = compile_fix(temp_folder + file, temp_folder)
                    if res:
                        global cnt, right_cnt
                        print(cnt, right_cnt, patch, lprob)
                        print_write([patch])
                        print_write([lprob])

                        pass_suite = quixbug_test_suite.pass_test_suite(file.replace('.java','').lower())
                        if pass_suite >= 0:
                            right_cnt += 1
                            print("Correct patch:", file.replace('.java',''))
                            print(patch)
                            print_write(['Correct patch'])
                            return 0
                return -1

            flag = test_patch(patches)
            if flag == 0:
                break

cnt, right_cnt = 0, 0
for (dirpath, dirnames, filenames) in os.walk('/home/n44jiang/python-workspace/DeepRepair/QuixBugs/merged_quixjava/' + \
                                              'coconut/2010_context_nocontext_identifier/'):
    for filename in filenames:
        cnt += 1
        #print(cnt, filename)
        read_result_file(dirpath + filename, None)
        #break
#read_result_file('fairseq-data/java/fconv/perfectloc/ENCORE_PERFECT_LOC.txt','/local/tlutelli/temp/', False)
#read_result_file('fairseq-data/java/fconv/ochoai_100loc/Chart_1dirENCORE_ochoai_5.txt',"/local/tlutelli/tempochoai_Chart1/", True)

