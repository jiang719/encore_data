import os
import shutil
import subprocess
import threading
from source.tokenization.tokenization import token2statement, get_strings_numbers
import source.compilator.d4j_setup as d4j_setup
import sys
import shutil


class RunCmd(threading.Thread):
    # https://stackoverflow.com/questions/4158502/kill-or-terminate-subprocess-when-timeout?noredirect=1
    def __init__(self, cmd, timeout):
        threading.Thread.__init__(self)
        self.cmd = cmd
        self.timeout = timeout
        self.out = b"TIMEOUT"
        self.err = b"TIMEOUT"

    def run(self):
        self.p = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        self.out, self.err = self.p.communicate()

    def Run(self):
        self.start()
        self.join(self.timeout)
        return self.out, self.err

        if self.is_alive():
            os.kill(self.p.pid, 0)
            self.p.kill()      #use self.p.kill() if process needs a kill -9
            self.join()


def get_meta_tokens(file, temp_dir):
    temp_file = temp_dir + file
    final_strings = []
    final_numbers = []
    with open(temp_file, 'r') as file:
        try:
            data = file.readlines()
        except:
            data = ""
            return False
        for idx, line in enumerate(data):
            strings, numbers = get_strings_numbers(line)
            for num in numbers:
                if num != '0' and num != '1':
                    final_numbers.append(num)
            final_strings += strings
    final_numbers = list(set(final_numbers))
    final_strings = list(set(final_strings))
    return final_strings, final_numbers


def insert_fix_quixbugs(file,  loc, patch, temp_dir):
    temp_file = temp_dir + file
    shutil.copyfile(temp_file, temp_file + '.bak')
    with open(temp_file, 'r') as file:
        try:
            data = file.readlines()
        except:
            data = ""
            return False
    count_tabs = 0
    final_patch = ""
    with open(temp_file, 'w') as file:
        for idx, line in enumerate(data):
            if idx == loc -1:

                for i, char in enumerate(data[idx]):
                    if char != ' ':
                        break
                    else:
                        count_tabs +=1
                for i in range(0, count_tabs):
                    final_patch += " "
                file.write(final_patch + patch)
            else:
                file.write(line)
    return temp_file + '.bak'


def read_result_file(path, init_temp_dir):

    to_copy = '/local/tlutelli/quixbugs/'
    quixbug_dir = to_copy + 'python_programs/'
    current_meta = ""
    with open(path, 'r') as fin:
        meta = ""
        lines = fin.read().split('\n')
        print(len(lines))
    bug = path.split('/')[-1].rsplit('_',1)[0]
    temp_dir = init_temp_dir + bug + '/'
    temp_file_dir = temp_dir + 'python_programs/'
    if os.path.isdir(temp_dir):
        return
    shutil.copytree(to_copy, temp_dir)
    for i, line in enumerate(lines):
        if 'START PATCH' in line:
            # Start new patch
            source = lines[i+1]
            target = lines[i+2]
            tokenized_patch = lines[i+3]
            meta = lines[i+4]
            if current_meta != meta:
                current_meta = meta
                print("Start working on new bug:")
                print(meta)
            loc = int(meta.split(' ')[1].replace('\n', ''))
            file = meta.split(' ')[0].lower() + '.py'
            strings, numbers = get_meta_tokens(file, temp_file_dir)
            #patches = token2statement(tokenized_patch.split(' '), numbers, strings)
            patches = token2statement(tokenized_patch.split(' '), numbers, strings)

            def test_patch(patches):
                for patch in patches:
                    #print(patch)
                    original_file = insert_fix_quixbugs(file, loc, patch + '\n', temp_file_dir)
                    #shutil.copytree(codeflaws_dir + bug, temp_dir)
                    os.chdir(temp_dir)
                    out, err = RunCmd(["python", "tester.py", file.replace('.py','')], 1000).Run()
                    # get number of test cases
                    #print(out.decode())
                    #print(err.decode())
                    good_python = []
                    bad_python = []
                    start = 0

                    for line in out.decode().split('\n'):
                        #print(line)
                        if line.startswith('Correct'):
                            start = 1
                            good_python.append(line.replace("Correct ",''))
                        if line.startswith('Bad'):
                            bad_python.append(line.replace("Bad ",''))
                            start = 2
                        if start == 1 and line != "":
                            good_python.append(line.replace('Correct',''))
                        if start == 2 and line != "":
                            bad_python.append(line.replace('Bad',''))
                    #print(good_python)
                    #print(bad_python)
                    if bad_python == good_python and "TIMEOUT" not in out.decode():
                        print("Correct patch: ", file.replace('.py',''))
                        print(patch)
                        print("Target patch: ")
                        print(target)
                        print(out.decode())
                        return 0
                    #else:
                    #    print(good_python)
                    #    print(bad_python)
                    #    print(out.decode())
                    #    #sys.exit()
                #print("Bad patch: ", file.replace('.py',''))
                #print(patch)
                #print(out.decode())
                #print(err.decode())
                return -1
            flag = test_patch(patches)
            if flag == 0:
                break;

                '''
                count_test_cases = 0
                for (dirpath, dirnames, filenames) in os.walk(temp_dir):
                    for filename in filenames:
                        if filename.startswith("heldout-input"):
                            count_test_cases += 1
                correct = 0
                if err.decode() == "":
                    #RUN TEST SUITE
                    for i in range(1, count_test_cases +1):
                        out, err = RunCmd(["./test-valid.sh", "p" + str(i)], 50).Run()
                        if "Accepted" not in out.decode():
                            print(out.decode())
                            break
                        else:
                            correct +=1
                    if correct == count_test_cases:
                        print("Correct patch")
                        sys.exit()
                        if os.path.isdir(temp_dir):
                            shutil.rmtree(temp_dir)
                '''

for (dirpath, dirnames, filenames) in os.walk('/home/tlutelli/ENCORE_data/fairseq-data/python/fconv/results/ALL/'):
    for filename in filenames:
        if "ENCORE" in filename:
            read_result_file(dirpath + filename, '/local/tlutelli/temp_QUIXBUG/')
            #break;
#read_result_file('fairseq-data/java/fconv/perfectloc/ENCORE_PERFECT_LOC.txt','/local/tlutelli/temp/', False)
#read_result_file('fairseq-data/java/fconv/ochoai_100loc/Chart_1dirENCORE_ochoai_5.txt',"/local/tlutelli/tempochoai_Chart1/", True)
