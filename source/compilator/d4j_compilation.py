import os
import shutil
import subprocess
import threading
from source.tokenization.tokenization import token2statement, get_strings_numbers
import source.compilator.d4j_setup as d4j_setup
import sys
import psutil

class RunCmd(threading.Thread):
    # https://stackoverflow.com/questions/4158502/kill-or-terminate-subprocess-when-timeout?noredirect=1
    def __init__(self, cmd, timeout):
        threading.Thread.__init__(self)
        self.cmd = cmd
        self.timeout = timeout
        self.out = "TIMEOUT"
        self.err = "TIMEOUT"

    def run(self):
        self.p = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        self.out, self.err = self.p.communicate()

    def Run(self):
        self.start()
        self.join(self.timeout)
        return self.out, self.err

        if self.is_alive():
            current_process = psutil.Process()
            children = current_process.children(recursive=True)
            for child in children:
                print('Killing Child pid {}'.format(child.pid))
                os.kill(child.pid, signal.SIGTERM)
                os.kill(child.pid, signal.SIGKILL)
            os.kill(self.p.pid, signal.SIGTERM)
            os.kill(self.p.pid, signal.SIGKILL)


def extract_failed_test_cases(string_test_cases):
    test_cases = string_test_cases.replace("''",'').replace('\\n','').split(" - ")[1:]
    return test_cases


def get_meta_tokens(file, temp_dir, loc_start, loc_end):
    temp_file = temp_dir + file
    final_strings = []
    final_numbers = []
    with open(temp_file, 'r') as file:
        try:
            data = file.readlines()
        except:
            data = ""
            return False
        '''
        for i in range(loc_start-1, loc_end-1):
            line = data[i]
            strings, numbers = get_strings_numbers(line)
            for num in numbers:
                if num != '0' and num != '1':
                    final_numbers.append([num, max(loc_start - i, loc_end - i)])
        '''
        for idx, line in enumerate(data):
            strings, numbers = get_strings_numbers(line)
            for num in numbers:
                if num != '0' and num != '1':
                    final_numbers.append([num, max(loc_start - idx, loc_end - idx)])
            for str in strings:
                final_strings.append([str, max(loc_start - idx, loc_end - idx)])
    final_numbers.sort(key=lambda x: x[1])
    final_strings.sort(key=lambda x: x[1])
    return final_strings, final_numbers


def insert_fix_defects4j(file,  loc_start, loc_end, patch, temp_dir):
    temp_file = temp_dir + file
    shutil.copyfile(temp_file, temp_file + '.bak')
    with open(temp_file, 'r') as file:
        try:
            data = file.readlines()
        except:
            data = ""
            return False
    patch_flag = False
    with open(temp_file, 'w') as file:
        for idx, line in enumerate(data):
            if idx >= loc_start -1 and idx <=  loc_end - 1 and patch_flag == False:
                file.write(patch)
                patch_flag = True
            elif idx >= loc_start -1 and idx <= loc_end -1 and patch_flag == True:
                continue
            else:
                file.write(line)
    return temp_file + '.bak'


def compile_fix(temp_folder):
    os.chdir(temp_folder)
    p = subprocess.Popen(["/local/tlutelli/defects4j/framework/bin/defects4j", "compile"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    res = False
    if "FAIL" in str(err) or "FAIL" in str(out):
        res = False
    else:
        res = True
    return res


def defects4j_test_suite(temp_folder,debug=False):
    os.chdir(temp_folder)
    out, err = RunCmd(["/local/tlutelli/defects4j/framework/bin/defects4j", "test" ], 300).Run()
    return out, err


def run_individual_test_case(temp_folder, test_case):
    out, err = RunCmd(["/local/tlutelli/defects4j/framework/bin/defects4j","test", '-w',temp_folder,'-t',test_case], 300).Run()
    return out, err


def rem_temp_dir(temp_folder):
    #print(temp_folder)
    for root, dirs, files in os.walk(temp_folder, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))
    #if os.path.isdir(temp_folder):
    #    shutil.rmtree(temp_folder)


def read_result_file(path,init_temp_dir,is_ochoai=False,debug=False):
    temp_dir = init_temp_dir
    count = 0
    found = False
    debug = True
    current_meta = ""
    with open(path, 'r') as fin:
        meta = ""
        lines = fin.read().split('\n')
        print(len(lines))

    project = temp_dir.split('_')[-2]
    bug_id = temp_dir.split('_')[-1].replace('/','')
    d4j_setup.clean_temp_folder(init_temp_dir)
    d4j_setup.load_defects4j_project(project, bug_id + 'b', init_temp_dir)
    if project == "Mockito":
        print("Mockito needs separate compilation")
        test = compile_fix(init_temp_dir)
    out_init, err_init = defects4j_test_suite(init_temp_dir)
    #test_cases_2_run_first = extract_failed_test_cases(str(out_init))
    #print(str(out_init))
    #print(err_init)
    #out_length = len(test_cases_2_run_first)
    failed_test_cases = str(out_init).split(' - ')
    out_length = len(failed_test_cases)

    print(out_length)
    c = 0
    for i, line in enumerate(lines):
        c += 1
        print(c)
        if 'START PATCH' in line:
            # Start new patch
            source = lines[i+1]
            target = lines[i+2]
            tokenized_patch = lines[i+3]
            meta = lines[i+4]
            if current_meta != meta:
                current_meta = meta
                print("Start working on new bug:")
                print(meta)

            project = lines[i+4].split(' ')[0]
            bug_id = lines[i+4].split(' ')[1]
            score = lines[i+6]
            #if os.path.isdir(temp_dir):
            #    rem_temp_dir(temp_dir)
            #    temp_dir = init_temp_dir + str(i)  + '/'
            #    clean_temp_folder(temp_dir)
            #    load_defects4j_project(project, bug_id + 'b', temp_dir)
            if is_ochoai:
                dic = {'Closure': 'src/', 'Chart': 'source/', 'Math': 'src/main/java/', 'Lang': 'src/java/', 'Time': 'src/main/java/', 'Mockito': 'src/', 'Back': 'src/main/'}
                meta = lines[i+6].replace(' $LINE$ ','\t')
                meta = meta.split('$FILE$')[1]
                #print(lines[i+4])
                meta = lines[i+4].replace(' ','\t') + '\t' + meta
                project = lines[i+4].split(' ')[0]
                metas = meta.split('\t')

                init_filename =  metas[2].strip().replace('.','/') + '.java'
                if '$' in init_filename:
                    init_filename = init_filename.split('$')[0] + '.java'
                filename = dic[project] + init_filename
                if not os.path.isfile(temp_dir + filename):
                    filename = dic['Lang'] + init_filename
                if not os.path.isfile(temp_dir + filename):
                    filename = dic['Math'] + init_filename
                if not os.path.isfile(temp_dir + filename):
                    filename = dic['Back'] + init_filename
                if not os.path.isfile(temp_dir + filename):
                    print("Error parsing meta")
                metas[2] = filename
                meta = "\t".join(metas)
            loc_start = int(meta.split(' ')[3])
            loc_end = int(meta.split(' ')[4])
            file = meta.split(' ')[2]


            strings, numbers = get_meta_tokens(file, temp_dir, loc_start, loc_end)
            strings = [item[0] for item in strings]
            numbers = [item[0] for item in numbers]

            patches = token2statement(tokenized_patch.split(' '), numbers, strings)
            #if(debug):
            #    print(patches)

            count = 0
            for patch in patches:
                print(patch)
                count += 1
                if count < 100:
                    print(loc_start)
                    print(loc_end)
                    original_file = insert_fix_defects4j(file, loc_start, loc_end, patch + '\n', temp_dir)
                    if project == 'Mockito':
                        flag = compile_fix(temp_dir)
                        #for test_case in test_cases_2_run_first:
                        #    run_individual_test_case(temp_dir,test_case)
                    out, err = defects4j_test_suite(temp_dir, debug)
                    test = False
                    #print(str(out))
                    #print(str(err))
                    if 'TIMEOUT' in str(err) or 'TIMEOUT' in str(out):
                        print("Time out for patch: ", patch)


                    if 'FAIL' in str(err) or 'FAIL' in str(out):
                        test = False
                    elif len(str(out).split(' - ')) < out_length:

                        failed_test_for_patches = str(out).split(' - ')[1:]
                        test = True
                        for failed_test_for_patch in failed_test_for_patches:
                            if failed_test_for_patch not in failed_test_cases:
                                test = False
                            print(failed_test_for_patch)
                            print("Partial patch: " + patch + "\n for:" + meta)


                    if "Failing tests: 0" in str(out):
                        print("Successful Test case pass for patch: " + patch + "\n for: " + meta )
                        exit()

                    if test and str(err) != 'TIMEOUT':
                        print("Better Partial patch: " + patch + "\n for: " + meta)
                        print(str(out))
                        exit()



                    #if patch == 'defineSlot( astParameter , functionNode , jsDocParameter.getJSType( ) , false ) ;':
                        #exit()
                    shutil.copyfile(original_file, original_file.replace('.bak', ''))

                else:
                    break



print(sys.argv)
read_result_file(sys.argv[1], sys.argv[2], int(sys.argv[3]), True)
'''
def test():
    temp_folder="/local/tlutelli/temp/"
    #load_defects4j_project('Time', str(19) + 'b', "/local/tlutelli/temp/" )
    out_init, err_init = defects4j_test_suite("/local/tlutelli/temp/")
    test_cases= extract_failed_test_cases(str(out_init))
    test_case = test_cases[1]
    #out, err =  run_individual_test_case(temp_folder, test_case)
    #print(out)
    out, err = defects4j_test_suite(temp_folder)
    print(out)
    print(err)
test()
'''
#read_result_file('fairseq-data/java/fconv/perfectloc/ENCORE_PERFECT_LOC.txt','/local/tlutelli/temp/', False)
#read_result_file('fairseq-data/java/fconv/ochoai_100loc/Chart_1dirENCORE_ochoai_5.txt',"/local/tlutelli/tempochoai_Chart1/", True)
