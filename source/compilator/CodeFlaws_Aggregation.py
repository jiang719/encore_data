
import os

fixed_bugs ={}
target = {}

fin = open('data/cpp/test/codeflaws/all_codeflaws_onelines.tsv', 'r')

lines = fin.readlines()
for line in lines:
    buggy, fix, bug, loc = line.split('\t')
    #print(bug + ' ' + loc)
    target[bug + ' ' + loc.replace('\n','')] = fix.strip().rstrip()




for root, dirs, files in os.walk('CodeFlaws_ENCORE'):
    for file in files:
        fin = open(os.path.join(root, file), 'r')
        lines = fin.read().split('\n')
        for idx, line in enumerate(lines):
            if line.startswith('Start'):
                current_bug = lines[idx + 1]
            if line.startswith("Correct patch"):
                fixed_bugs[current_bug] = lines[idx + 1]


identical = 0
for k, values in fixed_bugs.items():
    if values == target[k]:
        identical += 1
    else:
        print(k)
        print("Buggy:\t", values )
        print("Target:\t", target[k])
print(len(fixed_bugs))
print(identical)
