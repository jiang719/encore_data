# make classes for extra classes and then just parse them in
# make version for proper programs

import copy
import json
import sys
import subprocess
import types
import os
from threading import Timer
import threading
import time

QUIXBUG_MAIN_DIR = "/home/n44jiang/research-data/QuixBugs4/"
INIT_DIR = "/home/n44jiang/python-workspace/DeepRepair/ENCORE_data/"

def prettyprint(o):
    if isinstance(o, types.GeneratorType):
        return("(generator) " + str(list(o)))
    else:
        return(str(o))


graph_based = ["breadth_first_search",
               "depth_first_search",
               "detect_cycle",
               "minimum_spanning_tree",
               "reverse_linked_list",
               "shortest_path_length",
               "shortest_path_lengths",
               "shortest_paths",
               "topological_ordering"
               ]

def command(cmd, FNULL, timeout = 10):
    p = subprocess.Popen(cmd, stderr=FNULL, stdout=subprocess.PIPE, universal_newlines=True)
    t_beginning = time.time()
    while True:
        if p.poll() is not None:
            break
        seconds_passed = time.time() - t_beginning
        if timeout and seconds_passed > timeout:
            p.terminate()
            return None
        time.sleep(0.1)
    return p


def pass_test_suite(algo):
    FNULL = open(os.devnull, 'w')
    #print("Test ", algo, " with patch ", str(patch_idx))
    jar_dir = '/home/n44jiang/'
    try:
        os.chdir(QUIXBUG_MAIN_DIR)
        p1 = subprocess.Popen(["/usr/bin/javac", "-cp", ".:java_programs:"+jar_dir+"junit4-4.12.jar:"+jar_dir+
                               "hamcrest-all-1.3.jar", "java_testcases/junit/" + algo.upper() + "_TEST.java"],
                              stdout=subprocess.PIPE, stderr=FNULL, universal_newlines=True)

        p2 = command(["/usr/bin/java", "-cp", ".:java_programs:"+jar_dir+"junit4-4.12.jar:"+jar_dir+"hamcrest-all-1.3.jar",
            "org.junit.runner.JUnitCore", "java_testcases.junit." + algo.upper() + "_TEST"], FNULL, timeout = 10)
        java_out = p2.stdout.read() if p2 is not None else "FAILURES"
        #print(java_out.decode())
        #print(java_out)
        if "FAILURES" in java_out:
            os.chdir(INIT_DIR)
            return -1
        else:
            return 1
    except Exception as e:
        print(e)
        return -1
    finally:
        os.chdir(INIT_DIR)


