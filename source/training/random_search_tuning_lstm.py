import random
import os
import subprocess


def tuning_optimizer():
    #### Optimization Parameters

    clip_norm = random.uniform(0, 1)
    sentence_avg = random.choice([True, False])
    OPT = ['adagrad', 'nag', 'sgd', 'adam']
    optimizer = OPT[random.randint(0, 3)]

    CRIT = ['composite_loss', 'cross_entropy', 'adaptive_loss', 'label_smoothed_cross_entropy']
    criterion = CRIT[random.randint(0, 3)]

    lr = random.uniform(0, 1)
    momentum = random.uniform(0, 1)
    lr_shrink = random.uniform(0, 0.5)
    weight_decay = random.uniform(0, 1)


def tuning_hyper_parameters(fairseqdir, savedir, trainbin):
    dropout = random.uniform(0, 1)
    encoder_embed_dim = random.randint(50, 500)
    decoder_embed_dim = encoder_embed_dim # random.randint(50, 500)
    decoder_out_embed_dim = random.randint(50, 500)

    encoder_layers = random.randint(1, 20)
    decoder_layers = random.randint(1, 20)

    clip_norm = random.uniform(0, 1)
    OPT = ['adagrad', 'nag', 'sgd', 'adam']
    optimizer = OPT[random.randint(0, 3)]

    CRIT = ['cross_entropy', 'label_smoothed_cross_entropy']
    criterion = CRIT[random.randint(0, 1)]

    lr = random.uniform(0, 1)
    decoder_attention = random.choice([True, False])
    if decoder_attention:
        attention = ' --decoder-attention '
    else:
        attention = ''
    momentum = random.uniform(0, 1)

    dir = savedir + 'd_' + str(dropout) + '_eed_' + str(encoder_embed_dim) \
          + 'ded_' + str(decoder_embed_dim) + 'doed' + str(decoder_out_embed_dim) \
          + 'el_' + str(encoder_layers) + 'dl_' + str(decoder_layers) + 'lr_' + str(lr) + 'm_' \
          + str(momentum) + 'cn_' + str(clip_norm) + optimizer + '_' + criterion + '_da' + str(decoder_attention)
    print(dir)

    # print(string_parameters)
    cmd = 'python ' + fairseqdir + 'train.py --save-dir ' + dir + \
             ' --arch lstm  --max-tokens 1000 --distributed-world-size 1  --no-epoch-checkpoints --log-format json ' + \
             '--encoder-embed-dim ' + str(encoder_embed_dim) + \
             ' --decoder-embed-dim ' + str(decoder_embed_dim) + \
             ' --decoder-out-embed-dim ' + str(decoder_out_embed_dim) + \
             ' --encoder-layers "' + str(encoder_layers) + \
             '" --decoder-layers "' + str(decoder_layers) + \
             '" --dropout ' + str(dropout) + \
             ' --clip-norm ' + str(clip_norm) + \
             ' --lr ' + str(lr) + \
             ' --optimizer ' + optimizer + \
             ' --criterion ' + criterion + \
          ' --momentum ' + str(momentum) + \
          ' --decoder-attention ' + str(decoder_attention) + \
          ' --max-epoch 1  --batch-size 32 ' + trainbin

    # training
    return cmd, dir


def main():
    fairseqdir = os.environ['FAIRSEQPY']
    init_dir = os.getcwd()
    savedir = init_dir + '/fairseq-data/java/lstm/models/'

    trainbin = init_dir + '/fairseq-data/java/train/bin'
    for i in range(1, 500):
        cmd, dir = tuning_hyper_parameters(fairseqdir, savedir, trainbin)
        if not os.path.exists(dir):
            os.makedirs(dir)
        cmd = cmd + " | tee " + dir + "/log.txt"
        print(cmd)
        subprocess.call(cmd, shell=True)


main()
