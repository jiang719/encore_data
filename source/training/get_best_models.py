import os
import json
import itertools

languages = ['java', 'javascript', 'python', 'cpp']

def get_best_parameters(path):
    results = {}
    for (dirpath, dirnames, filenames) in os.walk(path):
        for filename in filenames:
            if filename.endswith('.pt'):
                os.remove(dirpath + '/' + filename)
            if filename.endswith('log.txt'):
                fin = open(dirpath + '/' + filename)
                lines = fin.readlines()
                last_line = lines[-2]
                j = json.loads(last_line)
                try:
                    results[dirpath.split('/')[-1]] = float(j['valid_ppl'])
                except:
                    # Results is inf and not a float, we ignore
                    continue

    d_sort = [(k, results[k]) for k in sorted(results, key=results.get, reverse = False)]
    #for k, v in d_sort:
    #    print(v, k)
    return(d_sort)

for language in languages:
    i = 0
    # java is normal because we only tune for Java
    best_param_sorted = get_best_parameters('fairseq-data/java/fconv/models/')
    for k, v in best_param_sorted[:10]:
        i += 1
        print(v, ["".join(x) for _, x in itertools.groupby(k, key=str.isdigit)])
        parsed_data = ["".join(x) for _, x in itertools.groupby(k, key=str.isdigit)]
        dropout = "".join(parsed_data[1:4])
        share_input_output_embed = False
        if 'True' in parsed_data[4]:
            share_input_output_embed = True
        encoder_embed_dim = parsed_data[5]
        decoder_embed_dim = parsed_data[7]
        decoder_out_embed_dim = parsed_data[9]

        encoder_layers_1 = parsed_data[11]
        encoder_layers_2 = parsed_data[13]
        encoder_layers_3 = parsed_data[15]
        encoder_layers = "'[(" + str(encoder_layers_1) + ',' + str(encoder_layers_2) + ')] * ' + str(encoder_layers_3) + "'"

        decoder_layers_1 = int(parsed_data[17])
        decoder_layers_2 = int(parsed_data[19])
        decoder_layers_3 = int(parsed_data[21])
        decoder_layers = "'[(" + str(decoder_layers_1) + ',' + str(decoder_layers_2) + ')] * ' + str(decoder_layers_3) + "'"

        lr = "".join(parsed_data[23:26])
        momentum = "".join(parsed_data[27:30])
        clip_norm ="".join(parsed_data[31:34])
        optimizer = parsed_data[34].split('_')[0]
        criterion =parsed_data[34].split('_', 1)[-1].replace('_lrs', '')
        savedir = 'fairseq-data/' + language + '/fconv/trained/fconv_tuned_' + str(i)
        trainbin = 'fairseq-data/' + language + '/train/bin'
        fin = open('source/training/auto_scripts/' + language + 'fconv_tuned_' + str(i) + '.py', 'w')
        fin.write('from source.training.train import train_fconv\n')
        fin.write('train_fconv(' + str(dropout) + ',' + str(share_input_output_embed) + ',' + str(encoder_embed_dim) + ',' + \
                  str(decoder_embed_dim) + ',' + str(decoder_out_embed_dim) + ',' + str(encoder_layers) + ',' + str(decoder_layers) + ',' + \
                  str(lr) + ',' + str(momentum) + ',' + str(clip_norm) + ",'" + optimizer + "','" + criterion + "','" + savedir + "','" + trainbin + "')\n")

