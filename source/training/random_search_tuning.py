import random
import os
import subprocess


def tuning_optimizer():
    #### Optimization Parameters

    clip_norm = random.uniform(0, 1)
    sentence_avg = random.choice([True, False])
    OPT = ['adagrad', 'nag', 'sgd', 'adam']
    optimizer = OPT[random.randint(0, 3)]

    CRIT = ['composite_loss', 'cross_entropy', 'adaptive_loss', 'label_smoothed_cross_entropy']
    criterion = CRIT[random.randint(0, 3)]

    lr = random.uniform(0, 1)
    momentum = random.uniform(0, 1)
    lr_shrink = random.uniform(0, 0.5)
    weight_decay = random.uniform(0, 1)


def tuning_hyper_parameters(fairseqdir, savedir, trainbin):
    dropout = random.uniform(0, 1)
    share_input_output_embed = random.choice([True, False])
    encoder_embed_dim = random.randint(50, 500)
    decoder_embed_dim = encoder_embed_dim # random.randint(50, 500)
    if share_input_output_embed:
        decoder_out_embed_dim = decoder_embed_dim
        share = ' --share-input-output-embed '
    else:
        decoder_out_embed_dim = random.randint(50, 500)
        share = ''

    encoder_layers_1 = 128 * random.randint(1, 5)
    encoder_layers_2 = random.randint(2, 10)
    encoder_layers_3 = random.randint(2, 10)

    encoder_layers = '[(' + str(encoder_layers_1) + ',' + str(encoder_layers_2) + ')] * ' + str(encoder_layers_3)
    encoder_layers_string = str(encoder_layers_1) + '_' + str(encoder_layers_2) + '_' + str(encoder_layers_3)

    decoder_layers_1 = 128 * random.randint(1, 5)
    decoder_layers_2 = random.randint(1, 10)
    decoder_layers_3 = random.randint(1, 10)

    decoder_layers = '[(' + str(decoder_layers_1) + ',' + str(decoder_layers_2) + ')] * ' + str(decoder_layers_3)
    decoder_layers_string = str(decoder_layers_1) + '_' + str(decoder_layers_2) + '_' + str(decoder_layers_3)



    clip_norm = random.uniform(0, 1)
    OPT = ['adagrad', 'nag', 'sgd']
    optimizer = OPT[random.randint(0, 2)]

    CRIT = ['cross_entropy', 'label_smoothed_cross_entropy']
    criterion = CRIT[random.randint(0, 1)]

    lr = random.uniform(0, 1)
    momentum = random.uniform(0, 1)

    dir = savedir + 'd_' + str(dropout) + 's_' + str(share_input_output_embed) +  '_eed_' + str(encoder_embed_dim) \
                        + 'ded_' + str(decoder_embed_dim) + 'doed' + str(decoder_out_embed_dim) \
                        + 'el_' + encoder_layers_string + 'dl_' + decoder_layers_string  + 'lr_' + str(lr) + 'm_' + \
                        str(momentum) + 'cn_' + str(clip_norm) + optimizer + '_' + criterion + '_lrs'

    # print(string_parameters)
    cmd = 'python ' + fairseqdir + 'train.py --save-dir ' + dir + \
             ' --arch fconv  --max-tokens 1000 --distributed-world-size 1  --no-epoch-checkpoints --log-format json ' + \
             '--encoder-embed-dim ' + str(encoder_embed_dim) + \
             ' --decoder-embed-dim ' + str(decoder_embed_dim) + \
             ' --decoder-out-embed-dim ' + str(decoder_out_embed_dim) + \
             ' --encoder-layers "' + encoder_layers + \
             '" --decoder-layers "' + decoder_layers + \
             '" --dropout ' + str(dropout) + \
             share + \
             ' --clip-norm ' + str(clip_norm) + \
             ' --lr ' + str(lr) + \
             ' --optimizer ' + optimizer + \
             ' --criterion ' + criterion + \
          ' --momentum ' + str(momentum) + \
          ' --max-epoch 1  --batch-size 32 ' + trainbin

    # training
    return cmd, dir


def main():
    fairseqdir = os.environ['FAIRSEQPY']
    init_dir = os.getcwd()
    savedir = init_dir + '/fairseq-data/java/fconv/models/'

    trainbin = init_dir + '/fairseq-data/java/train/bin'
    for i in range(1, 500):
        cmd, dir = tuning_hyper_parameters(fairseqdir, savedir, trainbin)
        if not os.path.exists(dir):
            os.makedirs(dir)
        cmd = cmd + " | tee " + dir + "/log.txt"
        print(cmd)
        subprocess.call(cmd, shell=True)


main()
