import random
import os
import subprocess


def tuning_optimizer():
    #### Optimization Parameters

    clip_norm = random.uniform(0, 1)
    sentence_avg = random.choice([True, False])
    OPT = ['adagrad', 'nag', 'sgd', 'adam']
    optimizer = OPT[random.randint(0, 3)]

    CRIT = ['composite_loss', 'cross_entropy', 'adaptive_loss', 'label_smoothed_cross_entropy']
    criterion = CRIT[random.randint(0, 3)]

    lr = random.uniform(0, 1)
    momentum = random.uniform(0, 1)
    lr_shrink = random.uniform(0, 0.5)
    weight_decay = random.uniform(0, 1)


def tuning_hyper_parameters(fairseqdir, savedir, trainbin):
    decoder_attention_head = random.randint(1, 20)
    decoder_embed_dim = decoder_attention_head * random.randint(1, 10)
    decoder_layers = decoder_attention_head
    
    encoder_attention_head = decoder_attention_head
    encoder_embed_dim = decoder_embed_dim
    encoder_layers = encoder_attention_head
    
    dropout = random.uniform(0, 1)
    att_dropout = random.uniform(0, 1)
    relu_dropout = random.uniform(0, 1)
    clip_norm = random.uniform(0, 1)

    OPT = ['adagrad', 'nag', 'sgd', 'adam']
    optimizer = OPT[random.randint(0, 3)]

    CRIT = ['cross_entropy', 'label_smoothed_cross_entropy']
    criterion = CRIT[random.randint(0, 1)]

    lr = random.uniform(0, 1)
    momentum = random.uniform(0, 1)

    dir = savedir + 'd_' + str(dropout) + '_eed_' + str(encoder_embed_dim) \
          + 'ded_' + str(decoder_embed_dim) + 'el' + str(encoder_layers) \
          + 'ad_' + str(att_dropout) + 'rd_' + str(relu_dropout) + 'lr_' + str(lr) + 'm_' \
          + str(momentum) + 'cn_' + str(clip_norm) + optimizer + '_' + criterion
    print(dir)

    # print(string_parameters)
    cmd = 'python ' + fairseqdir + 'train.py --save-dir ' + dir + \
          ' --arch transformer  --max-tokens 1000 --distributed-world-size 1  --no-epoch-checkpoints --log-format json ' + \
          '--encoder-embed-dim ' + str(encoder_embed_dim) + \
          ' --decoder-embed-dim ' + str(decoder_embed_dim) + \
          ' --encoder-attention-heads ' + str(encoder_attention_head) + \
          ' --decoder-attention-heads ' + str(decoder_attention_head) + \
          ' --encoder-layers ' + str(encoder_layers) + \
          ' --decoder-layers ' + str(decoder_layers) + \
          ' --dropout ' + str(dropout) + \
          ' --attention-dropout ' + str(att_dropout) + \
          ' --relu-dropout ' + str(relu_dropout) + \
          ' --clip-norm ' + str(clip_norm) + \
          " --optimizer " + optimizer + \
          " --criterion " + criterion + \
          " --lr " + str(lr) + \
          " --min-lr 1e-4 --momentum " + str(momentum) + \
          " --max-epoch 1 --batch-size 32 " + trainbin

    # training
    return cmd, dir


def main():
    fairseqdir = os.environ['FAIRSEQPY']
    init_dir = os.getcwd()
    savedir = init_dir + '/fairseq-data/java/transformer/models/'

    trainbin = init_dir + '/fairseq-data/java/train/bin'
    for i in range(1, 500):
        cmd, dir = tuning_hyper_parameters(fairseqdir, savedir, trainbin)
        if not os.path.exists(dir):
            os.makedirs(dir)
        cmd = cmd + " | tee " + dir + "/log.txt"
        print(cmd)
        subprocess.call(cmd, shell=True)


main()
