import os
import subprocess
from source.training.split_remadd import split_exclude


def main():
    fairseq = os.environ['FAIRSEQPY']
    languages = ['java',]
    init_dir = os.getcwd()
    for language in languages:
        print("Pre-processing data for :", language)
        data_dir = init_dir + '/data/' + language + '/single_input/'
        train_dir = data_dir + 'train/'
        fairseq_dir = init_dir + '/fairseq-data/' + language + '/single_input/train/'
        if not os.path.exists(fairseq_dir):
            os.makedirs(fairseq_dir)
        split_exclude(data_dir + 'exclude.txt', train_dir + '_train_remadd.txt',  train_dir + '_train_meta.txt',
                      fairseq_dir + 'train.src', fairseq_dir + 'train.trg', fairseq_dir + 'train.meta')
        split_exclude(data_dir + 'exclude.txt', train_dir + '_valid_remadd.txt',  train_dir + '_valid_meta.txt',
                      fairseq_dir + 'valid.src', fairseq_dir + 'valid.trg', fairseq_dir + 'valid.meta')
        split_exclude(data_dir + 'exclude.txt', train_dir + '_test_remadd.txt',  train_dir + '_test_meta.txt',
                      fairseq_dir + 'test.src', fairseq_dir + 'test.trg', fairseq_dir + 'test.meta')
        cmd = 'python ' + fairseq + 'preprocess.py ' + '--source-lang src --target-lang trg --workers 20 ' + \
            '--trainpref ' + fairseq_dir + 'train ' + \
            '--validpref ' + fairseq_dir + 'valid ' + \
            '--testpref ' + fairseq_dir + 'test ' + \
            '--destdir ' + fairseq_dir + 'bin'
        subprocess.call(cmd, shell=True)


main()

