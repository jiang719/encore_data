import os
import sys
import pickle
import random
from operator import add
import source.tokenization.tokenization as tokenization

valid_size = 10000
test_size = 1000

def save_data(data, label, meta, path, name):
    foutd = open(path + name + "_remadd.txt", 'w', encoding='utf-8', errors='ignore')
    for i, d in enumerate(data):
        foutd.write(" ".join(d).replace('\n', '').replace('\t', '') + '\t' + " ".join(label[i]).replace('\n', '').replace('\t', '') + '\n')
    foutm = open(path + name + "_meta.txt", 'w', encoding='utf-8', errors='ignore')
    for m in meta:
        foutm.write(m+'\n')


# https://stackoverflow.com/questions/23289547/shuffle-two-list-at-once-with-same-order
def unison_shuffled_copies(a, b, c):
    try:
        assert len(a) == len(b)
        assert len(a) == len(c)
        d = list(zip(a, b, c))
        random.shuffle(d)
        a, b, c = zip(*d)
        return a, b, c
    except:
        print(len(a))
        print(len(b))
        print(len(c))
        sys.exit()

def get_training_testing(data, labels, meta_list, valid_size, test_size, shuffle=False):
    if shuffle:
        data, labels, meta_list = unison_shuffled_copies(data, labels, meta_list)
        train_data = data[:-(test_size + valid_size)]
        train_labels = labels[:-(test_size + valid_size)]
        train_meta = meta_list[:-(test_size + valid_size)]
        valid_data = data[-(test_size + valid_size):-test_size]
        valid_labels = labels[-(test_size + valid_size):-test_size]
        valid_meta = meta_list[-(test_size + valid_size):-test_size]
        test_data = data[-test_size:]
        test_labels = labels[-test_size:]
        test_meta = meta_list[-test_size:]
    else:
        train_data = data[:-(test_size + valid_size)]
        train_labels = labels[:-(test_size + valid_size)]
        train_meta = meta_list[:-(test_size + valid_size)]
        valid_data = data[-(test_size + valid_size):-test_size]
        valid_labels = labels[-(test_size + valid_size):-test_size]
        valid_meta = meta_list[-(test_size + valid_size):-test_size]
        test_data = data[-test_size:]
        test_labels = labels[-test_size:]
        test_meta = meta_list[-test_size:]
    return([train_data, train_labels, train_meta],
           [valid_data, valid_labels, valid_meta],
           [test_data, test_labels, test_meta])


def tokenize_df(df):
    df['buggy_lines'] = df['buggy_lines'].apply(lambda x: tokenization.tokenize(x.decode("utf-8")))
    df['clean_lines'] = df['clean_lines'].apply(lambda x: tokenization.tokenize(x.decode("utf-8")))
    df['context_before'] = df['context_before'].apply(lambda x: tokenization.tokenize(x.decode("utf-8")))
    df['context_after'] = df['context_after'].apply(lambda x: tokenization.tokenize(x.decode("utf-8")))
    print(df.sample(5))
    return df


def main():
    languages = ['java']
    init_dir = os.getcwd()
    for language in languages:
        saved_data_path = init_dir + "/data/" + language + "/single_input/train/"
        # open raw dataframe
        input_data= "/local/tlutelli/coconut_framework/data/dataframes/java/raw_data.pickle"
        with open(input_data, 'rb') as handle:
            dataframe = pickle.load(handle)
        print(dataframe.shape)
        for col in dataframe.columns: 
            print(col) 
        dataframe = tokenize_df(dataframe)
        dataframe["rem_toks"] = dataframe["context_before"].apply(lambda x: " ".join(x)) + " <START> " + dataframe["buggy_lines"].apply(lambda x: " ".join(x)) + " <END> " + dataframe["context_after"].apply(lambda x: " ".join(x))
        dataframe["add_toks"] = dataframe["clean_lines"].apply(lambda x: " ".join(x))
        meta_lines = []
        rem_toks = []
        add_toks = []
        i = 0
        for index, rows in dataframe.iterrows():
            i += 1
            rem_toks.append(rows.rem_toks)
            add_toks.append(rows.add_toks)
            meta_lines.append(str(i))

        print(rem_toks[0])
        print(rem_toks[1])
        print(add_toks[0])
        print(add_toks[1])
        [train_data, train_labels, train_meta], \
        [valid_data, valid_labels, valid_meta], \
        [test_data, test_labels, test_meta] = get_training_testing(rem_toks, add_toks,
                                                                   meta_lines,
                                                                   valid_size=valid_size,
                                                                   test_size=test_size,
                                                                   shuffle=True)
        save_data(train_data, train_labels, train_meta, saved_data_path, '_train')
        save_data(valid_data, valid_labels, valid_meta, saved_data_path, '_valid')
        save_data(test_data, test_labels, test_meta, saved_data_path, '_test')

        
main()
