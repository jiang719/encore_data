import os
import random
import source.tokenization.tokenization as tokenization
import sys
from operator import add
valid_size = 10000
test_size = 1000


def save_data(data, label, meta, path, name):
    foutd = open(path + name + "_remadd.txt", 'w', encoding='utf-8', errors='ignore')
    foutm = open(path + name + "_meta.txt", 'w', encoding='utf-8', errors='ignore')
    for i, d in enumerate(data):
        foutd.write(" ".join(d).replace('\n', '').replace('\t', '') + '\t' + " ".join(label[i]).replace('\n', '').replace('\t', '') + '\n')
        foutm.write(meta[i]+'\n')


# https://stackoverflow.com/questions/23289547/shuffle-two-list-at-once-with-same-order
def unison_shuffled_copies(a, b, c):
    try:
        assert len(a) == len(b)
        assert len(a) == len(c)
        d = list(zip(a, b, c))
        random.shuffle(d)
        a, b, c = zip(*d)
        return a, b, c
    except:
        print(len(a))
        print(len(b))
        print(len(c))
        sys.exit()

def get_training_testing(data, labels, meta_list, valid_size, test_size, shuffle=False):
    if shuffle:
        data, labels, meta_list = unison_shuffled_copies(data, labels, meta_list)
        train_data = data[:-(test_size + valid_size)]
        train_labels = labels[:-(test_size + valid_size)]
        train_meta = meta_list[:-(test_size + valid_size)]
        valid_data = data[-(test_size + valid_size):-test_size]
        valid_labels = labels[-(test_size + valid_size):-test_size]
        valid_meta = meta_list[-(test_size + valid_size):-test_size]
        test_data = data[-test_size:]
        test_labels = labels[-test_size:]
        test_meta = meta_list[-test_size:]
    else:
        train_data = data[:-(test_size + valid_size)]
        train_labels = labels[:-(test_size + valid_size)]
        train_meta = meta_list[:-(test_size + valid_size)]
        valid_data = data[-(test_size + valid_size):-test_size]
        valid_labels = labels[-(test_size + valid_size):-test_size]
        valid_meta = meta_list[-(test_size + valid_size):-test_size]
        test_data = data[-test_size:]
        test_labels = labels[-test_size:]
        test_meta = meta_list[-test_size:]
    return([train_data, train_labels, train_meta],
           [valid_data, valid_labels, valid_meta],
           [test_data, test_labels, test_meta])


#https://stackoverflow.com/questions/519633/lazy-method-for-reading-big-file-in-python
def read_in_chunks(file_object, chunk_size=1024):
    """Lazy function (generator) to read a file piece by piece.
    Default chunk size: 1k."""
    while True:
        data = file_object.read(chunk_size)
        if not data:
            break
        yield data

def process_data(lines):
    tokens = [tokenization.tokenize(line) for line in lines]
    return tokens


def main_context():
    languages = ['python']
    init_dir = os.getcwd()

    for language in languages:
        saved_data_path = init_dir + "/data/" + language + "/context/train/"
        data_path = '/local/tlutelli/coconut_framework/data/' + language + '/context/training/origin'
        rem_path = os.path.join(data_path, 'rem.txt')
        add_path = os.path.join(data_path, 'add.txt')
        context_path = os.path.join(data_path, 'context.txt')
        meta_path = os.path.join(data_path, 'meta.txt')
        
        rem_lines = open(rem_path)
        add_lines = open(add_path)
        context_lines = open(context_path)
        meta_lines_init = open(meta_path).read().split('\n')
        

        BUF_SIZE = 10000000
        
        
        rem_toks_init = []
        add_toks_init = []
        context_toks_init = []
        
        tmp_lines = add_lines.readlines(BUF_SIZE)
        while tmp_lines:
            add_toks_init += process_data(tmp_lines)
            tmp_lines = add_lines.readlines(BUF_SIZE)      
            print(len(add_toks_init))  
        
        tmp_lines = context_lines.readlines(BUF_SIZE)
        while tmp_lines:
            context_toks_init += process_data(tmp_lines)
            tmp_lines = context_lines.readlines(BUF_SIZE)  
            print(len(context_toks_init))
        
        tmp_lines = rem_lines.readlines(BUF_SIZE)
        while tmp_lines:
            rem_toks_init += process_data(tmp_lines)
            tmp_lines = rem_lines.readlines(BUF_SIZE)
            print(len(rem_toks_init))
        
        print("Loading Done")
        rem_toks = []
        add_toks = []      
        meta_lines = []  

        print(rem_toks_init[5])
        print(add_toks_init[5])
        print("CONTEXT")
        print(context_toks_init[5])
        
        print(rem_toks_init[-2])
        print(add_toks_init[-2])
        print("CONTEXT")
        print(context_toks_init[-2])
        for i in range(0, len(rem_toks_init)):
            if len(rem_toks_init[i]) < 10000 and len(add_toks_init[i]) < 10000:
                add_toks.append(add_toks_init[i])
                meta_lines.append(meta_lines_init[i])
                if context_toks_init[i] and rem_toks_init[i] and len(context_toks_init[i]) < 10000 :
                    rem_toks.append(rem_toks_init[i] + ['<CTX>'] +  context_toks_init[i])
                elif rem_toks_init[i]:
                    rem_toks.append(rem_toks_init[i])
                elif context_toks_init[i] and len(context_toks_init[i]) < 10000:
                    rem_toks.append(['<CTX>'] + context_toks_init[i])
                else:
                    rem_toks.append([" "])

        print(len(rem_toks))
        print(len(add_toks))
        print(len(meta_lines))
        [train_data, train_labels, train_meta], \
        [valid_data, valid_labels, valid_meta], \
        [test_data, test_labels, test_meta] = get_training_testing(rem_toks, add_toks,
                                                                   meta_lines,
                                                                   valid_size=valid_size,
                                                                   test_size=test_size,
                                                                   shuffle=True)
        print(len(train_data))
        print(len(valid_data))
        print(len(test_data))
        save_data(train_data, train_labels, train_meta, saved_data_path, '_train')
        save_data(valid_data, valid_labels, valid_meta, saved_data_path, '_valid')
        save_data(test_data, test_labels, test_meta, saved_data_path, '_test')


main_context()


