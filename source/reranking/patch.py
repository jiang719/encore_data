class Patch(object):
    source = ""
    target = ""
    patch = ""
    defects4j_id = ""
    row_num = 0
    score = 0.0
    meta = ""

    def __init__(self, source, target, patch, defects4j_id, row_num, score, meta):
        self.source = source
        self.target = target
        self.patch = patch
        self.defects4j_id = defects4j_id
        self.row_num = row_num
        self.score = score
        self.meta = meta
