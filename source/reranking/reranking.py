from patch import Patch
import os
from collections import defaultdict

BUG_LIST = ['Closure 62',
'Closure 65',
'Math 80',
'Closure 67',
'Math 75',
'Lang 59',
'Math 41',
'Closure 51',
'Math 2',
'Closure 10',
'Chart 11',
'Time 16',
'Chart 1',
'Closure 73',
'Closure 38',
'Chart 10',
'Lang 57',
'Math 85',
'Math 58',
'Chart 24',
'Math 105',
'Mockito 26',
'Math 30',
'Math 59',
'Chart 12',
'Closure 71',
'Math 63',
'Closure 130',
'Math 32',
'Chart 13',
'Math 69',
'Closure 14',
'Mockito 34',
'Closure 63',
'Math 5',
'Closure 57',
'Math 33',
'Lang 16',
'Math 34',
'Closure 52',
'Math 82',
'Closure 92',
'Time 4',
'Mockito 29',
'Closure 18',
'Closure 123',
'Closure 93',
'Closure 86',
'Closure 104',
'Closure 114',
'Math 96',
'Math 11',
'Closure 125',
'Lang 61',
'Lang 6',
'Math 70',
'Math 57',
'Math 104',
'Chart 9',
'Time 19',
'Lang 33',
'Lang 24',
'Lang 26',
'Chart 8',
'Math 94',
'Math 27',
'Mockito 24',
'Closure 31',
'Chart 20',
'Closure 70',
'Lang 21',
'Lang 29',
'Mockito 38',
'Closure 113']


def read_beam(beam_file, meta_data):
    lines = open(beam_file).read().split('\n')
    dataset = []

    for line in lines:
        if line.startswith('S'): # start of new bug
            source  = line.split('\t')[1]
            row_num = int(line.split('\t')[0].split('-')[1])
            defects4j_id = " ".join(meta_data[row_num])
            meta = "\t".join(meta_data[row_num])
        elif line.startswith('T'):
            target = line.split('\t')[1]
        elif line.startswith('H'):
            hypothesis = line.split('\t')[2]
            score = float(line.split('\t')[1])
            dataset.append(Patch(source, target, hypothesis, defects4j_id, row_num, score, meta))

    return dataset


def read_beam_ochoai(beam_file, bug_id, meta_data):
    lines = open(beam_file).read().split('\n')
    dataset = []

    for line in lines:
        if line.startswith('S'): # start of new bug
            source  = line.split('\t')[1]
            row_num = int(line.split('\t')[0].split('-')[1])
            defects4j_id = bug_id
            meta = "\t".join(meta_data[row_num])
        elif line.startswith('T'):
            target = line.split('\t')[1]
        elif line.startswith('H'):
            hypothesis = line.split('\t')[2]
            score = float(line.split('\t')[1])
            dataset.append(Patch(source, target, hypothesis, defects4j_id, row_num, score, meta))
    return dataset


def get_defects4j_meta(meta):
    fin = open(meta, 'r')
    data = []
    for l in fin.readlines():
        data.append(l.split('\t'))
    return data

def ochoai():
    # open the 5 output.

    for bug in BUG_LIST:
        print(bug)
        f_dir = bug.split(' ')
        final_dir = '_'.join(f_dir) + 'dir/'
        MAIN_DIR = "/home.local/icse_fairseq_data/ochoai_100loc/"
        FULL_DIR = MAIN_DIR + final_dir
        meta_data = get_defects4j_meta("/home.local/icse_fairseq_data/meta/" + final_dir + '_test_meta.txt')
        if os.path.isdir(MAIN_DIR):
            coconut1 = FULL_DIR + 'fconv_tuned_1/output.tok.nbest.txt'
            coconut2 = FULL_DIR + 'fconv_tuned_2/output.tok.nbest.txt'
            coconut3 = FULL_DIR + 'fconv_tuned_3/output.tok.nbest.txt'
            coconut4 = FULL_DIR + 'fconv_tuned_4/output.tok.nbest.txt'
            coconut5 = FULL_DIR + 'fconv_tuned_5/output.tok.nbest.txt'
            all_coconut = read_beam_ochoai(coconut1, bug,meta_data) + read_beam_ochoai(coconut2, bug,meta_data) + read_beam_ochoai(coconut3, bug,meta_data) + read_beam_ochoai(coconut4, bug,meta_data) + read_beam_ochoai(coconut5, bug,meta_data)

            print(len(all_coconut))

            all_coconut = sorted(all_coconut, key=lambda x: (x.meta, x.score), reverse=True)
            temp_patch = []
            fout = open(MAIN_DIR + final_dir[:-1] + 'ENCORE_ochoai_5.txt', 'w')
            for result in all_coconut:
                if [result.defects4j_id, result.patch] not in temp_patch:
                    temp_patch.append([result.defects4j_id, result.patch])
                    fout.write("START PATCH" + '\n')
                    fout.write(result.source + '\n')
                    fout.write(result.target + '\n')
                    fout.write(result.patch + '\n')
                    fout.write(result.defects4j_id + '\n')
                    fout.write(str(result.score) + '\n')
                    fout.write(result.meta + '\n')
            print(len(temp_patch))


def perfect_loc():


    # open the 5 output.
    MAIN_DIR = "fairseq-data/java/fconv/context_perfectloc_d4j_all/"

    meta_data = get_defects4j_meta('data/java/context/test/context/_test_meta.txt')

    coconut1 = MAIN_DIR + 'context_tuned_1/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'context_tuned_2/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'context_tuned_3/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'context_tuned_4/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'context_tuned_5/output.tok.nbest.txt'
    coconut6 = MAIN_DIR + 'context_tuned_6/output.tok.nbest.txt'
    coconut7 = MAIN_DIR + 'context_tuned_7/output.tok.nbest.txt'
    coconut8 = MAIN_DIR + 'context_tuned_8/output.tok.nbest.txt'
    coconut9 = MAIN_DIR + 'context_tuned_9/output.tok.nbest.txt'
    coconut10 = MAIN_DIR + 'context_tuned_10/output.tok.nbest.txt'

    all_coconut = read_beam(coconut1, meta_data) + read_beam(coconut2, meta_data) + read_beam(coconut3, meta_data) + read_beam(coconut4, meta_data) + read_beam(coconut5, meta_data) + \
                   read_beam(coconut6, meta_data) + read_beam(coconut7, meta_data) + read_beam(coconut8, meta_data) + read_beam(coconut9, meta_data) + read_beam(coconut10, meta_data)

    print(len(all_coconut))

    all_coconut = sorted(all_coconut, key=lambda x: (x.defects4j_id, x.score), reverse=True)
    temp_patch = []
    current_patch = ""
    for result in all_coconut:
        if current_patch != result.defects4j_id:
            current_patch = result.defects4j_id
            temp_patch = []
            fout = open(MAIN_DIR + '/secondtry/' + result.defects4j_id.replace(" ",'').replace('\n','').replace('/','.') + '_ENCORE_PERFECT_LOC.txt', 'w')
        if result.patch not in temp_patch:
            temp_patch.append(result.patch)
            fout.write("START PATCH" + '\n')
            fout.write(" " + '\n')
            fout.write(" " + '\n')
            fout.write(result.patch + '\n')
            fout.write(result.defects4j_id + '\n')
            fout.write(str(result.score) + '\n')
            fout.write(result.meta + '\n')
    print(len(temp_patch))



def indiv_results():
    # open the 10 output.
    MAIN_DIR = "/home/tlutelli/issta_data/fairseq-data/java/nocontext/results/"

    meta_data = get_defects4j_meta('data/java/context/test/no_context/_test_meta.txt')
    coconut1 = MAIN_DIR + 'fconv_tuned_1/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'fconv_tuned_2/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'fconv_tuned_3/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'fconv_tuned_4/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'fconv_tuned_5/output.tok.nbest.txt'
    coconut6 = MAIN_DIR + 'fconv_tuned_6/output.tok.nbest.txt'
    coconut7 = MAIN_DIR + 'fconv_tuned_7/output.tok.nbest.txt'
    coconut8 = MAIN_DIR + 'fconv_tuned_8/output.tok.nbest.txt'
    coconut9 = MAIN_DIR + 'fconv_tuned_9/output.tok.nbest.txt'
    coconut10 = MAIN_DIR + 'fconv_tuned_10/output.tok.nbest.txt'
    encore = [coconut1, coconut1, coconut3, coconut4, coconut5, coconut6, coconut7, coconut8, coconut9, coconut10]
    MAIN_DIR = "fairseq-data/java/fconv/context_perfectloc_d4j_all/"
    coconut1 = MAIN_DIR + 'context_tuned_1/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'context_tuned_2/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'context_tuned_3/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'context_tuned_4/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'context_tuned_5/output.tok.nbest.txt'
    coconut6 = MAIN_DIR + 'context_tuned_6/output.tok.nbest.txt'
    coconut7 = MAIN_DIR + 'context_tuned_7/output.tok.nbest.txt'
    coconut8 = MAIN_DIR + 'context_tuned_8/output.tok.nbest.txt'
    coconut9 = MAIN_DIR + 'context_tuned_9/output.tok.nbest.txt'
    coconut10 = MAIN_DIR + 'context_tuned_10/output.tok.nbest.txt'
    coconuts = [coconut1, coconut1, coconut3, coconut4, coconut5, coconut6, coconut7, coconut8, coconut9, coconut10]
    meta_coco = get_defects4j_meta('data/java/context/test/context/_test_meta.txt')
    for i in range(0,10):
        print("Working on coconut:", str(i))
        all_coconut = []
        current_patch = ""
        all_patches = defaultdict(list)
        for j in range(0,i+1):
            print(j)
            all_coconut +=read_beam(encore[j], meta_data)

        for result in all_coconut:
            if current_patch != result.defects4j_id:
                current_patch = result.defects4j_id
                count = 0
            if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
                # temp_patch[current_patch].append(result.patch)
                all_patches[current_patch].append([result.patch, result.score, result.meta, count, "no_context"])
        all_coconut = []
        for j in range(0, i+1):
            all_coconut +=read_beam(coconuts[j], meta_coco)
        for result in all_coconut:
            if current_patch != result.defects4j_id:
                current_patch = result.defects4j_id
                count = 0
            if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
                # temp_patch[current_patch].append(result.patch)
                all_patches[current_patch].append([result.patch, result.score, result.meta, count, "context"])
        if not os.path.exists(MAIN_DIR + '../coconut_' + str(i)):
            os.mkdir('../coconut_' + str(i))
        for key in all_patches:
            temp_patch = []
            fout = open(MAIN_DIR + '../coconut_' + str(i) + '/' + key.replace(" ","").replace('\n','').replace('/','.') + '_ENCORE_PERFECT_LOC.txt', 'w')
            sorted_list =  sorted(all_patches[key], key=lambda x: x[1], reverse=True)
            #print(key +" " +  str(len(sorted_list)))
            for elem in sorted_list:
                if elem[0] not in temp_patch:
                    fout.write("START PATCH" + '\n')
                    fout.write("\n")
                    fout.write("\n")
                    fout.write(elem[0] + "\n")
                    fout.write(key + "\n")
                    fout.write(str(elem[1]) + " " + str(elem[3]) + " " + elem[4] + '\n')
                    temp_patch.append(elem[0])

def perfect_loc_merged():
    # open the 5 output.
    MAIN_DIR = "fairseq-data/java/fconv/perfectloc_d4j_all/"

    meta_data = get_defects4j_meta('data/java/context/test/no_context/_test_meta.txt')

    coconut1 = MAIN_DIR + 'fconv_tuned_1/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'fconv_tuned_2/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'fconv_tuned_3/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'fconv_tuned_4/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'fconv_tuned_5/output.tok.nbest.txt'
    coconut6 = MAIN_DIR + 'fconv_tuned_6/output.tok.nbest.txt'
    coconut7 = MAIN_DIR + 'fconv_tuned_7/output.tok.nbest.txt'
    coconut8 = MAIN_DIR + 'fconv_tuned_8/output.tok.nbest.txt'
    coconut9 = MAIN_DIR + 'fconv_tuned_9/output.tok.nbest.txt'
    coconut10 = MAIN_DIR + 'fconv_tuned_10/output.tok.nbest.txt'

    all_coconut = read_beam(coconut1, meta_data) + read_beam(coconut2, meta_data) + read_beam(coconut3, meta_data) + read_beam(coconut4, meta_data) + read_beam(coconut5, meta_data) + \
                   read_beam(coconut6, meta_data) + read_beam(coconut7, meta_data) + read_beam(coconut8, meta_data) + read_beam(coconut9, meta_data) + read_beam(coconut10, meta_data)

    print(len(all_coconut))

    #all_coconut = sorted(all_coconut, key=lambda x: (x.defects4j_id, x.score), reverse=True)
    #temp_patch = defaultdict(list)
    current_patch = ""
    all_patches = defaultdict(list)
    for result in all_coconut:
        if current_patch != result.defects4j_id:
            current_patch = result.defects4j_id
            count = 0
        if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
            #temp_patch[current_patch].append(result.patch)
            all_patches[current_patch].append([result.patch, result.score,  result.meta,count, "no_context"])

    # open the 5 output.
    MAIN_DIR = "fairseq-data/java/fconv/context_perfectloc_d4j_all/"

    meta_data = get_defects4j_meta('data/java/context/test/context/_test_meta.txt')

    coconut1 = MAIN_DIR + 'context_tuned_1/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'context_tuned_2/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'context_tuned_3/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'context_tuned_4/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'context_tuned_5/output.tok.nbest.txt'
    coconut6 = MAIN_DIR + 'context_tuned_6/output.tok.nbest.txt'
    coconut7 = MAIN_DIR + 'context_tuned_7/output.tok.nbest.txt'
    coconut8 = MAIN_DIR + 'context_tuned_8/output.tok.nbest.txt'
    coconut9 = MAIN_DIR + 'context_tuned_9/output.tok.nbest.txt'
    coconut10 = MAIN_DIR + 'context_tuned_10/output.tok.nbest.txt'

    all_coconut = read_beam(coconut1, meta_data) + read_beam(coconut2, meta_data) + read_beam(coconut3, meta_data) + read_beam(coconut4, meta_data) + read_beam(coconut5, meta_data) + \
                   read_beam(coconut6, meta_data) + read_beam(coconut7, meta_data) + read_beam(coconut8, meta_data) + read_beam(coconut9, meta_data) + read_beam(coconut10, meta_data)

    print(len(all_coconut))

    #all_coconut = sorted(all_coconut, key=lambda x: (x.defects4j_id, x.score), reverse=True)
    current_patch = ""
    temp_patch = defaultdict(list)
    for result in all_coconut:
        if current_patch != result.defects4j_id:
            current_patch = result.defects4j_id
            count = 0

        if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
            all_patches[current_patch].append([result.patch, result.score, result.meta, count, 'context'])

    for key in all_patches:
        temp_patch = []
        fout = open(MAIN_DIR + '../merged/' + key.replace(" ","").replace('\n','').replace('/','.') + '_ENCORE_PERFECT_LOC.txt', 'w')
        sorted_list =  sorted(all_patches[key], key=lambda x: x[1], reverse=True)
        print(key +" " +  str(len(sorted_list)))
        for elem in sorted_list:
            if elem[0] not in temp_patch:
                fout.write("START PATCH" + '\n')
                fout.write("\n")
                fout.write("\n")
                fout.write(elem[0] + "\n")
                fout.write(key + "\n")
                fout.write(str(elem[1]) + " " + str(elem[3]) + " " + elem[4] + '\n')
                temp_patch.append(elem[0])


def quixjava_merged():
    # open the 5 output.

    MAIN_DIR = "/home/n44jiang/python-workspace/DeepRepair/QuixBugs/result/coconut/2006_nocontext_identifier/"

    meta_data = get_defects4j_meta('/home/n44jiang/python-workspace/DeepRepair/QuixBugs/result/coconut/2006_nocontext_identifier/_test_meta.txt')

    coconut1 = MAIN_DIR + 'result/1/checkpoint_best.pt/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'result/2/checkpoint_best.pt/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'result/3/checkpoint_best.pt/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'result/4/checkpoint_best.pt/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'result/5/checkpoint_best.pt/output.tok.nbest.txt'
    coconut6 = MAIN_DIR + 'result/6/checkpoint_best.pt/output.tok.nbest.txt'
    coconut7 = MAIN_DIR + 'result/7/checkpoint_best.pt/output.tok.nbest.txt'
    coconut8 = MAIN_DIR + 'result/8/checkpoint_best.pt/output.tok.nbest.txt'
    coconut9 = MAIN_DIR + 'result/9/checkpoint_best.pt/output.tok.nbest.txt'
    coconut10 = MAIN_DIR + 'result/10/checkpoint_best.pt/output.tok.nbest.txt'

    all_coconut = read_beam(coconut1, meta_data) + read_beam(coconut2, meta_data) + read_beam(coconut3, meta_data) + read_beam(coconut4, meta_data) + read_beam(coconut5, meta_data) + \
                   read_beam(coconut6, meta_data) + read_beam(coconut7, meta_data) + read_beam(coconut8, meta_data) + read_beam(coconut9, meta_data) + read_beam(coconut10, meta_data)

    print(len(all_coconut))

    #all_coconut = sorted(all_coconut, key=lambda x: (x.defects4j_id, x.score), reverse=True)
    #temp_patch = defaultdict(list)
    current_patch = ""
    all_patches = defaultdict(list)
    for result in all_coconut:
        if current_patch != result.defects4j_id:
            current_patch = result.defects4j_id
            count = 0
        if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
            #temp_patch[current_patch].append(result.patch)
            all_patches[current_patch].append([result.patch, result.score,  result.meta,count, "no_context"])


    # open the 5 output.
    MAIN_DIR = "/home/n44jiang/python-workspace/DeepRepair/QuixBugs/result/coconut/2006_context_identifier/"

    meta_data = get_defects4j_meta('/home/n44jiang/python-workspace/DeepRepair/QuixBugs/result/coconut/2006_context_identifier/_test_meta.txt')

    coconut1 = MAIN_DIR + 'result/1/checkpoint_best.pt/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'result/2/checkpoint_best.pt/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'result/3/checkpoint_best.pt/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'result/4/checkpoint_best.pt/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'result/5/checkpoint_best.pt/output.tok.nbest.txt'
    coconut6 = MAIN_DIR + 'result/6/checkpoint_best.pt/output.tok.nbest.txt'
    coconut7 = MAIN_DIR + 'result/7/checkpoint_best.pt/output.tok.nbest.txt'
    coconut8 = MAIN_DIR + 'result/8/checkpoint_best.pt/output.tok.nbest.txt'
    coconut9 = MAIN_DIR + 'result/9/checkpoint_best.pt/output.tok.nbest.txt'
    coconut10 = MAIN_DIR + 'result/10/checkpoint_best.pt/output.tok.nbest.txt'

    all_coconut = read_beam(coconut1, meta_data) + read_beam(coconut2, meta_data) + read_beam(coconut3, meta_data) + read_beam(coconut4, meta_data) + read_beam(coconut5, meta_data) + \
                   read_beam(coconut6, meta_data) + read_beam(coconut7, meta_data) + read_beam(coconut8, meta_data) + read_beam(coconut9, meta_data) + read_beam(coconut10, meta_data)

    print(len(all_coconut))

    #all_coconut = sorted(all_coconut, key=lambda x: (x.defects4j_id, x.score), reverse=True)
    current_patch = ""
    temp_patch = defaultdict(list)
    for result in all_coconut:
        if current_patch != result.defects4j_id:
            current_patch = result.defects4j_id
            count = 0

        if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
            all_patches[current_patch].append([result.patch, result.score, result.meta, count, 'context'])


    for key in all_patches:
        temp_patch = []
        fout = open(MAIN_DIR + '../../../merged_quixjava/coconut/2006_context_nocontext_identifier/' +
                    key.replace(" ","").replace('\n','').replace('/','.') + '_ENCORE_PERFECT_LOC.txt', 'w')
        sorted_list =  sorted(all_patches[key], key=lambda x: x[1], reverse=True)
        print(key +" " +  str(len(sorted_list)))
        for elem in sorted_list:
            if elem[0] not in temp_patch:
                fout.write("START PATCH" + '\n')
                fout.write("\n")
                fout.write("\n")
                fout.write(elem[0] + "\n")
                fout.write(key + "\n")
                fout.write(str(elem[1]) + " " + str(elem[3]) + " " + elem[4] + '\n')
                temp_patch.append(elem[0])


def codeflaws():
    # open the 5 output.
    MAIN_DIR = "fairseq-data/cpp/codeflaw_all/results/"

    meta_data = get_defects4j_meta('data/cpp/test/codeflaws/all/_test_meta.txt')

    coconut1 = MAIN_DIR + 'fconv_tuned_1/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'fconv_tuned_2/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'fconv_tuned_3/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'fconv_tuned_4/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'fconv_tuned_5/output.tok.nbest.txt'


    all_coconut = read_beam(coconut1, meta_data) + read_beam(coconut2, meta_data) + read_beam(coconut3, meta_data) + read_beam(coconut4, meta_data) + read_beam(coconut5, meta_data)

    print(len(all_coconut))

    all_coconut = sorted(all_coconut, key=lambda x: (x.defects4j_id, x.score), reverse=True)
    temp_patch = []
    current_patch = ""
    for result in all_coconut:
        if current_patch != result.defects4j_id:
            current_patch = result.defects4j_id
            temp_patch = []
            fout = open(MAIN_DIR + result.defects4j_id.replace(" ",'').replace('\n','').replace('/','.') + '_ENCORE_PERFECT_LOC.txt', 'w')
        if result.patch not in temp_patch:
            temp_patch.append(result.patch)
            fout.write("START PATCH" + '\n')
            fout.write(result.source + '\n')
            fout.write(result.target + '\n')
            fout.write(result.patch + '\n')
            fout.write(result.defects4j_id + '\n')
            fout.write(str(result.score) + '\n')
            fout.write(result.meta + '\n')
    print(len(temp_patch))


def genprog():
    # open the 5 output.
    MAIN_DIR = "fairseq-data/cpp/codeflaw_all/results/"

    meta_data = get_defects4j_meta('data/cpp/test/codeflaws/all/_test_meta.txt')

    coconut1 = MAIN_DIR + 'fconv_tuned_1/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'fconv_tuned_2/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'fconv_tuned_3/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'fconv_tuned_4/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'fconv_tuned_5/output.tok.nbest.txt'


    all_coconut = read_beam(coconut1, meta_data) + read_beam(coconut2, meta_data) + read_beam(coconut3, meta_data) + read_beam(coconut4, meta_data) + read_beam(coconut5, meta_data)

    print(len(all_coconut))

    all_coconut = sorted(all_coconut, key=lambda x: (x.defects4j_id, x.score), reverse=True)
    temp_patch = []
    current_patch = ""
    for result in all_coconut:
        if current_patch != result.defects4j_id:
            current_patch = result.defects4j_id
            temp_patch = []
            fout = open(MAIN_DIR + result.defects4j_id.replace(" ",'').replace('\n','').replace('/','.') + '_ENCORE_PERFECT_LOC.txt', 'w')
        if result.patch not in temp_patch:
            temp_patch.append(result.patch)
            fout.write("START PATCH" + '\n')
            fout.write(result.source + '\n')
            fout.write(result.target + '\n')
            fout.write(result.patch + '\n')
            fout.write(result.defects4j_id + '\n')
            fout.write(str(result.score) + '\n')
            fout.write(result.meta + '\n')
    print(len(temp_patch))


def python_quixbugs():
    # open the 5 output.
    MAIN_DIR = "fairseq-data/python/fconv/results/"

    meta_data = get_defects4j_meta('data/python/test/_test_meta.txt')

    coconut1 = MAIN_DIR + 'fconv_tuned_1/output.tok.nbest.txt'
    coconut2 = MAIN_DIR + 'fconv_tuned_2/output.tok.nbest.txt'
    coconut3 = MAIN_DIR + 'fconv_tuned_3/output.tok.nbest.txt'
    coconut4 = MAIN_DIR + 'fconv_tuned_4/output.tok.nbest.txt'
    coconut5 = MAIN_DIR + 'fconv_tuned_5/output.tok.nbest.txt'


    all_coconut = read_beam(coconut1, meta_data) + read_beam(coconut2, meta_data) + read_beam(coconut3, meta_data) + read_beam(coconut4, meta_data) + read_beam(coconut5, meta_data)

    print(len(all_coconut))

    all_coconut = sorted(all_coconut, key=lambda x: (x.defects4j_id, x.score), reverse=True)
    current_patch = ""
    for result in all_coconut:
        if current_patch != result.defects4j_id:
            current_patch = result.defects4j_id
            temp_patch = []
            fout = open(MAIN_DIR + result.defects4j_id.replace(" ",'').replace('\n','').replace('/','.') + '_ENCORE_PERFECT_LOC.txt', 'w')
        if result.patch not in temp_patch:
            temp_patch.append(result.patch)
            fout.write("START PATCH" + '\n')
            fout.write(result.source + '\n')
            fout.write(result.target + '\n')
            fout.write(result.patch + '\n')
            fout.write(result.defects4j_id + '\n')
            fout.write(str(result.score) + '\n')
            fout.write(result.meta + '\n')
    print(len(temp_patch))

#indiv_results()
quixjava_merged()
#perfect_loc_merged()
#codeflaws()
#ochoai()
