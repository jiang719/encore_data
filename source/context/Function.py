

class Function(object):
    commit_id = ""
    file_name = ""
    buggy_function = b""
    clean_function = b""

    def __init__(self, commit_id, file_name, buggy=None, clean=None):
        self.commit_id = commit_id
        self.file_name = file_name
        if clean:
            self.clean_function = buggy
            self.buggy_function = None
        if buggy:
            self.buggy_function = buggy
            self.clean_function = None
