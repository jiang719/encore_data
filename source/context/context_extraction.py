import pickle
from source.context.Function import Function
import sys
import os
import source.data_extraction.git_wrapper as git_wrapper
import source.tokenization.tokenization as tokenization


def load_function_pickle(pickle_path):
    with open(pickle_path, 'rb') as handle:
        function_dict = pickle.load(handle)
    return function_dict


def load_meta(meta_path):
    defects = []
    fin = open(meta_path, 'r')
    for line in fin.readlines():
        commit, file, project = line.split(',')
        defects.append([commit, file[3:]])
    return defects


def main(j):
    count = 0
    defects = load_meta('data/java/train/meta_lines.txt')
    meta = open("data/java/train/meta_lines.txt", 'r').readlines()
    rem = open('data/java/train/removed_lines.txt', 'r')
    rem_lines = rem.readlines()
    add = open('data/java/train/added_lines.txt', 'r')
    add_lines = add.readlines()

    foutadd = open('data/java/context/' + str(j) + 'added_lines.txt', 'w')
    foutrem = open('data/java/context/' + str(j) + 'removed_lines.txt', 'w')
    foutmeta = open('data/java/context/' + str(j) + 'meta_lines.txt', 'w')
    foutcontext = open('data/java/context/' + str(j) + 'context_lines.txt', 'w')
    print("Start loading ", j)
    function_dict = load_function_pickle('/local/tlutelli/DefectPrediction/pickle_func/functions' + str(j) + '.pkl')
    print(len(function_dict))
    for func in function_dict.items():
        count += 1
        if count % 1000 == 0:
            print("Process " + str(j) + " Done: " +  str(count * 1.0 / len(function_dict)))

        indices = [i for i, x in enumerate(defects) if x == [func[1].commit_id, func[1].file_name.split('/buggy/', 1)[-1]]]
        if indices:
            buggy = []
            if func[1].buggy_function:
                buggy = func[1].buggy_function.strip().splitlines()
                buggy_tok = []
                for b in buggy:
                    buggy_tok.append(tokenization.tokenize(b))
            for i in indices:
                tok_add = tokenization.tokenize(add_lines[i])
                tok_rem = tokenization.tokenize(rem_lines[i])
                if buggy and tok_rem in buggy_tok:
                    foutadd.write(add_lines[i])
                    foutrem.write(rem_lines[i])
                    foutmeta.write(meta[i])
                    function_tokenized = tokenization.tokenize(" ".join(func[1].buggy_function.strip().splitlines()))
                    foutcontext.write(" ".join(function_tokenized) + '\n')
                defects.pop(i)


main(sys.argv[1])
