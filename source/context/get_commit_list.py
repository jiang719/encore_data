import os

def get_commit_list():
    meta_file = os.getcwd() + '/data/java/java_1k_commits_meta_lines.txt'
    init_dir = '/local/tlutelli/DefectPrediction'
    project_repos = init_dir + '/data/repos/java/'
    projects = os.listdir(project_repos)  # Load the list of projects
    progress_count = 0.
    for project in projects:
        project_path = project_repos + project
        if os.path.isdir(project_path):  # check if the repo exist
            os.chdir(project_path)
            print(project_path)
            git_wrapper.pull_project(project_path)
            commits = git_wrapper.get_list_commit(project_path)
            os.chdir(init_dir)
            progress_count += 1.
            print("Project: " + project)
            print(progress_count / len(projects))
            for commit in commits:
                os.chdir(project_path)
                if commit_checker.is_fix(commit, project, project_repos):

                    output = git_wrapper.get_files_changed(commit)
                    os.chdir(init_dir)
                    filenames = str(output, 'utf-8').split('\n')
                    for filename in filenames:
                        if commit_checker.is_java(filename):
                            with open(meta_file, 'a') as fin:
                                fin.write(commit + ',' + filename + ',' + project + '\n')

        else:
            print(project_path)


main()
