from collections import defaultdict
from statistics import mean

from source.reranking.patch import Patch


def get_defects4j_meta(meta):
    fin = open(meta, 'r')
    data = []
    for l in fin.readlines():
        data.append(l.split('\t'))
    return data

def read_beam(beam_file, meta_data):
    lines = open(beam_file).read().split('\n')
    dataset = []

    for line in lines:
        if line.startswith('S'): # start of new bug
            source  = line.split('\t')[1]
            row_num = int(line.split('\t')[0].split('-')[1])
            defects4j_id = " ".join(meta_data[row_num])
            meta = "\t".join(meta_data[row_num])
        elif line.startswith('T'):
            target = line.split('\t')[1]
        elif line.startswith('H'):
            hypothesis = line.split('\t')[2]
            score = float(line.split('\t')[1])
            dataset.append(Patch(source, target, hypothesis, defects4j_id, row_num, score, meta))

    return dataset



def indiv_results():
    # open the 5 output.
    total_statements = 670
    MAIN_DIR_NOCONTEXT = '/home.local/icse_fairseq_data/nocontext/'
    meta_data = get_defects4j_meta('/home.local/icse_fairseq_data/nocontext/_test_meta.txt')
    coconut1 = MAIN_DIR_NOCONTEXT + 'fconv_tuned_1/output.tok.nbest.txt'
    c_time = [67490.4 / 670, 74910.9 / 670, 55146.3 / 670, 69285.4 / 670, 55043.6 / 670, 75522.9 / 670, 64544.8 / 670, 23753.0 / 670, 64065.5 / 670, 32924.9 / 670]
    print(c_time[0])
    coconut2 = MAIN_DIR_NOCONTEXT + 'fconv_tuned_2/output.tok.nbest.txt'
    coconut3 = MAIN_DIR_NOCONTEXT + 'fconv_tuned_3/output.tok.nbest.txt'
    coconut4 = MAIN_DIR_NOCONTEXT + 'fconv_tuned_4/output.tok.nbest.txt'
    coconut5 = MAIN_DIR_NOCONTEXT + 'fconv_tuned_5/output.tok.nbest.txt'
    coconut6 = MAIN_DIR_NOCONTEXT + 'fconv_tuned_6/output.tok.nbest.txt'
    coconut7 = MAIN_DIR_NOCONTEXT + 'fconv_tuned_7/output.tok.nbest.txt'
    coconut8 = MAIN_DIR_NOCONTEXT + 'fconv_tuned_8/output.tok.nbest.txt'
    coconut9 = MAIN_DIR_NOCONTEXT + 'fconv_tuned_9/output.tok.nbest.txt'
    coconut10 = MAIN_DIR_NOCONTEXT + 'fconv_tuned_10/output.tok.nbest.txt'
    encore = [coconut1, coconut1, coconut3, coconut4, coconut5, coconut6, coconut7, coconut8, coconut9, coconut10]
    MAIN_DIR_CONTEXT =  '/home.local/icse_fairseq_data/context/'

    coconut1 = MAIN_DIR_CONTEXT + 'context_tuned_1/output.tok.nbest.txt'
    e1_time =  9375.5 / 709
    coconut2 = MAIN_DIR_CONTEXT + 'context_tuned_2/output.tok.nbest.txt'
    e2_time = 8555.2 / 709
    coconut3 = MAIN_DIR_CONTEXT + 'context_tuned_3/output.tok.nbest.txt'
    e3_time = 9382.4 / 709
    coconut4 = MAIN_DIR_CONTEXT + 'context_tuned_4/output.tok.nbest.txt'
    e4_time = 10151.9 / 709
    coconut5 = MAIN_DIR_CONTEXT + 'context_tuned_5/output.tok.nbest.txt'
    e5_time = 9260.7 / 709
    coconut6 = MAIN_DIR_CONTEXT + 'context_tuned_6/output.tok.nbest.txt'
    e6_time = 10284.4 / 709
    coconut7 = MAIN_DIR_CONTEXT + 'context_tuned_7/output.tok.nbest.txt'
    e7_time = 9532.2 / 709
    coconut8 = MAIN_DIR_CONTEXT + 'context_tuned_8/output.tok.nbest.txt'
    e8_time = 9069.6 / 709
    coconut9 = MAIN_DIR_CONTEXT + 'context_tuned_9/output.tok.nbest.txt'
    e9_time = 9576.9 / 709
    coconut10 = MAIN_DIR_CONTEXT + 'context_tuned_10/output.tok.nbest.txt'
    e10_time = 10946.4 / 709
    e_time = [e1_time, e2_time, e3_time, e4_time, e5_time, e6_time, e7_time, e8_time, e9_time, e10_time]
    print(e_time[0])
    exit()
    coconuts = [coconut1, coconut1, coconut3, coconut4, coconut5, coconut6, coconut7, coconut8, coconut9, coconut10]
    print(encore)
    meta_coco = get_defects4j_meta('/home.local/icse_fairseq_data/context/_test_meta.txt')
    for i in range(0,10):
        print("Working on coconut:", str(i +1))
        all_coconut = []
        all_encore = []
        all_patches = defaultdict(list)
        one_coco_patch = defaultdict(list)
        only_encore_patch = defaultdict(list)
        current_patch = ""
        for j in range(0, i+1):
            all_coconut += read_beam(coconuts[j], meta_data)
            all_encore += read_beam(encore[j], meta_data)

        for result in all_coconut:
            if current_patch != result.defects4j_id:
                current_patch = result.defects4j_id

            if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
                # temp_patch[current_patch].append(result.patch)
                if result.patch == result.target:
                    one_coco_patch[current_patch].append([1, result.score, result.meta, "context"])
                    all_patches[current_patch].append([1, result.score, result.meta, "context"])
                else:
                    one_coco_patch[current_patch].append([0, result.score, result.meta, "context"])
                    all_patches[current_patch].append([0, result.score, result.meta, "context"])

        for result in all_encore:
            if current_patch != result.defects4j_id:
                current_patch = result.defects4j_id

            if '$STRING$ $STRING$' not in result.patch and 'System . exit' not in result.patch:
                if result.patch == result.target:
                    only_encore_patch[current_patch].append([1, result.score, result.meta, "nocontext"])
                    all_patches[current_patch].append([1, result.score, result.meta, "nocontext"])
                else:
                    only_encore_patch[current_patch].append([0, result.score, result.meta, "nocontext"])
                    all_patches[current_patch].append([0, result.score, result.meta, "nocontext"])

        print("Average Rank for ENCORE ONLY: ", str(i +1))
        all_rank = []
        if i == 0:
            full_time = e_time[0] / ((i + 1) * 1000)
        else:
            full_time = sum(e_time[0:i]) / ((i+1) * 1000)
        for key in all_patches:
            sorted_list = sorted(only_encore_patch[key], key=lambda x: x[1], reverse=True)
            count = 0
            # Find time:

            for elem in sorted_list:
                count +=1
                if elem[0] == 1:
                    all_rank.append(count * full_time)
                    break
        print(mean(all_rank))

        print("Average Rank for COCO ONLY: ", str(i + 1))
        all_rank = []
        if i == 0:
            full_time = c_time[0] / ((i + 1) * 1000)
        else:
            full_time = sum(c_time[0:i]) / ((i+1) * 1000)
        for key in one_coco_patch:
            sorted_list = sorted(one_coco_patch[key], key=lambda x: x[1], reverse=True)
            count = 0
            for elem in sorted_list:
                count +=1
                if elem[0] == 1:
                    all_rank.append(count * full_time)
                    break
        print(mean(all_rank))
        print("Average Rank for COMBINED: ", str(i + 1))
        all_rank = []

        for key in all_patches:
            sorted_list = sorted(all_patches[key], key=lambda x: x[1], reverse=True)
            count = 0
            count_context = 0
            count_nocontext = 0
            for elem in sorted_list:
                count +=1
                if elem[3] == "context":
                    count_context += 1
                else:
                    count_nocontext += 1

                if elem[0] == 1:
                    if i == 0:
                        full_time = (count_nocontext * e_time[0] + count_context * c_time[0]) / ((i + 1) * 2000)
                    else:
                        full_time = (count_nocontext * sum(e_time[0:i]) + count_context * sum(c_time[0:i])) / ((i + 1) * 2000)
                    all_rank.append(full_time )
                    break
        print(mean(all_rank))

indiv_results()