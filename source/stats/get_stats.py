import source.data_extraction.git_wrapper as git_wrapper
import os
import numpy as np
'''
All functions to get stats

'''


def get_commits_number(language, init_dir):
    number_commit = []
    projects = os.listdir(init_dir + '/data/repos/' + language)  # Load the list of projects
    for project in projects:
        project_path = init_dir + '/data/repos/' + language + '/' + project
        if os.path.isdir(project_path):  # check if the repo exist
            os.chdir(project_path)
            commits = git_wrapper.get_list_commit(project_path)
            number_commit.append(len(commits))
    summary_commits(number_commit)


def summary_commits(num_commits):
    print("# of commits: ", np.sum(num_commits))
    print("Average # of commits per project: ", np.average(num_commits))
    print("Median # of commits per project: ", np.median(num_commits))
    print("Max # of commits per project: ", np.max(num_commits))
    print("Min # of commits per project: ", np.min(num_commits))


init_dir = os.getcwd()
print(init_dir)
print("Python:")
get_commits_number('python', init_dir)
print("Cpp:")
get_commits_number('cpp', init_dir)
print("Java:")
get_commits_number('java', init_dir)
print("JavaScript:")
get_commits_number('javascript', init_dir)