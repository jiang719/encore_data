#!/bin/bash
repos_counter=1
for i in 1 2 3 4 5 6 7 8 9 10
do
echo $i
 for giturl in `curl  -s "https://api.github.com/search/repositories?q=language:Python&page=$i&per_page=100" |grep git_url | cut -d ':' -f 2-3|tr -d '",'`;
   do
    cd ../../../data/repos/python
    git clone $giturl $repos_counter;
    echo $giturl
    ((repos_counter++))
  done
done

