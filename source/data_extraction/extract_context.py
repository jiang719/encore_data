from source.data_extraction.function import Function
import source.tokenization.tokenization as tokenization
import pickle
import sys
import difflib


class Defect(object):
    commit_id = ""
    file_name = ""
    buggy = b""
    clean = b""
    context = b""

    def __init__(self, commit_id, file_name, buggy, clean, context):
        self.commit_id = commit_id
        self.file_name = file_name
        self.buggy = buggy
        self.clean = clean
        self.context = context

def load_pickled_function(pickle_path):
    with open(pickle_path, 'rb') as handle:
        function_dict = pickle.load(handle)
    return function_dict


def analyze_function(func, key):
    defect_list = []
    bug = func.buggy_function.strip().splitlines()
    clean = func.clean_function.strip().splitlines()

    start_change = False
    buggy = ""
    for line in difflib.unified_diff(bug, clean, fromfile='file1', tofile='file2', lineterm='', n=0):
        #print(line)
        if line.startswith("@@"):
            if buggy:
                #print("RESULT: " + buggy)
                defect_list.append(Defect(func.commit_id, func.file_name, buggy, clean, func.buggy_function))
            buggy = ""
            clean = ""
            start_change = True
        if line.startswith('-') and not line.startswith('---'):
            buggy += line[1:].strip() + ' '
        if line.startswith('+') and not line.startswith('+++'):
            clean += line[1:].strip() + ' '
    #print("RESULT: " + buggy)
    defect_list.append(Defect(func.commit_id, func.file_name, buggy, clean, func.buggy_function))
    return defect_list



def main():
    fout = open('data/java/context/rem.txt', 'w')
    fout0 = open('data/java/context/meta.txt', 'w')
    fout1 = open('data/java/context/context.txt', 'w')
    fout2 = open('data/java/context/add.txt', 'w')
    for i in range(0, 99):
        print("Start_loading: ", i)
        count = 0
        dict_function = load_pickled_function('/local/tlutelli/pickled_func/functions' + str(i) + '.pkl')
        for key, function in dict_function.items():
            if function.buggy_function and function.clean_function and function.buggy_function != function.clean_function:
                defect_list = analyze_function(function, key)
                for defect in defect_list:
                    if isinstance(defect.buggy, str) and isinstance(defect.context, str) and isinstance(defect.clean, str):
                        fout.write(defect.buggy.replace('\n','') + '\n')
                        fout0.write(defect.commit_id + '\t' + defect.file_name + '\n')
                        fout1.write(defect.context.replace('\n','') + '\n')
                        fout2.write(defect.clean.replace('\n','') + '\n')
            #if count == 10:
            #    for defect in defect_list:
            #        print(defect.buggy)

            #   sys.exit()


main()
