import os
import subprocess
import git


def get_commit_message(commit, project, repos):
    """
    This function return the commit message of a specific commit (output of git log).
    :param commit:
    :param project:
    :param repos:
    :return: string: commit message
    """
    os.chdir(repos + project)
    process = subprocess.Popen(
        ["git", "log", commit, "--format=%B", "-n", "1"], stdout=subprocess.PIPE)
    message = process.communicate()[0]
    message = str(message).lower()
    return message


def get_list_commit(directory):
    """
    This function returns the list of commits in a specifc function
    :param directory:
    :return: list of commits
    """
    os.chdir(directory)
    process = subprocess.Popen(
        ["git", "log", "--no-merges", "--format=%H"], stdout=subprocess.PIPE)
    commits = process.communicate()[0]
    list_commits = commits.decode().split('\n')[:-1]
    return list_commits


def pull_project(project_path):
    """
    This function pull a specific project
    :param project_path:
    :return:
    """
    if os.path.isfile(".git/index.lock"):
        os.remove(".git/index.lock")
    try:
        g = git.Repo(project_path)
        g.git.reset('--hard', 'origin/master')
        return 0
    except Exception as e:
        print(str(e))
        print("Issue with project: " + project_path)
        return -1


def get_diff(commit):
    process = subprocess.Popen(["git", "diff", commit + "^", commit, "--unified=10"], stdout=subprocess.PIPE)
    output = process.communicate()[0]
    return output


def checkout_file(commit, file):
    process = subprocess.Popen(['git', 'checkout', commit + '^', file])
    output = process.communicate()[0]
    return output


def checkout_buggy_file(commit, file):
    """
    This function checkout the buggy file (i.e., the file before the commit)
    :param commit:
    :param file:
    :return:
    """
    process = subprocess.Popen(['git', 'checkout', commit + '^', file], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, err = process.communicate()
    return output, err


def checkout_clean_file(commit, file):
    """
    This function checkout the clean file (i.e., the file after the commit)
    :param commit:
    :param file:
    :return:
    """
    process = subprocess.Popen(['git', 'checkout', commit, file], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, err = process.communicate()
    return output, err


def get_files_changed(commit):
    """
    Return the list of files that were changed by a specific commit
    :param commit:
    :return:
    """
    process = subprocess.Popen(["git", "diff-tree", "--no-commit-id", "--name-only", "-r", commit], stdout=subprocess.PIPE)
    output = process.communicate()[0]
    return output
