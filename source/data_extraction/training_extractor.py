import os
import source.data_extraction.git_wrapper as git_wrapper
import source.data_extraction.commit_checker as commit_checker
from shutil import copyfile


# Split commit file by file
def split_commit(patch_diff):
    try:
        file_chunks = patch_diff.decode().split('\n---')
    except:
        file_chunks = ""
    return file_chunks


def get_filename(file_chunk):
    # parse the filename
    if len(file_chunk.splitlines()) > 0:
        filename = file_chunk.splitlines()[0]
    else:
        filename = " "
    return filename


def appendlist(li):
    res = ""
    for val in li:
        if val.startswith('+') or val.startswith('-'):
            val = ' '.join(val.split()[1:])
        res = res + val.strip()
    return res


# get deleted and added lines and unchanged lines
def get_change_idx(list_code_chunk):
    idx_plus_lines = []
    idx_minus_lines = []
    for idx, l in enumerate(list_code_chunk):
        if l.startswith('+'):
            idx_plus_lines.append(idx)
        if l.startswith('-'):
            idx_minus_lines.append(idx)

    return idx_plus_lines, idx_minus_lines


def bracket_matching(code_chunk_list, plus_idx, minus_lines_idx, plus_lines_idx):
    complete_add_code_indices = []
    is_add_complete = False
    minus_idx = plus_idx - 1
    add_left_brackets = 0
    add_right_brackets = 0

    # generate added line
    # THE PLUS LINE PART
    # look forward for the end token
    for idx, l in enumerate(code_chunk_list):
        # start from plus_idx
        if idx < plus_idx:
            continue
        # consider only neutral line and plus line
        if idx in minus_lines_idx:
            continue

        if l.find(');') != -1:
            plus_end_idx = idx
            break
        else:
            plus_end_idx = idx

    # look backwards for the first occurrence of balance
    for idx, l in reversed(list(enumerate(code_chunk_list))):
        if idx > plus_end_idx or is_add_complete:
            continue
        if idx in minus_lines_idx:
            continue

        add_right_brackets = add_right_brackets + l.count(')')
        add_left_brackets = add_left_brackets + l.count('(')

        if not is_add_complete:
            complete_add_code_indices.insert(0, idx)
            if add_left_brackets == add_right_brackets:
                is_add_complete = True

    # the remove part
    complete_rem_code_indices = []
    is_rem_complete = False

    rem_left_brackets = 0
    rem_right_brackets = 0

    for idx, l in enumerate(code_chunk_list):
        if idx < minus_idx:
            continue
        if idx in plus_lines_idx:
            continue

        if l.find(');') != -1:
            # all of this is to get the end token
            minus_end_idx = idx
            break
        else:
            minus_end_idx = idx

    # look backwards for the first occurrence of balance
    for idx, l in reversed(list(enumerate(code_chunk_list))):
        if idx > minus_end_idx or is_rem_complete:
            continue
        if idx in plus_lines_idx:
            continue

        rem_right_brackets = rem_right_brackets + l.count(')')
        rem_left_brackets = rem_left_brackets + l.count('(')

        if not is_rem_complete:
            complete_rem_code_indices.insert(0, idx)
            if rem_left_brackets == rem_right_brackets and \
                    minus_idx in complete_rem_code_indices:
                is_rem_complete = True

    complete_add_code_list = []
    complete_rem_code_list = []

    for idx, l in enumerate(code_chunk_list):
        if idx in complete_add_code_indices:
            complete_add_code_list.append(
                code_chunk_list[idx])
        if idx in complete_rem_code_indices:
            complete_rem_code_list.append(
                code_chunk_list[idx])
    new_plus_line = appendlist(complete_add_code_list)
    new_minus_line = appendlist(complete_rem_code_list)

    return new_minus_line, new_plus_line,


def get_one_line_change(patch_diff, commit, del_fil, add_fil, met_fil, project, language_checker, project_path,
                        init_dir, lang):
    print(commit)
    file_chunks = split_commit(patch_diff)
    for file_chunk in file_chunks:
        filename = get_filename(file_chunk)
        if not language_checker(filename):
            continue
        code_chunks = file_chunk.split('@@')[1:]  # consider code_chunks
        for code_chunk in code_chunks:
            good_lines = 1  # assume we keep the line before the checks
            code_chunk_list = []

            for l in code_chunk.splitlines():
                code_chunk_list.append(l)

            # remove wrong chunks like "-24,5 +24,7"
            if len(code_chunk_list) < 2:
                continue
            plus_lines_idx, minus_lines_idx = get_change_idx(code_chunk_list)

            # only addition or only deletion
            if len(plus_lines_idx) == 0 or len(minus_lines_idx) == 0:
                continue

            for plus_idx in plus_lines_idx:
                # solving cases like:
                # plus_idx:[8,12]
                # minus_idx: [6,7,11]

                # 11  not in minus_idx
                # 12 -in minus_idx
                # 13 +in plus_idx
                # 14  not in plus_idx

                minus_idx = plus_idx - 1
                if (minus_idx in minus_lines_idx) and (not (plus_idx + 1 in plus_lines_idx)) and (
                        not (minus_idx - 1 in minus_lines_idx)):

                    minus_line = code_chunk_list[plus_idx - 1]  # line 12
                    plus_line = code_chunk_list[plus_idx]  # line 13

                    # empty check on the buggy line. One Line does not handle addition only.
                    if minus_line == "":
                        continue

                    # remove sign
                    minus_line = ' '.join(minus_line[1:].split())
                    plus_line = ' '.join(plus_line[1:].split())
                    minus_line = commit_checker.remove_comments(minus_line, lang)
                    plus_line = commit_checker.remove_comments(plus_line, lang)
                    if minus_line and plus_line:
                        if commit_checker.is_import_package(minus_line, plus_line, lang):
                            good_lines = 3
                        elif commit_checker.is_cosmetic(minus_line, plus_line):
                            good_lines = 4
                        elif commit_checker.is_assert(minus_line, plus_line):
                            good_lines = 6
                        elif commit_checker.is_deprecated(minus_line, plus_line):
                            good_lines = 7

                        else:
                            # at this point the string is assumed to be clean
                            # check if this line is bracket-balanced
                            if plus_line.count('(') != plus_line.count(')'):
                                minus_line, plus_line = bracket_matching(code_chunk_list, plus_idx,
                                                                         minus_lines_idx, plus_lines_idx)

                            minus_line = commit_checker.remove_comments(minus_line, lang)
                            plus_line = commit_checker.remove_comments(plus_line, lang)

                            minus_line_uni = commit_checker.safe_unicode(minus_line)
                            minus_line = minus_line_uni.encode('utf-8')
                            plus_line_uni = commit_checker.safe_unicode(plus_line)
                            plus_line = plus_line_uni.encode('utf-8')
                            with open(del_fil, 'ab') as fin:
                                fin.write(minus_line + b'\n')
                            with open(add_fil, 'ab') as fin:
                                fin.write(plus_line + b'\n')
                            with open(met_fil, 'a') as fin:
                                fin.write(commit + ',' + filename + ',' + project + '\n')


def get_commits(lang):
    """
    This functions finds all commits we consider buggy for a language and write the output in a file
    :return: write output in /data/XXX/commits_meta_lines.txt
    """
    init_dir = os.getcwd()
    meta_file = os.getcwd() + '/data/' + lang + '_commits_meta_lines.txt'
    project_repos = init_dir + '/data/repos/' + lang + '/'
    projects = os.listdir(project_repos)
    progress_count = 0.
    for project in projects:
        project_path = project_repos + project
        if os.path.isdir(project_path):  # check if the repo exist
            os.chdir(project_path)
            git_wrapper.pull_project(project_path)
            commits = git_wrapper.get_list_commit(project_path)
            os.chdir(init_dir)
            progress_count += 1.
            print("Project: " + project)
            print(progress_count / len(projects))
            for commit in commits:
                os.chdir(project_path)
                if commit_checker.is_fix(commit, project, project_repos):

                    output = git_wrapper.get_files_changed(commit)
                    os.chdir(init_dir)
                    filenames = str(output, 'utf-8').split('\n')
                    for filename in filenames:
                        if (lang == 'java' and commit_checker.is_java(filename)) or \
                                (lang == 'c' and commit_checker.is_cpp(filename)) or \
                                (lang == 'javascript' and commit_checker.is_javascript(filename)) or \
                                (lang == 'python' and commit_checker.is_python(filename)):
                            with open(meta_file, 'a') as fin:
                                fin.write(commit + ',' + filename + ',' + project + '\n')


def get_files(lang):
    """
    This file extract buggy and clean files for a language so they can be analyzed by understand to extract context
    :return:
    """
    init_dir = os.getcwd()
    i = 0
    meta_file = os.getcwd() + '/data/' + lang + '_commits_meta_lines.txt'
    project_repos = init_dir + '/data/repos/' + lang + '/'
    temp_repo = init_dir + '/' + lang + '_temp/'
    with open(meta_file, 'r') as meta:
        lines = meta.readlines()
    for line in lines:
        try:
            i += 1
            if i % 100 == 0:
                print(str(float(i) / len(lines)) + "% done")
            commit, file_path, project = line.replace('\n', '').split(',')
            project_path = project_repos + project
            if os.path.isdir(project_path):
                os.chdir(project_path)
                filename = file_path.split('/')[-1]
                rep_bug = temp_repo + project + '/' + commit + '/' + filename
                directory_buggy = rep_bug + '/buggy/'
                if not os.path.exists(directory_buggy + file_path.rsplit('/', 1)[0]):
                    output, err = git_wrapper.checkout_buggy_file(commit, file_path)
                    if not err:
                        if len(file_path.rsplit('/')) > 1:
                            os.makedirs(directory_buggy + file_path.rsplit('/', 1)[0])
                        else:
                            os.makedirs(directory_buggy)
                        copyfile(file_path, directory_buggy + file_path)

                directory_clean = rep_bug + '/clean/'
                if not os.path.exists(directory_clean + file_path.rsplit('/', 1)[0]):
                    output, err = git_wrapper.checkout_clean_file(commit, file_path)
                    if not err:
                        if len(file_path.rsplit('/')) > 1:
                            os.makedirs(directory_clean + file_path.rsplit('/', 1)[0])
                        else:
                            os.makedirs(directory_clean)
                        copyfile(file_path, directory_clean + file_path)
            os.chdir(init_dir)
        except Exception as e:
            print(e)





def extract_data(lang):
    languages = ['c', 'java', 'javascript', 'python']
    #lang = 'javascript'
    get_commits(lang)
    get_files(lang)



def main():
    languages = ['c', 'java', 'javascript', 'python']
    init_dir = os.getcwd()
    for lang in languages:
        del_file = init_dir + '/data/' + lang + '/train/removed_lines.txt'
        cont_file = init_dir + '/data/' + lang + '/train/context_lines.txt'
        add_file = init_dir + '/data/' + lang + '/train/added_lines.txt'
        meta_file = init_dir + '/data/' + lang + '/train/meta_lines.txt'
        project_repos = init_dir + '/data/repos/' + lang + '/'

        projects = os.listdir(project_repos)  # Load the list of projects
        progress_count = 0.
        for project in projects:
            project_path = project_repos + project
            if os.path.isdir(project_path):  # check if the repo exist
                os.chdir(project_path)
                print(project_path)
                git_wrapper.pull_project(project_path)
                commits = git_wrapper.get_list_commit(project_path)
                os.chdir(init_dir)
                progress_count += 1.
                print("Project: " + project)
                print(progress_count / len(projects))
                for commit in commits:
                    os.chdir(project_path)
                    if commit_checker.is_fix(commit, project, project_repos):
                        output = git_wrapper.get_diff(commit)
                        os.chdir(init_dir)

                        if lang == 'java':
                            get_one_line_change(output, commit, del_file, add_file, meta_file, project,
                                                commit_checker.is_java, project_path, init_dir, lang)
                        if lang == 'c':
                            get_one_line_change(output, commit, del_file, add_file, meta_file, project,
                                                commit_checker.is_cpp, project_path, init_dir, lang)
                        if lang == 'python':
                            get_one_line_change(output, commit, del_file, add_file, meta_file, project,
                                                commit_checker.is_python, project_path, init_dir, lang)
                        if lang == 'javascript':
                            get_one_line_change(output, commit, del_file, add_file, meta_file, project,
                                                commit_checker.is_javascript, project_path, init_dir, lang)
            else:
                print(project_path)

extract_data('c')
