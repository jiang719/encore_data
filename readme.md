######### Setup the environment
1. Get fairseq from source using:
```
git clone https://github.com/pytorch/fairseq
python setup.py build develop
pip install --editable .
```

2. Set up the path 'FAIRSEQPY' with
```
export FAIRSEQPY = /directory/where/fairseqpy/is/installed/
```
for example:
```
export /home/username/fairseq/
```

######### Downloading 1,000 top gitHub repos 
for each language.
1. Go to /source/data_extraction/downloads/
2. run each downloadXXXProjects.sh, with XXX being the language. For example:
```
bash downloadJavaProjects.sh
```
3. The top 1,000 github projects for each language will be downloaded in /data/repos/XXX/ Note that this steps takes a
lot of time and a few 100s G.
For reference, here is how long it took for me:
Java: 1h41min, 72G
C: 2h11min, 120G
Python: 1h25min, 58G
Javascript: 52 min, 34G

######## Data extraction:
1. cd ENCORE_data
2. get the list of commits for each language
```
python -m source.data_extraction.training_extractor extract_data
```
Reference time and size:
Java: 325m, 1,041,202 commits
C: 1904m, 3,589,129 commits
Python: 544m, 843,971 commits
Javascript: 184m. 590,496 commits

```
python -m source.data_extraction.training_extractor get_files
```
Reference time and size:
Java: 1,464min
C:
Python: 671min
Javascript: 502min

after that, we can delete data/repos/ to save disk space.
```
python -m source.data_extraction.understand_work
```
Reference time and size:
Java: 1,342min
C:
Python: 2,153min
Javascript: 2,417min

Pickle all functions:
```
pickle functions
```
Reference time and size:
Java: 
C:
Python: 95min
Javascript: 


```
write context
```


3. The extracted data will be in data/language/train/ with the fixes lines in added_lines.txt, the buggy lines in removed_lines.txt
and the metadata in meta_lines.txt. There is a line by line mapping between these 3 files.

######## Tokenization:
1. In source/tokenization/generate_data.py you can choose the size of the validation and test set by modifying valid_size
        and test_size. By default the sizes are 2,000 and 1,000 respectively.
1. cd ENCORE_data
```
python -m source.tokenization.generate_data
```
3. the data will be tokenized and generated in data/language/train/ the files starting with _test are the test set, the ones with
       _train for training and the ones with _valid will be used for validation. remadd contains the buggy and fixed lines
       separated by a tab (\t). All lines are tokenized with each token space separated. 
       
######## Prepare data from training and exclude test data from the training set
```
python -m source.training.prepare_data
```

######## Tuning the models
```
python -m source.training.random_search_tuning
python -m source.training.random_search_tuning_lstm
python -m source.training.random_search_tuning_transformer
```

This will randomly tuned each models for one epoch (500 times)

######## Extracting and ordering the top-k models
```
python -m source.training.get_best_models
```

This will return the top 10 best models and delete the tuned model (only keep the log) to save disk space.
Then the output is parsed to generate scripts to fully train the top 10 models.
The scripts will be in source/training/auto_scripts


######## Further train the top k models for each language
You can just run the models created in autoscripts. For example, the best model (according to the previous step) with context for Java is obtained by running:

```
python -m source.training.autoscript.context_tuned_1
```



######## Test the models for each language
```
python -m source.testing.prepare_test_data
get_testing_data.sh
testing_d4j_perfect_loc
reranking
compilitor
```



####### Getting Stats
